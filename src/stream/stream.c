/*
MIT License
-----------

Copyright (c) 2020 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <nshttp2/stddef.h>
#include <nshttp2/stream.h>

#ifdef __unix__

#include <sys/mman.h>

#endif

Http2ReturnCodeT http2StreamAllocate(Http2StreamT **http2Stream,
                                     const uint_fast32_t streamId,
                                     const uint8_t weight,
                                     const uint32_t remainingSendWindow,
                                     const uint32_t remainingReceiveWindow,
                                     const size_t maxIncomingDataInMemory) {
    (*http2Stream) = http2Calloc(sizeof(Http2StreamT));

    if (*http2Stream == NULL) {
        return HTTP2_NO_MEMORY;
    }

    pthread_mutex_init(&(*http2Stream)->mutex, NULL);
    (*http2Stream)->streamId = streamId;
    (*http2Stream)->weight = weight;
    (*http2Stream)->state = IDLE;
    (*http2Stream)->remainingSendWindow = remainingSendWindow;
    (*http2Stream)->remainingReceiveWindow = remainingReceiveWindow;
    (*http2Stream)->incomingDataBuffer.maxIncomingDataInMemory = maxIncomingDataInMemory;

    return HTTP2_SUCCESS;
}

Http2ReturnCodeT http2StreamDeallocate(Http2StreamT *HTTP2_CONST_RESTRICT_SPECIFIER http2Stream) {
    if (http2Stream->incomingHeadersBuffer != NULL) {
        http2BufferDeallocate(http2Stream->incomingHeadersBuffer);
    }

    if (http2Stream->parent != NULL) {
        http2StreamRemoveChild(http2Stream->parent, http2Stream);
    }

    if (http2Stream->children != NULL &&
        http2Stream->children->hashmap != NULL &&
        (kh_size(http2Stream->children->hashmap) > 0u)) {
        size_t i = 0u;
        size_t childrenCount;
        Http2StreamT **child_streams;
        Http2StreamT *childStream;

        childrenCount = kh_size(http2Stream->children->hashmap);
        child_streams = http2Malloc(sizeof(Http2StreamT *) * childrenCount);

        if (child_streams == NULL) {
            return HTTP2_NO_MEMORY;
        }

        kh_foreach_value(http2Stream->children->hashmap, childStream, {
            child_streams[i++] = childStream;
        })

        for (i = 0u; i < childrenCount; ++i) {
            http2StreamRemoveChild(http2Stream, child_streams[i]);
        }

        http2Free(child_streams);
    }

    if (http2Stream->siblings != NULL &&
        http2Stream->siblings->hashmap != NULL &&
        (kh_size(http2Stream->siblings->hashmap) > 0u)) {
        size_t i = 0u;
        size_t siblingCount;
        Http2StreamT **sibling_streams;
        Http2StreamT *siblingStream;

        siblingCount = kh_size(http2Stream->siblings->hashmap);
        sibling_streams = http2Malloc(sizeof(Http2StreamT *) * siblingCount);

        if (sibling_streams == NULL) {
            return HTTP2_NO_MEMORY;
        }

        kh_foreach_value(http2Stream->siblings->hashmap, siblingStream, {
            sibling_streams[i++] = siblingStream;
        })

        for (i = 0u; i < siblingCount; ++i) {
            http2StreamRemoveSibling(http2Stream, sibling_streams[i]);
        }

        http2Free(sibling_streams);
    }

    http2TemporaryBufferReset(&http2Stream->incomingDataBuffer);

    if (http2Stream->outgoingDataMemoryBuffer != NULL) {
        http2BufferDeallocate(http2Stream->outgoingDataMemoryBuffer);
    }

    pthread_mutex_destroy(&(*http2Stream).mutex);

    http2Free(http2Stream);

    return HTTP2_SUCCESS;
}

int http2StreamAddChild(Http2StreamT *HTTP2_CONST_RESTRICT_SPECIFIER http2Stream,
                        Http2StreamT *HTTP2_CONST_RESTRICT_SPECIFIER child) {
    if (http2Stream->children == NULL) {
        switch (streamHashmapAllocate(&http2Stream->children)) {
            case HTTP2_NO_MEMORY:
                return HTTP2_NO_MEMORY;
            default:
                break;
        }
    } else {
        Http2StreamT *child_stream;
        kh_foreach_value(http2Stream->children->hashmap, child_stream, {
            if (child_stream != child) {
                http2StreamAddSibling(child, child_stream);
                http2StreamAddSibling(child_stream, child);
            }
        })
    }

    streamHashmapInsert(http2Stream->children, child);

    child->parent = http2Stream;

    return HTTP2_SUCCESS;
}

void http2StreamRemoveChild(Http2StreamT *const http2Stream,
                            Http2StreamT *HTTP2_CONST_RESTRICT_SPECIFIER child) {
    if (http2Stream->children != NULL) {
        streamHashmapRemove(http2Stream->children, child->streamId);

        if (kh_size(http2Stream->children->hashmap) == 0u) {
            streamHashmapDeallocate(http2Stream->children);
            http2Stream->children = NULL;
        }
    }

    child->parent = NULL;
}

Http2ReturnCodeT http2StreamAddSibling(Http2StreamT *HTTP2_CONST_RESTRICT_SPECIFIER http2Stream,
                                       Http2StreamT *HTTP2_CONST_RESTRICT_SPECIFIER sibling) {
    bool sibling_list_created = false;

    if (http2Stream->siblings == NULL) {
        switch (streamHashmapAllocate(&http2Stream->siblings)) {
            case HTTP2_NO_MEMORY:
                return HTTP2_NO_MEMORY;
            default:
                sibling_list_created = true;
                break;
        }
    }

    if (sibling->siblings == NULL) {
        switch (streamHashmapAllocate(&sibling->siblings)) {
            case HTTP2_NO_MEMORY:
                if (sibling_list_created) {
                    streamHashmapDeallocate(http2Stream->siblings);
                    http2Stream->siblings = NULL;
                }

                return HTTP2_NO_MEMORY;
            default:
                break;
        }
    }

    streamHashmapInsert(http2Stream->siblings, sibling);
    streamHashmapInsert(sibling->siblings, http2Stream);

    return HTTP2_SUCCESS;
}

void http2StreamRemoveSibling(Http2StreamT *HTTP2_CONST_RESTRICT_SPECIFIER http2Stream,
                              Http2StreamT *HTTP2_CONST_RESTRICT_SPECIFIER sibling) {
    if (http2Stream->siblings != NULL) {
        streamHashmapRemove(http2Stream->siblings, sibling->streamId);

        if (kh_size(http2Stream->siblings->hashmap) == 0u) {
            streamHashmapDeallocate(http2Stream->siblings);

            http2Stream->siblings = NULL;
        }
    }

    if (sibling->siblings != NULL) {
        streamHashmapRemove(sibling->siblings, http2Stream->streamId);

        if (kh_size(sibling->siblings->hashmap) == 0u) {
            streamHashmapDeallocate(sibling->siblings);

            sibling->siblings = NULL;
        }
    }
}

HTTP2_INLINE_SPECIFIER void http2StreamSetState(Http2StreamT *HTTP2_CONST_RESTRICT_SPECIFIER http2Stream,
                                                const Http2StreamStateT state) {
    http2Stream->state = state;
}

uint8_t http2StreamFrameGetError(Http2StreamT *HTTP2_CONST_RESTRICT_SPECIFIER http2Stream,
                                 Http2FrameT *HTTP2_CONST_RESTRICT_SPECIFIER frame,
                                 const bool sent) {
    switch (frame->header.type) {
        /* We accept priority and window update frames in all states */
        case PRIORITY_FRAME_TYPE:
        case WINDOW_UPDATE_FRAME_TYPE:
            return NO_ERROR_ERROR_CODE;
        default:
            break;
    }

    switch (http2Stream->state) {
        case IDLE:
            switch (frame->header.type) {
                /* In idle we can process the first headers frame or push promise */
                case HEADERS_FRAME_TYPE:
                case PUSH_PROMISE_FRAME_TYPE:
                    return NO_ERROR_ERROR_CODE;
                default:
                    return PROTOCOL_ERROR_ERROR_CODE;
            }
        case RESERVED_LOCAL:
            if (sent) {
                switch (frame->header.type) {
                    case HEADERS_FRAME_TYPE:
                    case RST_STREAM_FRAME_TYPE:
                        return NO_ERROR_ERROR_CODE;
                    default:
                        return PROTOCOL_ERROR_ERROR_CODE;
                }
            } else {
                switch (frame->header.type) {
                    case RST_STREAM_FRAME_TYPE:
                        return NO_ERROR_ERROR_CODE;
                    default:
                        return PROTOCOL_ERROR_ERROR_CODE;
                }
            }
        case RESERVED_REMOTE:
            if (sent) {
                switch (frame->header.type) {
                    case RST_STREAM_FRAME_TYPE:
                        return NO_ERROR_ERROR_CODE;
                    default:
                        return PROTOCOL_ERROR_ERROR_CODE;
                }
            } else {
                switch (frame->header.type) {
                    case HEADERS_FRAME_TYPE:
                    case RST_STREAM_FRAME_TYPE:
                        return NO_ERROR_ERROR_CODE;
                    default:
                        return PROTOCOL_ERROR_ERROR_CODE;
                }
            }
        case OPEN:
            if (http2Stream->flags & HTTP2_STREAM_CONTINUATION_EXPECTED) {
                if (frame->header.type != CONTINUATION_FRAME_TYPE) {
                    return PROTOCOL_ERROR_ERROR_CODE;
                } else {
                    return NO_ERROR_ERROR_CODE;
                }
            } else {
                switch (frame->header.type) {
                    case RST_STREAM_FRAME_TYPE:
                    case DATA_FRAME_TYPE:
                        return NO_ERROR_ERROR_CODE;
                    default:
                        return PROTOCOL_ERROR_ERROR_CODE;
                }
            }
        case HALF_CLOSED_LOCAL:
            if (sent) {
                switch (frame->header.type) {
                    case RST_STREAM_FRAME_TYPE:
                        return NO_ERROR_ERROR_CODE;
                    default:
                        return STREAM_CLOSED_ERROR_CODE;
                }
            } else {
                return NO_ERROR_ERROR_CODE;
            }
        case HALF_CLOSED_REMOTE:
            if (sent) {
                return NO_ERROR_ERROR_CODE;
            } else {
                switch (frame->header.type) {
                    case RST_STREAM_FRAME_TYPE:
                        return NO_ERROR_ERROR_CODE;
                    default:
                        return STREAM_CLOSED_ERROR_CODE;
                }
            }
        case CLOSED:
            if (http2Stream->flags & HTTP2_STREAM_CONTINUATION_EXPECTED) {
                if (frame->header.type != CONTINUATION_FRAME_TYPE) {
                    return PROTOCOL_ERROR_ERROR_CODE;
                } else {
                    return NO_ERROR_ERROR_CODE;
                }
            } else {
                return PROTOCOL_ERROR_ERROR_CODE;
            }
        default:
            return NO_ERROR_ERROR_CODE;
    }
}

Http2ReturnCodeT http2StreamDecodeHeaders(Http2StreamT *HTTP2_CONST_RESTRICT_SPECIFIER http2Stream,
                                          Http2HeaderListT **headerList,
                                          nghttp2_hd_inflater *nghttp2HdInflater) {
    Http2BufferT *incomingHeadersBuffer;
    incomingHeadersBuffer = http2Stream->incomingHeadersBuffer;

    if (incomingHeadersBuffer == NULL || incomingHeadersBuffer->length == 0u) {
        *headerList = NULL;
        return HTTP2_SUCCESS;
    } else {
        switch (http2HeadersDecode(incomingHeadersBuffer->buffer,
                                   incomingHeadersBuffer->length,
                                   headerList,
                                   nghttp2HdInflater)) {
            case NGHTTP2_ERR_NOMEM:
                return HTTP2_NO_MEMORY;
            case NGHTTP2_ERR_HEADER_COMP:
            case NGHTTP2_ERR_BUFFER_ERROR:
                return HTTP2_INTERNAL_ERROR;
            default:
                http2BufferDeallocate(incomingHeadersBuffer);
                http2Stream->incomingHeadersBuffer = NULL;
                return HTTP2_SUCCESS;
        }
    }
}

Http2ReturnCodeT http2StreamAddHeaderBlock(Http2StreamT *HTTP2_CONST_RESTRICT_SPECIFIER http2Stream,
                                           Http2FrameT *HTTP2_CONST_RESTRICT_SPECIFIER frame) {
    bool hasPriority;
    uint8_t *headerBlock = NULL;
    uint32_t headerBlockLength;

    hasPriority = http2FrameHasPriority(frame);

    if (hasPriority) {
        headerBlockLength = (frame->header.length - HTTP2_PRIORITY_FRAME_PAYLOAD_SIZE);
    } else {
        headerBlockLength = frame->header.length;
    }

    switch (frame->header.type) {
        case HEADERS_FRAME_TYPE:
            headerBlock = frame->payload.http2HeadersFramePayload->headerPayload;
            break;
        case CONTINUATION_FRAME_TYPE:
            headerBlock = frame->payload.http2ContinuationPayload->headerBlockFragment;
            break;
    }

    if (headerBlock == NULL) {
        return HTTP2_INTERNAL_ERROR;
    }

    if (http2Stream->incomingHeadersBuffer == NULL) {
        http2BufferAllocate(&http2Stream->incomingHeadersBuffer, 0u);
    }

    switch (http2BufferAppend(http2Stream->incomingHeadersBuffer,
                              headerBlock,
                              headerBlockLength)) {
        case HTTP2_NO_MEMORY:
            return HTTP2_NO_MEMORY;
        default:
            return HTTP2_SUCCESS;
    }
}

Http2ReturnCodeT http2StreamAppendOutgoingMemoryData(Http2StreamT *HTTP2_CONST_RESTRICT_SPECIFIER http2Stream,
                                                     uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer,
                                                     size_t bufferLength) {
    if (http2Stream->outgoingDataMemoryBuffer == NULL) {
        switch (http2BufferAllocate(&http2Stream->outgoingDataMemoryBuffer, 0u)) {
            case HTTP2_NO_MEMORY:
                return HTTP2_NO_MEMORY;
            default:
                switch (http2BufferAppend(http2Stream->outgoingDataMemoryBuffer, buffer, bufferLength)) {
                    case HTTP2_SUCCESS:
                        return HTTP2_SUCCESS;
                    default:
                        http2BufferDeallocate(http2Stream->outgoingDataMemoryBuffer);
                        http2Stream->outgoingDataMemoryBuffer = NULL;
                        return HTTP2_NO_MEMORY;
                }
        }
    } else {
        switch (http2BufferAppend(http2Stream->outgoingDataMemoryBuffer, buffer, bufferLength)) {
            case HTTP2_SUCCESS:
                return HTTP2_SUCCESS;
            default:
                return HTTP2_NO_MEMORY;
        }
    }
}

Http2ReturnCodeT http2StreamAppendIncomingData(Http2StreamT *HTTP2_CONST_RESTRICT_SPECIFIER http2Stream,
                                               uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer,
                                               size_t bufferLength,
                                               const char *temporaryFileTemplate) {
    return http2TemporaryBufferAppend(&http2Stream->incomingDataBuffer, buffer, bufferLength, temporaryFileTemplate);
}

Http2ReturnCodeT http2StreamGetIncomingData(Http2StreamT *HTTP2_CONST_RESTRICT_SPECIFIER http2Stream,
                                            uint8_t **buffer,
                                            size_t *HTTP2_CONST_RESTRICT_SPECIFIER bufferLength) {
    switch (http2TemporaryBufferGetData(&http2Stream->incomingDataBuffer, buffer, bufferLength)) {
        default:
            return HTTP2_NO_MEMORY;
        case HTTP2_SUCCESS:
            return HTTP2_SUCCESS;
    }
}

void http2StreamDeallocateIncomingData(Http2StreamT *HTTP2_CONST_RESTRICT_SPECIFIER http2Stream) {
    http2TemporaryBufferReset(&http2Stream->incomingDataBuffer);
}

void http2StreamDeallocateOutgoingData(Http2StreamT *HTTP2_CONST_RESTRICT_SPECIFIER http2Stream) {
    if (http2Stream->outgoingDataMemoryBuffer != NULL) {
        http2BufferDeallocate(http2Stream->outgoingDataMemoryBuffer);
        http2Stream->outgoingDataMemoryBuffer = NULL;
    }
}

bool http2StreamHasRemainingOutgoingData(Http2StreamT *HTTP2_CONST_RESTRICT_SPECIFIER http2Stream) {
    if (http2Stream->outgoingDataMemoryBuffer != NULL) {
        return true;
    }

    return false;
}

void http2StreamSetCustomData(Http2StreamT *http2Stream, void *customData) {
    http2Stream->customData = customData;
}

void http2StreamGetCustomData(Http2StreamT *http2Stream, void **customData) {
    *customData = http2Stream->customData;
}
