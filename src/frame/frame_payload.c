/*
MIT License
-----------

Copyright (c) 2020 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <nshttp2/stddef.h>
#include <nshttp2/frame.h>
#include <string.h>

#ifdef __unix__

#include <arpa/inet.h>
#include <nshttp2/return_code.h>

#elif defined(_WIN32)

#include <winsock2.h>

#pragma comment(lib, "Ws2_32. lib")

#endif

void http2BufferRemovePadding(uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer,
                              uint32_t frameLength,
                              Http2BufferWithoutPaddingT *bufferWithoutPadding) {
    /* Removes the head specifying the pad length */
    bufferWithoutPadding->buffer = buffer + sizeof(uint8_t);
    /* Frame length minus the padding length and padding length specifier */
    bufferWithoutPadding->frameLengthWithoutPadding = frameLength - *(buffer) - sizeof(uint8_t);
}

void
http2PriorityFramePayloadToBytes(Http2PriorityFramePayloadT *HTTP2_CONST_RESTRICT_SPECIFIER http2PriorityFramePayload,
                                 uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer) {
    uint32_t dependency_bytes = htonl(http2PriorityFramePayload->dependency);

    if (http2PriorityFramePayload->exclusive) {
        dependency_bytes = dependency_bytes | HTTP2_EXCLUSIVE_FLAG_BITS;
    }

    memcpy(buffer, &dependency_bytes, sizeof(uint32_t));
    memcpy(buffer + sizeof(uint32_t),
           &http2PriorityFramePayload->weight,
           sizeof(uint8_t));
}

static HTTP2_INLINE_SPECIFIER void http2WritePaddingPayload(uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer,
                                                            Http2FramePaddingT *HTTP2_CONST_RESTRICT_SPECIFIER http2FramePadding,
                                                            const uint32_t framePayloadLength) {
    memcpy(buffer + sizeof(uint8_t) + framePayloadLength,
           http2FramePadding->padding,
           http2FramePadding->length);
}

void
http2PriorityFramePayloadFromBytes(Http2PriorityFramePayloadT *HTTP2_CONST_RESTRICT_SPECIFIER http2PriorityFramePayload,
                                   const uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer) {
    http2PriorityFramePayload->exclusive = ((*buffer) & HTTP2_EXCLUSIVE_FLAG_BITS) > 0u;

    if (http2PriorityFramePayload->exclusive) {
        uint32_t dependency = *((uint32_t *) buffer);
        /* Before we copy the dependency we need to make sure
         * that it is does not have the exclusive flag set
         */
        dependency = dependency ^ HTTP2_EXCLUSIVE_FLAG_BITS;
        http2PriorityFramePayload->dependency = ntohl(dependency);
    } else {
        http2PriorityFramePayload->dependency = ntohl(*((uint32_t *) buffer));
    }

    memcpy(&http2PriorityFramePayload->weight, buffer + sizeof(uint32_t), sizeof(uint8_t));
}

void http2DataFramePayloadToBytes(uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER http2DataFramePayload,
                                  uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer,
                                  const uint32_t frameLength,
                                  Http2FramePaddingT *HTTP2_CONST_RESTRICT_SPECIFIER http2FramePadding) {
    if (http2FramePadding != NULL) {
        uint32_t frame_payload_length;

        memcpy(buffer, &http2FramePadding->length, sizeof(uint8_t));

        frame_payload_length = frameLength - sizeof(uint8_t) - http2FramePadding->length;

        memcpy(buffer + sizeof(uint8_t), http2DataFramePayload, frame_payload_length);

        http2WritePaddingPayload(buffer, http2FramePadding, frame_payload_length);
    } else {
        memcpy(buffer, http2DataFramePayload, frameLength);
    }
}

void http2DataFramePayloadFromBytes(uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER http2DataFramePayload,
                                    uint32_t *const frameLengthWithoutPadding,
                                    uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer,
                                    const uint32_t frameLength,
                                    const bool hasPadding) {
    if (hasPadding) {
        Http2BufferWithoutPaddingT buffer_without_padding;
        http2BufferRemovePadding(buffer, frameLength, &buffer_without_padding);

        *frameLengthWithoutPadding = buffer_without_padding.frameLengthWithoutPadding;

        memcpy(http2DataFramePayload,
               buffer_without_padding.buffer,
               buffer_without_padding.frameLengthWithoutPadding);
    } else {
        *frameLengthWithoutPadding = frameLength;
        memcpy(http2DataFramePayload, buffer, frameLength);
    }
}

void http2HeadersFramePayloadToBytes(Http2HeadersFramePayloadT *HTTP2_CONST_RESTRICT_SPECIFIER http2HeadersFramePayload,
                                     uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer,
                                     const uint32_t frameLength,
                                     Http2FramePaddingT *HTTP2_CONST_RESTRICT_SPECIFIER http2FramePadding,
                                     bool hasPriority) {
    if (http2FramePadding != NULL) {
        uint32_t frame_payload_length;

        *buffer = http2FramePadding->length;

        frame_payload_length = frameLength - sizeof(uint8_t) - http2FramePadding->length;

        http2WritePaddingPayload(buffer, http2FramePadding, frame_payload_length);

        if (hasPriority) {
            http2PriorityFramePayloadToBytes(&http2HeadersFramePayload->priority, buffer + sizeof(uint8_t));

            memcpy(buffer + sizeof(uint8_t) + HTTP2_PRIORITY_FRAME_PAYLOAD_SIZE,
                   http2HeadersFramePayload->headerPayload,
                   frame_payload_length - HTTP2_PRIORITY_FRAME_PAYLOAD_SIZE);
        } else {
            memcpy(buffer,
                   http2HeadersFramePayload->headerPayload,
                   frameLength);
        }
    } else {
        if (hasPriority) {
            http2PriorityFramePayloadToBytes(&http2HeadersFramePayload->priority, buffer);

            memcpy(buffer + HTTP2_PRIORITY_FRAME_PAYLOAD_SIZE,
                   http2HeadersFramePayload->headerPayload,
                   frameLength - HTTP2_PRIORITY_FRAME_PAYLOAD_SIZE);
        } else {
            memcpy(buffer,
                   http2HeadersFramePayload->headerPayload,
                   frameLength);
        }
    }
}

void
http2HeadersFramePayloadFromBytes(Http2HeadersFramePayloadT *HTTP2_CONST_RESTRICT_SPECIFIER http2HeadersFramePayload,
                                  uint32_t *HTTP2_CONST_RESTRICT_SPECIFIER frameLengthWithoutPadding,
                                  uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer,
                                  const uint32_t frameLength,
                                  const bool hasPadding,
                                  const bool has_priority) {
    if (hasPadding) {
        Http2BufferWithoutPaddingT bufferWithoutPadding;
        http2BufferRemovePadding(buffer, frameLength, &bufferWithoutPadding);

        *frameLengthWithoutPadding = bufferWithoutPadding.frameLengthWithoutPadding;

        if (has_priority) {
            http2PriorityFramePayloadFromBytes(&http2HeadersFramePayload->priority,
                                               bufferWithoutPadding.buffer);

            memcpy(http2HeadersFramePayload->headerPayload,
                   bufferWithoutPadding.buffer + HTTP2_PRIORITY_FRAME_PAYLOAD_SIZE,
                   bufferWithoutPadding.frameLengthWithoutPadding - HTTP2_PRIORITY_FRAME_PAYLOAD_SIZE);
        } else {
            memcpy(http2HeadersFramePayload->headerPayload,
                   bufferWithoutPadding.buffer,
                   bufferWithoutPadding.frameLengthWithoutPadding);
        }
    } else {
        *frameLengthWithoutPadding = frameLength;

        if (has_priority) {
            http2PriorityFramePayloadFromBytes(&http2HeadersFramePayload->priority, buffer);

            memcpy(http2HeadersFramePayload->headerPayload,
                   buffer + HTTP2_PRIORITY_FRAME_PAYLOAD_SIZE,
                   frameLength - HTTP2_PRIORITY_FRAME_PAYLOAD_SIZE);
        } else {
            memcpy(http2HeadersFramePayload->headerPayload,
                   buffer,
                   frameLength);
        }
    }
}

void
http2RstStreamFramePayloadToBytes(
        Http2RstStreamFramePayloadT *HTTP2_CONST_RESTRICT_SPECIFIER http2RstStreamFramePayload,
        uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer) {
    const uint32_t errorCode = htonl(http2RstStreamFramePayload->errorCode);
    memcpy(buffer, &errorCode, HTTP2_RST_STREAM_FRAME_PAYLOAD_SIZE);
}

void http2RstStreamFramePayloadFromBytes(
        Http2RstStreamFramePayloadT *HTTP2_CONST_RESTRICT_SPECIFIER http2RstStreamFramePayload,
        const uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer) {
    http2RstStreamFramePayload->errorCode = ntohl(*((uint32_t *) buffer));
}

/**
 * Settings frame format defined in https://tools.ietf.org/html/rfc7540#section-6.5.1
 */
static void http2SettingsFormatToBytes(const uint16_t identifier,
                                       const uint32_t value,
                                       uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer) {
    const uint16_t identifierBytes = htons(identifier);
    const uint32_t valueBytes = htonl(value);
    memcpy(buffer, &identifierBytes, sizeof(uint16_t));
    memcpy(buffer + sizeof(uint16_t), &valueBytes, sizeof(uint32_t));
}

void
http2SettingsFramePayloadToBytes(Http2SettingsFramePayloadT *HTTP2_CONST_RESTRICT_SPECIFIER http2SettingsFramePayload,
                                 uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer) {
    uint_fast8_t settingsCount = 0u;
    /* We use this variable to count the number of settings we have inserted.
     * Based on the inserted setting we copy the data into the right position in the buffer.
     */

    if (http2SettingsFramePayload->headerTableSize != ASSUMED_SETTING_TABLE_SIZE) {
        http2SettingsFormatToBytes(SETTINGS_HEADER_TABLE_SIZE_IDENTIFIER,
                                   http2SettingsFramePayload->headerTableSize,
                                   buffer + (settingsCount++) * HTTP2_SETTINGS_FORMAT_SIZE);
    }

    if (http2SettingsFramePayload->enablePush != ASSUMED_SETTING_ENABLE_PUSH) {
        http2SettingsFormatToBytes(SETTINGS_ENABLE_PUSH_IDENTIFIER,
                                   http2SettingsFramePayload->enablePush ? 1u : 0u,
                                   buffer + (settingsCount++) * HTTP2_SETTINGS_FORMAT_SIZE);
    }

    if (http2SettingsFramePayload->maxConcurrentStreams != ASSUMED_SETTING_MAX_CONCURRENT_STREAMS) {
        http2SettingsFormatToBytes(SETTINGS_MAX_CONCURRENT_STREAMS_IDENTIFIER,
                                   http2SettingsFramePayload->maxConcurrentStreams,
                                   buffer + (settingsCount++) * HTTP2_SETTINGS_FORMAT_SIZE);
    }

    if (http2SettingsFramePayload->initialWindowSize != ASSUMED_SETTING_INITIAL_WINDOW_SIZE) {
        http2SettingsFormatToBytes(SETTINGS_INITIAL_WINDOW_SIZE_IDENTIFIER,
                                   http2SettingsFramePayload->initialWindowSize,
                                   buffer + (settingsCount++) * HTTP2_SETTINGS_FORMAT_SIZE);
    }

    if (http2SettingsFramePayload->maxFrameSize != ASSUMED_SETTING_MAX_FRAME_SIZE) {
        http2SettingsFormatToBytes(SETTINGS_MAX_FRAME_SIZE_IDENTIFIER,
                                   http2SettingsFramePayload->maxFrameSize,
                                   buffer + (settingsCount++) * HTTP2_SETTINGS_FORMAT_SIZE);
    }

    if (http2SettingsFramePayload->maxHeaderListSize != ASSUMED_SETTING_MAX_HEADER_LIST_SIZE) {
        http2SettingsFormatToBytes(SETTINGS_MAX_HEADER_LIST_SIZE_IDENTIFIER,
                                   http2SettingsFramePayload->maxHeaderListSize,
                                   buffer + settingsCount * HTTP2_SETTINGS_FORMAT_SIZE);
    }
}

/**
 * Settings frame format defined in https://tools.ietf.org/html/rfc7540#section-6.5.1
 */
static uint16_t http2SettingsFormatFromBytes(uint32_t *HTTP2_CONST_RESTRICT_SPECIFIER value,
                                             const uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer) {
    *value = ntohl(*((uint32_t *) (buffer + sizeof(uint16_t))));
    return ntohs(*((uint16_t *) buffer));
}

void
http2SettingsFramePayloadFromBytes(Http2SettingsFramePayloadT *HTTP2_CONST_RESTRICT_SPECIFIER http2SettingsFramePayload,
                                   uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer,
                                   const uint32_t frameLength,
                                   const bool isClient) {
    uint_fast8_t header_num;

    /* Initial default values are defined in https://tools.ietf.org/html/rfc7540#section-6.5.2 */
    http2SettingsFramePayload->headerTableSize = ASSUMED_SETTING_TABLE_SIZE;
    http2SettingsFramePayload->enablePush = isClient ? false : ASSUMED_SETTING_ENABLE_PUSH;
    /* The default value for concurrent streams is unlimited, but we want to limit this anyway to limit resources. */
    http2SettingsFramePayload->maxConcurrentStreams = ASSUMED_SETTING_MAX_CONCURRENT_STREAMS;
    http2SettingsFramePayload->initialWindowSize = ASSUMED_SETTING_INITIAL_WINDOW_SIZE;
    http2SettingsFramePayload->maxFrameSize = ASSUMED_SETTING_MAX_FRAME_SIZE;
    http2SettingsFramePayload->maxHeaderListSize = ASSUMED_SETTING_MAX_HEADER_LIST_SIZE;

    for (header_num = 0u;
         header_num < (uint_fast8_t) (frameLength / HTTP2_SETTINGS_FORMAT_SIZE);
         ++header_num) {
        uint32_t value;

        switch (http2SettingsFormatFromBytes(&value, buffer + (header_num * HTTP2_SETTINGS_FORMAT_SIZE))) {
            case SETTINGS_HEADER_TABLE_SIZE_IDENTIFIER:
                http2SettingsFramePayload->headerTableSize = value;
                break;
            case SETTINGS_ENABLE_PUSH_IDENTIFIER:
                /* We check if the value is 1 and set to true, otherwise set to false. */
                http2SettingsFramePayload->enablePush = value == 1u;
                break;
            case SETTINGS_MAX_CONCURRENT_STREAMS_IDENTIFIER:
                http2SettingsFramePayload->maxConcurrentStreams = value;
                break;
            case SETTINGS_INITIAL_WINDOW_SIZE_IDENTIFIER:
                http2SettingsFramePayload->initialWindowSize = value;
                break;
            case SETTINGS_MAX_FRAME_SIZE_IDENTIFIER:
                http2SettingsFramePayload->maxFrameSize = value;
                break;
            case SETTINGS_MAX_HEADER_LIST_SIZE_IDENTIFIER:
                http2SettingsFramePayload->maxHeaderListSize = value;
                break;
        }
    }
}

void http2PushPromiseFramePayloadToBytes(
        Http2PushPromiseFramePayloadT *HTTP2_CONST_RESTRICT_SPECIFIER http2PushPromiseFramePayload,
        uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer,
        const uint32_t frameLength,
        Http2FramePaddingT *HTTP2_CONST_RESTRICT_SPECIFIER http2FramePadding) {
    const uint32_t promised_stream_id = htonl(http2PushPromiseFramePayload->promisedStreamId);

    if (http2FramePadding != NULL) {
        uint32_t frame_payload_length;

        memcpy(buffer, &http2FramePadding->length, sizeof(uint8_t));

        frame_payload_length = frameLength - sizeof(uint8_t) - http2FramePadding->length;

        memcpy(buffer + sizeof(uint8_t), &promised_stream_id, sizeof(uint32_t));
        memcpy(buffer + sizeof(uint8_t) + sizeof(uint32_t),
               http2PushPromiseFramePayload->headerBlockFragment,
               frame_payload_length - sizeof(uint32_t));

        http2WritePaddingPayload(buffer, http2FramePadding, frame_payload_length);
    } else {
        memcpy(buffer, &promised_stream_id, sizeof(uint32_t));
        memcpy(buffer + sizeof(uint32_t),
               http2PushPromiseFramePayload->headerBlockFragment,
               frameLength - sizeof(uint32_t));
    }
}

void http2PushPromiseFramePayloadFromBytes(
        Http2PushPromiseFramePayloadT *HTTP2_CONST_RESTRICT_SPECIFIER http2PushPromiseFramePayload,
        uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer,
        const uint32_t frameLength,
        const bool hasPadding) {
    if (hasPadding) {
        Http2BufferWithoutPaddingT buffer_without_padding;
        http2BufferRemovePadding(buffer, frameLength, &buffer_without_padding);
        http2PushPromiseFramePayload->promisedStreamId = ntohl(*((uint32_t *) buffer_without_padding.buffer));
        memcpy(http2PushPromiseFramePayload->headerBlockFragment,
               buffer_without_padding.buffer + sizeof(uint32_t),
               buffer_without_padding.frameLengthWithoutPadding - sizeof(uint32_t));
    } else {
        http2PushPromiseFramePayload->promisedStreamId = ntohl(*((uint32_t *) buffer));
        memcpy(http2PushPromiseFramePayload->headerBlockFragment,
               buffer + sizeof(uint32_t),
               frameLength - sizeof(uint32_t));
    }
}

void http2PingFramePayloadToBytes(Http2PingFramePayloadT *HTTP2_CONST_RESTRICT_SPECIFIER http2PingFramePayload,
                                  uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer) {
    memcpy(buffer, http2PingFramePayload->opaqueData, 8 * sizeof(uint8_t));
}

void http2PingFramePayloadFromBytes(Http2PingFramePayloadT *HTTP2_CONST_RESTRICT_SPECIFIER http2PingFramePayload,
                                    uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer) {
    memcpy(http2PingFramePayload->opaqueData, buffer, 8 * sizeof(uint8_t));
}

void http2GoawayFramePayloadToBytes(Http2GoawayFramePayloadT *HTTP2_CONST_RESTRICT_SPECIFIER http2GoawayFramePayload,
                                    uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer,
                                    const uint32_t payloadLength) {
    uint32_t value = htonl(http2GoawayFramePayload->lastStreamId);
    memcpy(buffer, &value, sizeof(uint32_t));

    value = htonl(http2GoawayFramePayload->errorCode);
    memcpy(buffer + sizeof(uint32_t), &value, sizeof(uint32_t));

    if (payloadLength > HTTP2_GOAWAY_MINIMUM_PAYLOAD_LEN) {
        memcpy(buffer + HTTP2_GOAWAY_MINIMUM_PAYLOAD_LEN,
               http2GoawayFramePayload->additionalDebugData,
               payloadLength - HTTP2_GOAWAY_MINIMUM_PAYLOAD_LEN);
    }
}

void http2GoawayFramePayloadFromBytes(Http2GoawayFramePayloadT *HTTP2_CONST_RESTRICT_SPECIFIER http2GoawayFramePayload,
                                      const uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer,
                                      const uint32_t payloadLength) {
    uint32_t value = ntohl(*(uint32_t *) buffer);
    http2GoawayFramePayload->lastStreamId = value;

    value = ntohl(*(uint32_t *) (buffer + sizeof(uint32_t)));
    http2GoawayFramePayload->errorCode = value;

    if (payloadLength > HTTP2_GOAWAY_MINIMUM_PAYLOAD_LEN) {
        memcpy(http2GoawayFramePayload->additionalDebugData,
               buffer + HTTP2_GOAWAY_MINIMUM_PAYLOAD_LEN,
               payloadLength - HTTP2_GOAWAY_MINIMUM_PAYLOAD_LEN);
    }
}

void http2WindowUpdatePayloadToBytes(Http2WindowUpdatePayloadT *HTTP2_CONST_RESTRICT_SPECIFIER http2WindowUpdatePayload,
                                     uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer) {
    const uint32_t window_size_increment = htonl(http2WindowUpdatePayload->windowSizeIncrement);
    memcpy(buffer, &window_size_increment, sizeof(uint32_t));
}

void
http2WindowUpdatePayloadFromBytes(Http2WindowUpdatePayloadT *HTTP2_CONST_RESTRICT_SPECIFIER http2WindowUpdatePayload,
                                  const uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer) {
    http2WindowUpdatePayload->windowSizeIncrement = ntohl(*((uint32_t *) buffer));
}

void http2ContinuationPayloadToBytes(Http2ContinuationPayloadT *HTTP2_CONST_RESTRICT_SPECIFIER http2ContinuationPayload,
                                     uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer,
                                     const uint32_t frameLength) {
    memcpy(buffer, http2ContinuationPayload->headerBlockFragment, frameLength);
}

void
http2ContinuationPayloadFromBytes(Http2ContinuationPayloadT *HTTP2_CONST_RESTRICT_SPECIFIER http2ContinuationPayload,
                                  uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer,
                                  const uint32_t frameLength) {
    memcpy(http2ContinuationPayload->headerBlockFragment, buffer, frameLength);
}

Http2ReturnCodeT http2HeadersFramePayloadAllocate(Http2HeadersFramePayloadT **http2HeadersFramePayload,
                                                  bool hasPriority,
                                                  uint32_t frameLength) {
    *http2HeadersFramePayload = http2Malloc(sizeof(Http2HeadersFramePayloadT));

    if (*http2HeadersFramePayload == NULL) {
        return HTTP2_NO_MEMORY;
    }

    if (hasPriority) {
        (*http2HeadersFramePayload)->headerPayload = http2Malloc(frameLength - HTTP2_PRIORITY_FRAME_PAYLOAD_SIZE);
    } else {
        (*http2HeadersFramePayload)->headerPayload = http2Malloc(frameLength);
    }

    if ((*http2HeadersFramePayload)->headerPayload == NULL) {
        http2Free((*http2HeadersFramePayload)->headerPayload);
        return HTTP2_NO_MEMORY;
    }

    return HTTP2_SUCCESS;
}

void
http2HeadersFramePayloadDeallocate(Http2HeadersFramePayloadT *HTTP2_CONST_RESTRICT_SPECIFIER http2HeadersFramePayload) {
    http2Free(http2HeadersFramePayload->headerPayload);
    http2Free(http2HeadersFramePayload);
}

Http2ReturnCodeT
http2PriorityFramePayloadAllocate(Http2PriorityFramePayloadT **http2PriorityFramePayload) {
    (*http2PriorityFramePayload) = http2Malloc(sizeof(Http2PriorityFramePayloadT));

    if (*http2PriorityFramePayload == NULL) {
        return HTTP2_NO_MEMORY;
    }

    return HTTP2_SUCCESS;
}

void http2PriorityFramePayloadDeallocate(Http2PriorityFramePayloadT *http2PriorityFramePayload) {
    http2Free(http2PriorityFramePayload);
}

Http2ReturnCodeT
http2RstStreamFramePayloadAllocate(Http2RstStreamFramePayloadT **http2RstStreamFramePayload) {
    (*http2RstStreamFramePayload) = http2Malloc(sizeof(Http2RstStreamFramePayloadT));

    if (*http2RstStreamFramePayload == NULL) {
        return HTTP2_NO_MEMORY;
    }

    return HTTP2_SUCCESS;
}

void http2RstStreamFramePayloadDeallocate(
        Http2RstStreamFramePayloadT *HTTP2_CONST_RESTRICT_SPECIFIER http2RstStreamFramePayload) {
    http2Free(http2RstStreamFramePayload);
}

Http2ReturnCodeT http2SettingsFramePayloadAllocate(Http2SettingsFramePayloadT **http2SettingsFramePayload) {
    (*http2SettingsFramePayload) = http2Malloc(sizeof(Http2SettingsFramePayloadT));

    if (*http2SettingsFramePayload == NULL) {
        return HTTP2_NO_MEMORY;
    }

    /* Initial default values are defined in https://tools.ietf.org/html/rfc7540#section-6.5.2 */
    (*http2SettingsFramePayload)->headerTableSize = ASSUMED_SETTING_TABLE_SIZE;
    (*http2SettingsFramePayload)->enablePush = ASSUMED_SETTING_ENABLE_PUSH;
    /* The default value for concurrent streams is unlimited, but we want to limit this anyway to limit resources. */
    (*http2SettingsFramePayload)->maxConcurrentStreams = ASSUMED_SETTING_MAX_CONCURRENT_STREAMS;
    (*http2SettingsFramePayload)->initialWindowSize = ASSUMED_SETTING_INITIAL_WINDOW_SIZE;
    (*http2SettingsFramePayload)->maxFrameSize = ASSUMED_SETTING_MAX_FRAME_SIZE;
    (*http2SettingsFramePayload)->maxHeaderListSize = ASSUMED_SETTING_MAX_HEADER_LIST_SIZE;

    return HTTP2_SUCCESS;
}

void
http2SettingsFramePayloadDeallocate(
        Http2SettingsFramePayloadT *HTTP2_CONST_RESTRICT_SPECIFIER http2SettingsFramePayload) {
    http2Free(http2SettingsFramePayload);
}

Http2ReturnCodeT
http2PushPromiseFramePayloadAllocate(Http2PushPromiseFramePayloadT **http2PushPromiseFramePayload,
                                     const uint32_t frameLength) {
    (*http2PushPromiseFramePayload) = http2Malloc(sizeof(Http2PushPromiseFramePayloadT));

    if ((*http2PushPromiseFramePayload) == NULL) {
        return HTTP2_NO_MEMORY;
    }

    (*http2PushPromiseFramePayload)->headerBlockFragment = http2Malloc(frameLength - sizeof(uint32_t));

    if ((*http2PushPromiseFramePayload)->headerBlockFragment == NULL) {
        http2Free(*http2PushPromiseFramePayload);
        return HTTP2_NO_MEMORY;
    }

    return HTTP2_SUCCESS;
}

void http2PushPromiseFramePayloadDeallocate(
        Http2PushPromiseFramePayloadT *HTTP2_CONST_RESTRICT_SPECIFIER http2PushPromiseFramePayload) {
    http2Free(http2PushPromiseFramePayload->headerBlockFragment);
    http2Free(http2PushPromiseFramePayload);
}

Http2ReturnCodeT http2PingFramePayloadAllocate(Http2PingFramePayloadT **http2PingFramePayload) {
    (*http2PingFramePayload) = http2Malloc(sizeof(Http2PingFramePayloadT));

    if (*http2PingFramePayload == NULL) {
        return HTTP2_NO_MEMORY;
    }

    return HTTP2_SUCCESS;
}

void http2PingFramePayloadDeallocate(Http2PingFramePayloadT *HTTP2_CONST_RESTRICT_SPECIFIER http2PingFramePayload) {
    http2Free(http2PingFramePayload);
}

Http2ReturnCodeT http2GoawayFramePayloadAllocate(Http2GoawayFramePayloadT **http2GoawayFramePayload,
                                                 size_t frameLength) {
    (*http2GoawayFramePayload) = http2Calloc(sizeof(Http2GoawayFramePayloadT));

    if (*http2GoawayFramePayload == NULL) {
        return HTTP2_NO_MEMORY;
    }

    if (frameLength > HTTP2_GOAWAY_MINIMUM_PAYLOAD_LEN) {
        (*http2GoawayFramePayload)->additionalDebugData = http2Malloc(frameLength - HTTP2_GOAWAY_MINIMUM_PAYLOAD_LEN);
    }

    return HTTP2_SUCCESS;
}

void
http2GoawayFramePayloadDeallocate(Http2GoawayFramePayloadT *HTTP2_CONST_RESTRICT_SPECIFIER http2GoawayFramePayload) {
    if (http2GoawayFramePayload->additionalDebugData != NULL) {
        http2Free(http2GoawayFramePayload->additionalDebugData);
    }

    http2Free(http2GoawayFramePayload);
}

Http2ReturnCodeT http2WindowUpdatePayloadAllocate(Http2WindowUpdatePayloadT **http2WindowUpdatePayload) {
    (*http2WindowUpdatePayload) = http2Malloc(sizeof(Http2WindowUpdatePayloadT));

    if (*http2WindowUpdatePayload == NULL) {
        return HTTP2_NO_MEMORY;
    }

    return HTTP2_SUCCESS;
}

void
http2WindowUpdatePayloadDeallocate(Http2WindowUpdatePayloadT *HTTP2_CONST_RESTRICT_SPECIFIER http2WindowUpdatePayload) {
    http2Free(http2WindowUpdatePayload);
}

Http2ReturnCodeT http2ContinuationPayloadAllocate(Http2ContinuationPayloadT **http2ContinuationPayload,
                                                  const uint32_t frameLength) {
    (*http2ContinuationPayload) = http2Malloc(sizeof(Http2ContinuationPayloadT));

    if (*http2ContinuationPayload == NULL) {
        return HTTP2_NO_MEMORY;
    }

    (*http2ContinuationPayload)->headerBlockFragment = http2Malloc(frameLength);

    if (*http2ContinuationPayload == NULL) {
        http2Free(*http2ContinuationPayload);
        return HTTP2_NO_MEMORY;
    }

    return HTTP2_SUCCESS;
}

void
http2ContinuationPayloadDeallocate(Http2ContinuationPayloadT *HTTP2_CONST_RESTRICT_SPECIFIER http2ContinuationPayload) {
    http2Free(http2ContinuationPayload->headerBlockFragment);
    http2Free(http2ContinuationPayload);
}

void
http2FramePayloadInitDefaultSettings(Http2SettingsFramePayloadT *HTTP2_CONST_RESTRICT_SPECIFIER settingsFramePayload) {
    settingsFramePayload->maxHeaderListSize = DEFAULT_SETTING_MAX_HEADER_LIST_SIZE;
    settingsFramePayload->maxFrameSize = DEFAULT_SETTING_MAX_FRAME_SIZE;
    settingsFramePayload->initialWindowSize = DEFAULT_SETTING_INITIAL_WINDOW_SIZE;
    settingsFramePayload->enablePush = DEFAULT_SETTING_ENABLE_PUSH;
    settingsFramePayload->maxConcurrentStreams = DEFAULT_SETTING_MAX_CONCURRENT_STREAMS;
    settingsFramePayload->headerTableSize = DEFAULT_SETTING_HEADER_TABLE_SIZE;
}
