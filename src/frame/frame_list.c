/*
MIT License
-----------

Copyright (c) 2020 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <nshttp2/stddef.h>
#include <nshttp2/frame_list.h>
#include <string.h>

static void http2FrameListElementsDeallocate(Http2FrameListT *HTTP2_CONST_RESTRICT_SPECIFIER http2FrameList,
                                             const uint8_t settings) {
    Http2FrameListElementT *http2FrameListElement = http2FrameList->head;

    while (http2FrameListElement != NULL) {
        Http2FrameListElementT *currentElement;

        if (settings & HTTP2_FRAME_LIST_DEALLOCATE_FRAMES) {
            http2FrameDeallocate(http2FrameListElement->http2Frame);
        }

        currentElement = http2FrameListElement;
        http2FrameListElement = http2FrameListElement->next;
        http2Free(currentElement);
    }
}

void http2FrameListDeallocate(Http2FrameListT *HTTP2_CONST_RESTRICT_SPECIFIER http2FrameList,
                              const uint8_t settings) {
    if (http2FrameList == NULL) {
        return;
    }

    http2FrameListElementsDeallocate(http2FrameList, settings);

    if (!(settings & HTTP2_FRAME_LIST_IS_STACK_ALLOCATED)) {
        http2Free(http2FrameList);
    }
}

Http2FrameListReturnCodeT http2FrameListAllocateEmpty(Http2FrameListT **http2FrameList) {
    *http2FrameList = http2Calloc(sizeof(Http2FrameListT));

    if (http2FrameList == NULL) {
        return HTTP2_FRAME_LIST_NOT_ENOUGH_MEMORY;
    }

    return HTTP2_FRAME_LIST_SUCCESS;
}

Http2FrameListReturnCodeT http2FrameListAllocate(Http2FrameListT **http2FrameList,
                                                 Http2FrameT *HTTP2_CONST_RESTRICT_SPECIFIER http2Frame) {
    Http2FrameListElementT *http2FrameListElement;

    *http2FrameList = http2Malloc(sizeof(Http2FrameListT));

    if (http2FrameList == NULL) {
        return HTTP2_FRAME_LIST_NOT_ENOUGH_MEMORY;
    }

    http2FrameListElement = http2Malloc(sizeof(Http2FrameListElementT));

    if (http2FrameListElement == NULL) {
        http2Free(http2FrameList);
        return HTTP2_FRAME_LIST_NOT_ENOUGH_MEMORY;
    }

    http2FrameListElement->http2Frame = http2Frame;
    http2FrameListElement->next = NULL;
    http2FrameListElement->previous = NULL;

    (*http2FrameList)->head = http2FrameListElement;
    (*http2FrameList)->tail = http2FrameListElement;
    (*http2FrameList)->length = 1u;
    (*http2FrameList)->requiredBufferSize = HTTP2_FRAME_HEADER_SIZE + http2Frame->header.length;

    return HTTP2_FRAME_LIST_SUCCESS;
}

Http2FrameListReturnCodeT http2FrameListAppend(Http2FrameListT *HTTP2_CONST_RESTRICT_SPECIFIER http2FrameList,
                                               Http2FrameT *HTTP2_CONST_RESTRICT_SPECIFIER http2Frame) {
    Http2FrameListElementT *http2FrameListElement = http2Malloc(sizeof(Http2FrameListElementT));

    if (http2FrameListElement == NULL) {
        return HTTP2_FRAME_LIST_NOT_ENOUGH_MEMORY;
    }

    http2FrameListElement->http2Frame = http2Frame;
    http2FrameListElement->next = NULL;
    http2FrameListElement->previous = http2FrameList->tail;

    switch (http2FrameList->length) {
        case 0u:
            http2FrameList->head = http2FrameListElement;
            http2FrameList->tail = http2FrameListElement;
            http2FrameList->length = 1u;
            break;
        case 1u:
            http2FrameList->tail->next = http2FrameListElement;
            http2FrameList->tail = http2FrameListElement;
            http2FrameList->length = 2u;
            break;
        default:
            http2FrameList->tail->next = http2FrameListElement;
            http2FrameList->tail = http2FrameListElement;
            ++http2FrameList->length;
            break;
    }

    http2FrameList->requiredBufferSize += HTTP2_FRAME_HEADER_SIZE + http2Frame->header.length;

    return HTTP2_FRAME_LIST_SUCCESS;
}

void http2FrameListJoin(Http2FrameListT *HTTP2_CONST_RESTRICT_SPECIFIER target,
                        Http2FrameListT *HTTP2_CONST_RESTRICT_SPECIFIER source) {
    if (source == NULL || source->length == 0) {
        return;
    }

    if (target->length == 0) {
        target->head = source->head;
        target->tail = source->tail;
        target->length = source->length;
        target->requiredBufferSize = source->requiredBufferSize;
    } else {
        target->tail->next = source->head;
        target->tail = source->tail;
        target->length += source->length;
        target->requiredBufferSize += source->requiredBufferSize;
    }

    http2Free(source);
}

Http2FrameListReturnCodeT http2FrameListFromBytes(uint8_t *const buffer,
                                                  const bool isClient,
                                                  const size_t filledLength,
                                                  Http2FrameListT **http2FrameList,
                                                  size_t *parsedBuffer) {
    Http2FrameHeaderT http2FrameHeader;
    uint8_t *buffer_ptr;
    size_t buffer_remaining_length;
    Http2FrameT *http2Frame;
    size_t buffer_movement;
    Http2FrameListT *next_http2_frame_list;

    if (filledLength < HTTP2_FRAME_HEADER_SIZE) {
        return HTTP2_FRAME_LIST_BUFFER_TOO_SHORT;
    }

    /* Some buffers send padding at the end to hide the message contents,
     * so we end parsing if this is the case
     */
    if (!http2IsNonZeroHeaderBuffer(buffer)) {
        return HTTP2_FRAME_LIST_BUFFER_PROCESSED;
    }

    http2FrameHeaderFromBytes(&http2FrameHeader, buffer);

    buffer_ptr = buffer;
    buffer_remaining_length = filledLength;

    if ((buffer_remaining_length - HTTP2_FRAME_HEADER_SIZE) < http2FrameHeader.length) {
        return HTTP2_FRAME_LIST_BUFFER_DOES_NOT_HAVE_WHOLE_BODY;
    }

    if ((buffer_remaining_length - HTTP2_FRAME_HEADER_SIZE) < http2FrameHeader.length) {
        return HTTP2_FRAME_LIST_BUFFER_HAS_ONLY_HEADER;
    }

    http2Frame = NULL;

    if (http2FrameHeader.type > CONTINUATION_FRAME_TYPE) {
        http2Frame = http2Calloc(sizeof(Http2FrameT));
    } else {
        if (http2FrameAllocate(&http2Frame,
                               http2FrameHeader.type,
                               http2FrameHeader.flags,
                               http2FrameHeader.length) == HTTP2_NO_MEMORY) {
            return HTTP2_FRAME_LIST_NOT_ENOUGH_MEMORY;
        }
    }

    http2Frame->header = http2FrameHeader;

    http2FrameFromBytesHeaderSet(http2Frame, isClient, buffer);

    if (http2FrameListAllocate(http2FrameList, http2Frame) == HTTP2_FRAME_LIST_NOT_ENOUGH_MEMORY) {
        http2FrameDeallocate(http2Frame);
        return HTTP2_FRAME_LIST_NOT_ENOUGH_MEMORY;
    }

    buffer_movement = HTTP2_FRAME_HEADER_SIZE + http2FrameHeader.length;
    buffer_ptr += buffer_movement;
    buffer_remaining_length -= buffer_movement;
    *parsedBuffer += buffer_movement;

    if (buffer_remaining_length == 0) {
        return HTTP2_FRAME_LIST_BUFFER_PROCESSED;
    }

    next_http2_frame_list = NULL;

    switch (http2FrameListFromBytes(buffer_ptr,
                                    isClient,
                                    buffer_remaining_length,
                                    &next_http2_frame_list,
                                    parsedBuffer)) {
        case HTTP2_FRAME_LIST_BUFFER_TOO_SHORT:
            if (next_http2_frame_list != NULL) {
                http2FrameListJoin(*http2FrameList, next_http2_frame_list);
            }

            return HTTP2_FRAME_LIST_BUFFER_TOO_SHORT;
        case HTTP2_FRAME_LIST_BUFFER_HAS_ONLY_HEADER:
            if (next_http2_frame_list != NULL) {
                http2FrameListJoin(next_http2_frame_list, *http2FrameList);
            }

            return HTTP2_FRAME_LIST_BUFFER_HAS_ONLY_HEADER;
        case HTTP2_FRAME_LIST_BUFFER_DOES_NOT_HAVE_WHOLE_BODY:
            if (next_http2_frame_list != NULL) {
                http2FrameListJoin(*http2FrameList, next_http2_frame_list);
            }

            return HTTP2_FRAME_LIST_BUFFER_DOES_NOT_HAVE_WHOLE_BODY;
        case HTTP2_FRAME_LIST_BUFFER_PROCESSED:
            if (next_http2_frame_list != NULL) {
                http2FrameListJoin(*http2FrameList, next_http2_frame_list);
            }

            return HTTP2_FRAME_LIST_BUFFER_PROCESSED;
        case HTTP2_FRAME_LIST_NOT_ENOUGH_MEMORY:
            http2FrameListDeallocate(*http2FrameList, HTTP2_FRAME_LIST_DEALLOCATE_FRAMES);
            return HTTP2_FRAME_LIST_NOT_ENOUGH_MEMORY;
        default:
            break;
    }

    return HTTP2_FRAME_LIST_BUFFER_PROCESSED;
}

Http2FrameListReturnCodeT http2FrameListToBytes(Http2BufferT **buffer,
                                                Http2FrameListT *HTTP2_CONST_RESTRICT_SPECIFIER http2FrameList) {
    Http2FrameListElementT *current;
    uint8_t *buffer_ptr;

    if (http2FrameList->length == 0u) {
        *buffer = NULL;
        return HTTP2_FRAME_LIST_EMPTY;
    }

    switch (http2BufferAllocate(buffer, http2FrameList->requiredBufferSize)) {
        case HTTP2_NO_MEMORY:
            return HTTP2_FRAME_LIST_NOT_ENOUGH_MEMORY;
        default:
            break;
    }

    current = http2FrameList->head;
    buffer_ptr = (*buffer)->buffer;

    while (current != NULL) {
        http2FrameToBytes(current->http2Frame, buffer_ptr, NULL);
        buffer_ptr += (current->http2Frame->header.length + HTTP2_FRAME_HEADER_SIZE);
        current = current->next;
    }

    (*buffer)->length = http2FrameList->requiredBufferSize;

    return HTTP2_FRAME_LIST_SUCCESS;
}

void http2FrameListElementRemove(Http2FrameListElementT *HTTP2_CONST_RESTRICT_SPECIFIER element,
                                 Http2FrameListT *HTTP2_CONST_RESTRICT_SPECIFIER frameList,
                                 const bool deallocateFrame) {
    if (deallocateFrame) {
        http2FrameDeallocate(element->http2Frame);
    }

    switch (frameList->length) {
        case 0u:
            return;
        case 1u:
            frameList->requiredBufferSize = 0u;
            frameList->tail = NULL;
            frameList->head = NULL;
            frameList->length = 0u;
            break;
        default:
            if (element == frameList->tail) {
                frameList->tail = frameList->tail->previous;
            }

            if (element == frameList->head) {
                frameList->head = frameList->head->next;
            }

            if (element->previous != NULL) {
                element->previous->next = element->next;
            }

            if (element->next != NULL) {
                element->next->previous = element->previous;
            }

            frameList->requiredBufferSize -= (element->http2Frame->header.length + HTTP2_FRAME_HEADER_SIZE);

            --frameList->length;
    }

    http2Free(element);
}

void http2DataToDataFrames(Http2FrameListT **frameList,
                           uint8_t *buffer,
                           const size_t bufferLength,
                           const uint32_t streamId,
                           const size_t maxFrameSize) {
    Http2FrameListT *frameListPtr;

    if ((buffer == NULL) || (bufferLength == 0u)) {
        *frameList = NULL;
    }

    frameListPtr = http2Calloc(sizeof(Http2FrameListT));

    if (bufferLength <= maxFrameSize) {
        uint8_t *dataBuffer;
        Http2FrameT *frame = http2Malloc(sizeof(Http2FrameT));
        frame->header.length = (uint32_t) bufferLength;
        frame->header.streamId = streamId;
        frame->header.flags = END_STREAM_FRAME_FLAG;
        frame->header.type = DATA_FRAME_TYPE;
        dataBuffer = http2Malloc(bufferLength);
        memcpy(dataBuffer, buffer, bufferLength);
        frame->payload.http2DataFramePayload = dataBuffer;
        http2FrameListAppend(frameListPtr, frame);
    } else {
        Http2FrameT *frame;
        size_t remainingBuffer = bufferLength;
        uint8_t *bufPtr = buffer;
        uint8_t *dataBuffer;

        while (remainingBuffer > maxFrameSize) {
            uint8_t *dataBuffer;
            Http2FrameT *frame = http2Malloc(sizeof(Http2FrameT));
            frame->header.length = (uint32_t) maxFrameSize;
            frame->header.streamId = streamId;
            frame->header.flags = 0u;
            frame->header.type = DATA_FRAME_TYPE;
            dataBuffer = http2Malloc(maxFrameSize);
            memcpy(dataBuffer, bufPtr, maxFrameSize);
            remainingBuffer -= maxFrameSize;
            bufPtr += maxFrameSize;
            http2FrameListAppend(frameListPtr, frame);
        }

        frame = http2Malloc(sizeof(Http2FrameT));
        frame->header.length = (uint32_t) remainingBuffer;
        frame->header.streamId = streamId;
        frame->header.flags = END_STREAM_FRAME_FLAG;
        frame->header.type = DATA_FRAME_TYPE;
        dataBuffer = http2Malloc(remainingBuffer);
        memcpy(dataBuffer, bufPtr, remainingBuffer);
        http2FrameListAppend(frameListPtr, frame);
    }

    *frameList = frameListPtr;
}
