/*
MIT License
-----------

Copyright (c) 2020 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <nshttp2/stddef.h>
#include <nshttp2/frame.h>
#include <string.h>

#ifdef __unix__

#include <arpa/inet.h>

#elif defined(_WIN32)

#include <winsock2.h>

#pragma comment(lib, "Ws2_32. lib")

#endif

Http2FrameReturnCodeT http2FrameToBytes(const Http2FrameT *HTTP2_CONST_RESTRICT_SPECIFIER http2Frame,
                                        uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer,
                                        Http2FramePaddingT *HTTP2_CONST_RESTRICT_SPECIFIER http2FramePadding) {
    /* First we encode the header */
    http2FrameHeaderToBytes(&http2Frame->header, buffer);

    if (http2Frame->header.length == 0u) {
        return FRAME_SUCCESS;
    }

    /* Next we encode the payload of the frame */
    switch (http2Frame->header.type) {
        case DATA_FRAME_TYPE:
            http2DataFramePayloadToBytes(http2Frame->payload.http2DataFramePayload,
                                         buffer + HTTP2_FRAME_HEADER_SIZE,
                                         http2Frame->header.length,
                                         http2FramePadding);
            break;
        case HEADERS_FRAME_TYPE:
            http2HeadersFramePayloadToBytes(http2Frame->payload.http2HeadersFramePayload,
                                            buffer + HTTP2_FRAME_HEADER_SIZE,
                                            http2Frame->header.length,
                                            http2FramePadding,
                                            http2Frame->header.flags & PRIORITY_FRAME_FLAG);
            break;
        case PRIORITY_FRAME_TYPE:
            http2PriorityFramePayloadToBytes(http2Frame->payload.http2PriorityFramePayload,
                                             buffer + HTTP2_FRAME_HEADER_SIZE);
            break;
        case RST_STREAM_FRAME_TYPE:
            http2RstStreamFramePayloadToBytes(http2Frame->payload.http2RstStreamFramePayload,
                                              buffer + HTTP2_FRAME_HEADER_SIZE);
            break;
        case SETTINGS_FRAME_TYPE:
            http2SettingsFramePayloadToBytes(http2Frame->payload.http2SettingsFramePayload,
                                             buffer + HTTP2_FRAME_HEADER_SIZE);
            break;
        case PUSH_PROMISE_FRAME_TYPE:
            http2PushPromiseFramePayloadToBytes(http2Frame->payload.http2PushPromiseFramePayload,
                                                buffer + HTTP2_FRAME_HEADER_SIZE,
                                                http2Frame->header.length,
                                                http2FramePadding);
            break;
        case PING_FRAME_TYPE:
            http2PingFramePayloadToBytes(http2Frame->payload.http2PingFramePayload,
                                         buffer + HTTP2_FRAME_HEADER_SIZE);
            break;
        case GOAWAY_FRAME_TYPE:
            http2GoawayFramePayloadToBytes(http2Frame->payload.http2GoawayFramePayload,
                                           buffer + HTTP2_FRAME_HEADER_SIZE,
                                           http2Frame->header.length);
            break;
        case WINDOW_UPDATE_FRAME_TYPE:
            http2WindowUpdatePayloadToBytes(http2Frame->payload.http2WindowUpdatePayload,
                                            buffer + HTTP2_FRAME_HEADER_SIZE);
            break;
        case CONTINUATION_FRAME_TYPE:
            http2ContinuationPayloadToBytes(http2Frame->payload.http2ContinuationPayload,
                                            buffer + HTTP2_FRAME_HEADER_SIZE,
                                            http2Frame->header.length);
            break;
        default:
            return UNSUPPORTED_FRAME_TYPE;
    }

    return FRAME_SUCCESS;
}

Http2FrameReturnCodeT http2FrameToBytesWithAllocation(Http2FrameT *HTTP2_CONST_RESTRICT_SPECIFIER http2Frame,
                                                      uint8_t **buffer,
                                                      size_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer_size,
                                                      Http2FramePaddingT *HTTP2_CONST_RESTRICT_SPECIFIER http2FramePadding) {
    const size_t buffer_length = http2Frame->header.length + HTTP2_FRAME_HEADER_SIZE;
    *buffer = http2Malloc(buffer_length);

    if (*buffer == NULL) {
        return FRAME_NO_MEMORY;
    } else {
        *buffer_size = buffer_length;
        return http2FrameToBytes(http2Frame, *buffer, http2FramePadding);
    }
}

Http2ReturnCodeT http2FrameFromBytesHeaderSet(Http2FrameT *HTTP2_CONST_RESTRICT_SPECIFIER http2Frame,
                                              const bool isClient,
                                              uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer) {
    const bool has_padding = http2Frame->header.flags & PADDED_FRAME_FLAG;

    switch (http2Frame->header.type) {
        case DATA_FRAME_TYPE:
            http2DataFramePayloadFromBytes(http2Frame->payload.http2DataFramePayload,
                                           &http2Frame->header.length,
                                           buffer + HTTP2_FRAME_HEADER_SIZE,
                                           http2Frame->header.length,
                                           has_padding);
            break;
        case HEADERS_FRAME_TYPE:
            http2HeadersFramePayloadFromBytes(http2Frame->payload.http2HeadersFramePayload,
                                              &http2Frame->header.length,
                                              buffer + HTTP2_FRAME_HEADER_SIZE,
                                              http2Frame->header.length,
                                              http2Frame->header.flags & PADDED_FRAME_FLAG,
                                              http2Frame->header.flags & PRIORITY_FRAME_FLAG);
            break;
        case PRIORITY_FRAME_TYPE:
            http2PriorityFramePayloadFromBytes(http2Frame->payload.http2PriorityFramePayload,
                                               buffer + HTTP2_FRAME_HEADER_SIZE);
            break;
        case RST_STREAM_FRAME_TYPE:
            http2RstStreamFramePayloadFromBytes(http2Frame->payload.http2RstStreamFramePayload,
                                                buffer + HTTP2_FRAME_HEADER_SIZE);
            break;
        case SETTINGS_FRAME_TYPE:
            if (http2Frame->header.length == 0u) {
                http2Frame->payload.http2SettingsFramePayload = NULL;
                break;
            }

            http2SettingsFramePayloadFromBytes(http2Frame->payload.http2SettingsFramePayload,
                                               buffer + HTTP2_FRAME_HEADER_SIZE,
                                               http2Frame->header.length,
                                               isClient);
            break;
        case PUSH_PROMISE_FRAME_TYPE:
            http2PushPromiseFramePayloadFromBytes(http2Frame->payload.http2PushPromiseFramePayload,
                                                  buffer + HTTP2_FRAME_HEADER_SIZE,
                                                  http2Frame->header.length,
                                                  http2Frame->header.flags & PADDED_FRAME_FLAG);
            break;
        case PING_FRAME_TYPE:
            http2PingFramePayloadFromBytes(http2Frame->payload.http2PingFramePayload,
                                           buffer + HTTP2_FRAME_HEADER_SIZE);
            break;
        case GOAWAY_FRAME_TYPE:
            http2GoawayFramePayloadFromBytes(http2Frame->payload.http2GoawayFramePayload,
                                             buffer + HTTP2_FRAME_HEADER_SIZE,
                                             http2Frame->header.length);
            break;
        case WINDOW_UPDATE_FRAME_TYPE:
            http2WindowUpdatePayloadFromBytes(http2Frame->payload.http2WindowUpdatePayload,
                                              buffer + HTTP2_FRAME_HEADER_SIZE);
            break;
        case CONTINUATION_FRAME_TYPE:
            http2ContinuationPayloadFromBytes(http2Frame->payload.http2ContinuationPayload,
                                              buffer + HTTP2_FRAME_HEADER_SIZE,
                                              http2Frame->header.length);
            break;
        default:
            return HTTP2_UNSUPPORTED_FRAME_TYPE;
    }

    return HTTP2_SUCCESS;
}

Http2ReturnCodeT http2FrameFromBytes(Http2FrameT *HTTP2_CONST_RESTRICT_SPECIFIER http2Frame,
                                     const bool isClient,
                                     uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer) {
    /* First we decode the header */
    http2FrameHeaderFromBytes(&http2Frame->header, buffer);

    /* Next we decode the payload */
    return http2FrameFromBytesHeaderSet(http2Frame, isClient, buffer);
}

void http2FrameHeaderToBytes(const Http2FrameHeaderT *HTTP2_CONST_RESTRICT_SPECIFIER http2FrameHeader,
                             uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer) {
    uint32_t stream_id_bytes;
    uint32_t length_bytes;

    length_bytes = htonl(http2FrameHeader->length);

    memcpy(buffer, (((uint8_t *) &length_bytes) + 1), sizeof(uint8_t) * 3);
    memcpy(buffer + sizeof(uint8_t) * 3, &http2FrameHeader->type, sizeof(uint8_t));
    memcpy(buffer + sizeof(uint8_t) * 4, &http2FrameHeader->flags, sizeof(uint8_t));
    stream_id_bytes = htonl(http2FrameHeader->streamId);
    memcpy(buffer + sizeof(uint8_t) * 5, &stream_id_bytes, sizeof(uint32_t));
}

void http2FrameHeaderFromBytes(Http2FrameHeaderT *HTTP2_CONST_RESTRICT_SPECIFIER http2FrameHeader,
                               uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer) {
    uint32_t length_bytes;

    memcpy(&http2FrameHeader->type, buffer + sizeof(uint8_t) * 3, sizeof(uint8_t));

    length_bytes = 0u;
    memcpy(((uint8_t *) &length_bytes) + sizeof(uint8_t), buffer, sizeof(uint8_t) * 3);
    http2FrameHeader->length = ntohl(length_bytes);

    memcpy(&http2FrameHeader->flags, buffer + sizeof(uint8_t) * 4, sizeof(uint8_t));
    http2FrameHeader->streamId = ntohl(*(uint32_t *) (buffer + sizeof(uint8_t) * 5));
}

bool http2IsValidConnectionPreface(uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer) {
    return stringStartWithSubstring((const char *) buffer, HTTP2_CONNECTION_PREFACE, HTTP2_CONNECTION_PREFACE_LEN);
}

Http2ReturnCodeT http2FramePayloadAllocate(Http2FramePayloadT *HTTP2_CONST_RESTRICT_SPECIFIER http2FramePayload,
                                           const uint8_t flags,
                                           const uint8_t type,
                                           const uint32_t frameLength) {
    if (frameLength == 0u) {
        http2FramePayload->http2DataFramePayload = NULL;
        return HTTP2_SUCCESS;
    }

    switch (type) {
        case DATA_FRAME_TYPE:
            http2FramePayload->http2DataFramePayload = http2Malloc(frameLength);

            if (http2FramePayload->http2DataFramePayload == NULL) {
                return HTTP2_NO_MEMORY;
            }

            break;
        case HEADERS_FRAME_TYPE:
            return http2HeadersFramePayloadAllocate(&http2FramePayload->http2HeadersFramePayload,
                                                    flags & PRIORITY_FRAME_FLAG,
                                                    frameLength);
        case PRIORITY_FRAME_TYPE:
            return http2PriorityFramePayloadAllocate(&http2FramePayload->http2PriorityFramePayload);
        case RST_STREAM_FRAME_TYPE:
            return http2RstStreamFramePayloadAllocate(&http2FramePayload->http2RstStreamFramePayload);
        case SETTINGS_FRAME_TYPE:
            return http2SettingsFramePayloadAllocate(&http2FramePayload->http2SettingsFramePayload);
        case PUSH_PROMISE_FRAME_TYPE:
            return http2PushPromiseFramePayloadAllocate(&http2FramePayload->http2PushPromiseFramePayload,
                                                        frameLength);
        case PING_FRAME_TYPE:
            return http2PingFramePayloadAllocate(&http2FramePayload->http2PingFramePayload);
        case GOAWAY_FRAME_TYPE:
            return http2GoawayFramePayloadAllocate(&http2FramePayload->http2GoawayFramePayload,
                                                   frameLength);
        case WINDOW_UPDATE_FRAME_TYPE:
            return http2WindowUpdatePayloadAllocate(&http2FramePayload->http2WindowUpdatePayload);
        case CONTINUATION_FRAME_TYPE:
            return http2ContinuationPayloadAllocate(&http2FramePayload->http2ContinuationPayload,
                                                    frameLength);
        default:
            return HTTP2_UNSUPPORTED_FRAME_TYPE;
    }

    return HTTP2_SUCCESS;
}

void http2FramePayloadDeallocate(Http2FramePayloadT *HTTP2_CONST_RESTRICT_SPECIFIER http2FramePayload,
                                 const uint8_t type) {
    if (http2FramePayload == NULL) {
        return;
    }

    switch (type) {
        case DATA_FRAME_TYPE:
            http2Free(http2FramePayload->http2DataFramePayload);
            break;
        case HEADERS_FRAME_TYPE:
            http2HeadersFramePayloadDeallocate(http2FramePayload->http2HeadersFramePayload);
            break;
        case PRIORITY_FRAME_TYPE:
            http2PriorityFramePayloadDeallocate(http2FramePayload->http2PriorityFramePayload);
            break;
        case RST_STREAM_FRAME_TYPE:
            http2RstStreamFramePayloadDeallocate(http2FramePayload->http2RstStreamFramePayload);
            break;
        case SETTINGS_FRAME_TYPE:
            http2SettingsFramePayloadDeallocate(http2FramePayload->http2SettingsFramePayload);
            break;
        case PUSH_PROMISE_FRAME_TYPE:
            http2PushPromiseFramePayloadDeallocate(http2FramePayload->http2PushPromiseFramePayload);
            break;
        case PING_FRAME_TYPE:
            http2PingFramePayloadDeallocate(http2FramePayload->http2PingFramePayload);
            break;
        case GOAWAY_FRAME_TYPE:
            http2GoawayFramePayloadDeallocate(http2FramePayload->http2GoawayFramePayload);
            break;
        case WINDOW_UPDATE_FRAME_TYPE:
            http2WindowUpdatePayloadDeallocate(http2FramePayload->http2WindowUpdatePayload);
            break;
        case CONTINUATION_FRAME_TYPE:
            http2ContinuationPayloadDeallocate(http2FramePayload->http2ContinuationPayload);
            break;
        default:
            break;
    }
}

void http2FrameDeallocate(Http2FrameT *HTTP2_CONST_RESTRICT_SPECIFIER http2Frame) {
    http2FramePayloadDeallocate(&http2Frame->payload, http2Frame->header.type);
    http2Free(http2Frame);
}

Http2ReturnCodeT http2FrameAllocate(Http2FrameT **http2Frame,
                                    const uint8_t type,
                                    const uint8_t flags,
                                    const uint32_t frameLength) {
    *http2Frame = http2Malloc(sizeof(Http2FrameT));

    if (*http2Frame == NULL) {
        return HTTP2_NO_MEMORY;
    }

    (*http2Frame)->header.type = type;
    (*http2Frame)->header.length = frameLength;
    (*http2Frame)->header.flags = flags;

    switch (http2FramePayloadAllocate(&(*http2Frame)->payload, flags, type, frameLength)) {
        case HTTP2_NO_MEMORY:
            http2Free(*http2Frame);
            return HTTP2_NO_MEMORY;
        case HTTP2_UNSUPPORTED_FRAME_TYPE:
            http2Free(*http2Frame);
            return HTTP2_UNSUPPORTED_FRAME_TYPE;
        default:
            return HTTP2_SUCCESS;
    }
}

bool http2IsNonZeroHeaderBuffer(uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer) {
    const uint8_t emptyArray[HTTP2_FRAME_HEADER_SIZE] = {0};

    /* If the start of the buffer is not zeroes we consider this valid */
    return memcmp(buffer, emptyArray, HTTP2_FRAME_HEADER_SIZE) != 0;
}

char *http2FrameTypeName(uint8_t frameTypeId) {
    switch (frameTypeId) {
        case DATA_FRAME_TYPE:
            return DATA_FRAME_NAME;
        case HEADERS_FRAME_TYPE:
            return HEADERS_FRAME_NAME;
        case PRIORITY_FRAME_TYPE:
            return PRIORITY_FRAME_NAME;
        case RST_STREAM_FRAME_TYPE:
            return RST_STREAM_FRAME_NAME;
        case SETTINGS_FRAME_TYPE:
            return SETTINGS_FRAME_NAME;
        case PUSH_PROMISE_FRAME_TYPE:
            return PUSH_PROMISE_FRAME_NAME;
        case PING_FRAME_TYPE:
            return PING_FRAME_NAME;
        case GOAWAY_FRAME_TYPE:
            return GOAWAY_FRAME_NAME;
        case WINDOW_UPDATE_FRAME_TYPE:
            return WINDOW_UPDATE_FRAME_NAME;
        case CONTINUATION_FRAME_TYPE:
            return CONTINUATION_FRAME_NAME;
        default:
            return "invalid";
    }
}

bool http2FrameIsStreamFrame(Http2FrameT *HTTP2_CONST_RESTRICT_SPECIFIER http2Frame) {
    switch (http2Frame->header.type) {
        case GOAWAY_FRAME_TYPE:
        case SETTINGS_FRAME_TYPE:
        case PING_FRAME_TYPE:
        case WINDOW_UPDATE_FRAME_TYPE:
            /* We want to only consider those with stream ids of 0 */
            return http2Frame->header.streamId == 0u;
        default:
            return false;
    }
}

bool http2FrameIsNonControlFrame(Http2FrameT *HTTP2_CONST_RESTRICT_SPECIFIER http2Frame) {
    switch (http2Frame->header.type) {
        case DATA_FRAME_TYPE:
        case HEADERS_FRAME_TYPE:
        case PRIORITY_FRAME_TYPE:
        case RST_STREAM_FRAME_TYPE:
        case PUSH_PROMISE_FRAME_TYPE:
        case CONTINUATION_FRAME_TYPE:
        case WINDOW_UPDATE_FRAME_TYPE:
            /* We want to only consider those with stream ids larger than 0 */
            return http2Frame->header.streamId > 0u;
        default:
            return false;
    }
}

HTTP2_INLINE_SPECIFIER bool http2FrameHasPriority(Http2FrameT *HTTP2_CONST_RESTRICT_SPECIFIER http2Frame) {
    return http2Frame->header.flags & PRIORITY_FRAME_FLAG;
}

HTTP2_INLINE_SPECIFIER bool http2FrameIsLastHeader(Http2FrameT *HTTP2_CONST_RESTRICT_SPECIFIER http2Frame) {
    return http2Frame->header.flags & END_HEADERS_FRAME_FLAG;
}

bool http2FrameIsSupported(Http2FrameT *HTTP2_CONST_RESTRICT_SPECIFIER http2Frame) {
    switch (http2Frame->header.type) {
        case DATA_FRAME_TYPE:
            switch (http2Frame->header.flags) {
                case DEFAULT_FRAME_FLAG:
                case PADDED_FRAME_FLAG:
                case END_STREAM_FRAME_FLAG:
                case PADDED_FRAME_FLAG | END_STREAM_FRAME_FLAG:
                    return true;
                default:
                    return false;
            }
        case HEADERS_FRAME_TYPE:
            switch (http2Frame->header.flags) {
                case DEFAULT_FRAME_FLAG:
                case END_HEADERS_FRAME_FLAG:
                case END_STREAM_FRAME_FLAG:
                case PADDED_FRAME_FLAG:
                case PRIORITY_FRAME_FLAG:
                case END_HEADERS_FRAME_FLAG | END_STREAM_FRAME_FLAG:
                case END_HEADERS_FRAME_FLAG | PADDED_FRAME_FLAG:
                case END_HEADERS_FRAME_FLAG | PRIORITY_FRAME_FLAG:
                case END_STREAM_FRAME_FLAG | PADDED_FRAME_FLAG:
                case END_STREAM_FRAME_FLAG | PRIORITY_FRAME_FLAG:
                case PADDED_FRAME_FLAG | PRIORITY_FRAME_FLAG:
                case END_HEADERS_FRAME_FLAG | END_STREAM_FRAME_FLAG | PADDED_FRAME_FLAG:
                case END_HEADERS_FRAME_FLAG | END_STREAM_FRAME_FLAG | PRIORITY_FRAME_FLAG:
                case END_STREAM_FRAME_FLAG | PADDED_FRAME_FLAG | PRIORITY_FRAME_FLAG:
                case END_HEADERS_FRAME_FLAG | END_STREAM_FRAME_FLAG | PADDED_FRAME_FLAG | PRIORITY_FRAME_FLAG:
                    return true;
                default:
                    return false;
            }
        case PRIORITY_FRAME_TYPE:
            switch (http2Frame->header.flags) {
                case ACK_FRAME_FLAG:
                case DEFAULT_FRAME_FLAG:
                    return true;
                default:
                    return false;
            }
        case RST_STREAM_FRAME_TYPE:
            switch (http2Frame->header.flags) {
                case DEFAULT_FRAME_FLAG:
                    return true;
                default:
                    return false;
            }
        case SETTINGS_FRAME_TYPE:
            switch (http2Frame->header.flags) {
                case DEFAULT_FRAME_FLAG:
                case ACK_FRAME_FLAG:
                    return true;
                default:
                    return false;
            }
        case PUSH_PROMISE_FRAME_TYPE:
            switch (http2Frame->header.flags) {
                case DEFAULT_FRAME_FLAG:
                case END_HEADERS_FRAME_FLAG:
                case PADDED_FRAME_FLAG:
                case END_HEADERS_FRAME_FLAG | PADDED_FRAME_FLAG:
                    return true;
                default:
                    return false;
            }
        case PING_FRAME_TYPE:
            switch (http2Frame->header.flags) {
                case DEFAULT_FRAME_FLAG:
                case ACK_FRAME_FLAG:
                    return true;
                default:
                    return false;
            }
        case GOAWAY_FRAME_TYPE:
        case WINDOW_UPDATE_FRAME_TYPE:
            switch (http2Frame->header.flags) {
                case DEFAULT_FRAME_FLAG:
                    return true;
                default:
                    return false;
            }
        case CONTINUATION_FRAME_TYPE:
            switch (http2Frame->header.flags) {
                case DEFAULT_FRAME_FLAG:
                case END_HEADERS_FRAME_FLAG:
                    return true;
            }
            break;
        default:
            return false;
    }

    return false;
}
