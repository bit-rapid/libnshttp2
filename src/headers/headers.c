/*
MIT License
-----------

Copyright (c) 2020 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <nshttp2/headers.h>

ssize_t http2HeadersDecode(uint8_t *bufferPtr,
                           uint32_t bufferLength,
                           Http2HeaderListT **http2HeaderList,
                           nghttp2_hd_inflater *const nghttp2Inflater) {
    size_t length = 0u;
    bool first_element = true;
    Http2HeaderListElementT *previousElement = NULL;

    *http2HeaderList = http2Calloc(sizeof(Http2HeaderListT));

    while (true) {
        nghttp2_nv nv;
        int inflateFlags = 0;

        Http2HeaderListElementT *currentHeaderElement = NULL;

        const ssize_t inflate_return_code = nghttp2_hd_inflate_hd2(nghttp2Inflater,
                                                                   &nv,
                                                                   &inflateFlags,
                                                                   bufferPtr,
                                                                   bufferLength,
                                                                   1);

        switch (inflate_return_code) {
            case NGHTTP2_ERR_NOMEM:
            case NGHTTP2_ERR_HEADER_COMP:
            case NGHTTP2_ERR_BUFFER_ERROR:
                http2HeaderListDeallocate(*http2HeaderList);
                *http2HeaderList = NULL;
                return inflate_return_code;
            default:
                bufferPtr += inflate_return_code;
                bufferLength -= inflate_return_code;
                break;
        }

        if (inflateFlags & NGHTTP2_HD_INFLATE_EMIT) {
            if (http2HeaderAllocate(&currentHeaderElement, nv.namelen, nv.valuelen) == NGHTTP2_ERR_NOMEM) {
                http2HeaderListDeallocate(*http2HeaderList);
                return NGHTTP2_ERR_NOMEM;
            }

            http2HeaderFromNghttp2Nv(&nv, currentHeaderElement->http2HeaderField);

            ++length;

            if (first_element) {
                (*http2HeaderList)->head = currentHeaderElement;
                first_element = false;
                previousElement = currentHeaderElement;
            } else {
                previousElement->next = currentHeaderElement;
                previousElement = currentHeaderElement;
            }

            currentHeaderElement = currentHeaderElement->next;
        }

        /* We are done with decoding we just need to signal the end of decompression for one header block */
        if (inflateFlags & NGHTTP2_HD_INFLATE_FINAL) {
            nghttp2_hd_inflate_end_headers(nghttp2Inflater);
            break;
        }

        /* We are done with decoding */
        if ((inflateFlags & NGHTTP2_HD_INFLATE_EMIT) == 0 && bufferLength == 0u) {
            break;
        }
    }

    previousElement->next = NULL;

    (*http2HeaderList)->length = length;
    (*http2HeaderList)->tail = previousElement;

    return EXIT_SUCCESS;
}

void http2HeaderListDeallocate(Http2HeaderListT *HTTP2_CONST_RESTRICT_SPECIFIER http2HeaderList) {
    Http2HeaderListElementT *current_header = http2HeaderList->head;

    while (current_header != NULL) {
        Http2HeaderListElementT *HTTP2_CONST_RESTRICT_SPECIFIER next_header = current_header->next;
        http2HeaderFieldDeallocate(current_header->http2HeaderField);
        http2Free(current_header);
        current_header = next_header;
    }

    http2Free(http2HeaderList);
}

void http2HeaderFromNghttp2Nv(nghttp2_nv *nv,
                              Http2HeaderFieldT *HTTP2_CONST_RESTRICT_SPECIFIER http2HeaderField) {
    memcpy(http2HeaderField->name, nv->name, nv->namelen);
    memcpy(http2HeaderField->value, nv->value, nv->valuelen);

    http2HeaderField->nameLength = nv->namelen;
    http2HeaderField->valueLength = nv->valuelen;
}

int http2HeaderAllocate(Http2HeaderListElementT **http2HeaderListElement,
                        const size_t nameLength,
                        const size_t valueLength) {
    Http2HeaderFieldT *header_field = NULL;

    (*http2HeaderListElement) = http2Calloc(sizeof(Http2HeaderListElementT));

    if ((*http2HeaderListElement) == NULL) {
        return NGHTTP2_ERR_NOMEM;
    }

    if (http2HeaderFieldAllocate(&header_field,
                                 nameLength,
                                 valueLength) == NGHTTP2_ERR_NOMEM) {
        http2Free(*http2HeaderListElement);
        return NGHTTP2_ERR_NOMEM;
    } else {
        (*http2HeaderListElement)->http2HeaderField = header_field;
        return EXIT_SUCCESS;
    }
}

int http2HeaderFieldAllocate(Http2HeaderFieldT **http2HeaderField,
                             const size_t nameLength,
                             const size_t valueLength) {
    *http2HeaderField = http2Malloc(sizeof(Http2HeaderFieldT));

    if ((*http2HeaderField) == NULL) {
        return NGHTTP2_ERR_NOMEM;
    }

    (*http2HeaderField)->nameLength = nameLength;
    (*http2HeaderField)->valueLength = valueLength;
    (*http2HeaderField)->name = http2Malloc(nameLength);

    if ((*http2HeaderField)->name == NULL) {
        http2Free(*http2HeaderField);
        return NGHTTP2_ERR_NOMEM;
    }

    (*http2HeaderField)->value = http2Malloc(valueLength);

    if ((*http2HeaderField)->value == NULL) {
        http2Free((*http2HeaderField)->name);
        http2Free(*http2HeaderField);
        return NGHTTP2_ERR_NOMEM;
    }

    return EXIT_SUCCESS;
}

void http2HeaderFieldDeallocate(Http2HeaderFieldT *HTTP2_CONST_RESTRICT_SPECIFIER http2HeaderField) {
    http2Free(http2HeaderField->name);
    http2Free(http2HeaderField->value);
    http2Free(http2HeaderField);
}

Http2ReturnCodeT http2HeadersEncode(Http2HeaderListT *HTTP2_CONST_RESTRICT_SPECIFIER http2HeaderList,
                                    Http2BufferT **buffer,
                                    nghttp2_hd_deflater *HTTP2_CONST_RESTRICT_SPECIFIER nghttp2Deflater) {
    nghttp2_nv *nv = NULL;
    size_t bufferLength;
    uint8_t *current_buffer;
    uint8_t *original_buffer;
    ssize_t return_code = http2HeadersToNvArray(http2HeaderList, &nv);

    if (return_code == NGHTTP2_ERR_NOMEM) {
        return HTTP2_NO_MEMORY;
    }

    bufferLength = nghttp2_hd_deflate_bound(nghttp2Deflater, nv, http2HeaderList->length);
    current_buffer = http2Malloc(bufferLength);

    if (current_buffer == NULL) {
        http2NvArrayDeallocate(nv, http2HeaderList->length);
        return HTTP2_NO_MEMORY;
    }

    return_code = nghttp2_hd_deflate_hd(nghttp2Deflater,
                                        current_buffer,
                                        bufferLength,
                                        nv,
                                        http2HeaderList->length);

    if (return_code == NGHTTP2_ERR_INSUFF_BUFSIZE) {
        http2NvArrayDeallocate(nv, http2HeaderList->length);
        http2Free(current_buffer);
        return HTTP2_BUFFER_INSUFFICIENT;
    }

    original_buffer = current_buffer;
    http2NvArrayDeallocate(nv, http2HeaderList->length);

    switch (return_code) {
        case NGHTTP2_ERR_HEADER_COMP:
            http2Free(current_buffer);
            return HTTP2_FAILURE;
        case NGHTTP2_ERR_NOMEM:
            http2Free(current_buffer);
            return HTTP2_NO_MEMORY;
        default:
            current_buffer = http2Realloc(current_buffer, (size_t) return_code);

            if (current_buffer == NULL) {
                http2Free(original_buffer);

                return HTTP2_NO_MEMORY;
            } else {
                *buffer = http2Malloc(sizeof(Http2BufferT));

                if (*buffer == NULL) {
                    http2Free(current_buffer);
                    return HTTP2_NO_MEMORY;
                }

                (*buffer)->buffer = current_buffer;
                (*buffer)->length = (size_t) return_code;

                return HTTP2_SUCCESS;
            }
    }
}

int http2HeaderFieldToNv(Http2HeaderFieldT *HTTP2_CONST_RESTRICT_SPECIFIER http2HeaderField,
                         nghttp2_nv *HTTP2_CONST_RESTRICT_SPECIFIER nv) {
    bool isSecureField = false;

    nv->valuelen = http2HeaderField->valueLength;
    nv->namelen = http2HeaderField->nameLength;
    nv->name = http2Malloc(http2HeaderField->nameLength);

    if (nv->name == NULL) {
        return NGHTTP2_ERR_NOMEM;
    }

    switch (http2HeaderField->nameLength) {
        case 6u:
            isSecureField = memcmp(http2HeaderField->name, "cookie", 6u) == 0;
            break;
        case 13u:
            isSecureField = memcmp(http2HeaderField->name, "authorization", 13u) == 0;
            break;
        case 19u:
            isSecureField = memcmp(http2HeaderField->name, "proxy-authorization", 19u) == 0;
            break;
        case 14u:
            isSecureField = memcmp(http2HeaderField->name, "authentication", 14u) == 0;
            break;
        case 12u:
            isSecureField = memcmp(http2HeaderField->name, "authenticate", 12u) == 0;
            break;
        case 9u:
            isSecureField = memcmp(http2HeaderField->name, "authorize", 9u) == 0;
            break;
        case 16u:
            isSecureField = memcmp(http2HeaderField->name, "www-authenticate", 16u) == 0;
            break;
        case 18u:
            isSecureField = memcmp(http2HeaderField->name, "proxy-authenticate", 18u) == 0;
            break;
        default:
            break;
    }

    if (isSecureField) {
        nv->flags = NGHTTP2_NV_FLAG_NO_INDEX;
    } else {
        nv->flags = NGHTTP2_NV_FLAG_NONE;
    }

    nv->value = http2Malloc(http2HeaderField->valueLength);

    if (nv->value == NULL) {
        http2Free(nv->name);
        return NGHTTP2_ERR_NOMEM;
    }

    memcpy(nv->name, http2HeaderField->name, http2HeaderField->nameLength);
    memcpy(nv->value, http2HeaderField->value, http2HeaderField->valueLength);

    return EXIT_SUCCESS;
}

void http2NvFieldsDeallocate(nghttp2_nv *nv) {
    http2Free(nv->name);
    http2Free(nv->value);
}

void http2NvArrayDeallocate(nghttp2_nv *HTTP2_CONST_RESTRICT_SPECIFIER nv,
                            const size_t length) {
    size_t i;

    for (i = 0; i < length; ++i) {
        http2NvFieldsDeallocate(nv + i);
    }

    http2Free(nv);
}

int http2HeadersToNvArray(Http2HeaderListT *HTTP2_CONST_RESTRICT_SPECIFIER http2HeaderList,
                          nghttp2_nv **nv) {
    size_t i;
    size_t j;

    Http2HeaderListElementT *current_header = http2HeaderList->head;
    *nv = http2Malloc(sizeof(nghttp2_nv) * http2HeaderList->length);

    if ((*nv) == NULL) {
        return NGHTTP2_ERR_NOMEM;
    }

    for (i = 0; i < http2HeaderList->length; ++i) {
        const int return_code = http2HeaderFieldToNv(current_header->http2HeaderField, ((*nv) + i));

        if (return_code == NGHTTP2_ERR_NOMEM) {
            for (j = i; j > 0; j--) {
                http2NvFieldsDeallocate(((*nv) + j));
            }

            return NGHTTP2_ERR_NOMEM;
        }

        current_header = current_header->next;
    }

    return EXIT_SUCCESS;
}

void http2HeaderListJoin(Http2HeaderListT *HTTP2_CONST_RESTRICT_SPECIFIER target,
                         Http2HeaderListT *HTTP2_CONST_RESTRICT_SPECIFIER source) {
    target->tail->next = source->head;
    target->length += source->length;
    target->tail = source->tail;
    http2Free(source);
}

int http2HeaderListAppend(Http2HeaderListT *HTTP2_CONST_RESTRICT_SPECIFIER headerList,
                          const char *HTTP2_CONST_RESTRICT_SPECIFIER name,
                          const size_t nameLength,
                          const char *HTTP2_CONST_RESTRICT_SPECIFIER value,
                          const size_t valueLength) {
    Http2HeaderFieldT *field;
    Http2HeaderListElementT *element = http2Calloc(sizeof(Http2HeaderListElementT));
    element->http2HeaderField = http2Malloc(sizeof(Http2HeaderFieldT));

    if (element->http2HeaderField == NULL) {
        http2Free(element);
        return NGHTTP2_ERR_NOMEM;
    }

    field = element->http2HeaderField;

    field->name = http2Malloc(nameLength);

    if (field->name == NULL) {
        http2Free(element);
        http2Free(element->http2HeaderField);
        return NGHTTP2_ERR_NOMEM;
    }

    memcpy(field->name, name, nameLength);
    field->nameLength = nameLength;

    field->value = http2Malloc(valueLength);

    if (field->value == NULL) {
        http2Free(element);
        http2Free(element->http2HeaderField);
        http2Free(field->name);
        return NGHTTP2_ERR_NOMEM;
    }

    memcpy(field->value, value, valueLength);
    field->valueLength = valueLength;

    switch (headerList->length) {
        case 0u:
            headerList->head = element;
            headerList->tail = element;
            headerList->length = 1u;
            return EXIT_SUCCESS;
        default:
            headerList->tail->next = element;
            headerList->tail = element;
            ++headerList->length;
            return EXIT_SUCCESS;
    }
}

Http2ReturnCodeT http2HeaderFramesFromList(Http2FrameListT **frameList,
                                           Http2HeaderListT *headerList,
                                           const size_t maxFrameSize,
                                           const bool isLastFrame,
                                           const uint32_t streamId,
                                           nghttp2_hd_deflater *nghttp2Deflater) {
    Http2BufferT *headerBuffer = NULL;
    Http2FrameListT *frameListPtr;
    http2HeadersEncode(headerList, &headerBuffer, nghttp2Deflater);

    if (headerBuffer == NULL) {
        return HTTP2_INTERNAL_ERROR;
    }

    frameListPtr = http2Calloc(sizeof(Http2FrameListT));

    if (headerBuffer->length <= maxFrameSize) {
        Http2FrameT *frame;
        Http2HeadersFramePayloadT *headersFramePayload = http2Malloc(sizeof(Http2HeadersFramePayloadT));
        headersFramePayload->headerPayload = headerBuffer->buffer;
        frame = http2Malloc(sizeof(Http2FrameT));
        frame->header.length = (uint32_t) headerBuffer->length;
        frame->header.type = HEADERS_FRAME_TYPE;
        frame->header.flags = isLastFrame ? END_HEADERS_FRAME_FLAG | END_STREAM_FRAME_FLAG : END_HEADERS_FRAME_FLAG;
        frame->header.streamId = streamId;
        frame->payload.http2HeadersFramePayload = headersFramePayload;
        http2FrameListAppend(frameListPtr, frame);
        http2Free(headerBuffer);
    } else {
        Http2ContinuationPayloadT *continuationPayload;
        Http2FrameT *frame;
        size_t remainingBuffer = headerBuffer->length;
        uint8_t *bufPtr = headerBuffer->buffer;
        Http2HeadersFramePayloadT *headersFramePayload = http2Malloc(sizeof(Http2HeadersFramePayloadT));
        uint8_t *headerData = http2Malloc(maxFrameSize);
        memcpy(headerData, headerBuffer->buffer, maxFrameSize);
        headersFramePayload->headerPayload = headerData;
        frame = http2Malloc(sizeof(Http2FrameT));
        frame->header.length = (uint32_t) maxFrameSize;
        frame->header.type = HEADERS_FRAME_TYPE;
        frame->header.flags = 0u;
        frame->header.streamId = streamId;
        frame->payload.http2HeadersFramePayload = headersFramePayload;
        remainingBuffer -= maxFrameSize;
        bufPtr += maxFrameSize;
        http2FrameListAppend(frameListPtr, frame);

        while (remainingBuffer > maxFrameSize) {
            Http2ContinuationPayloadT *continuationPayload = http2Malloc(sizeof(Http2ContinuationPayloadT));
            headerData = http2Malloc(maxFrameSize);
            memcpy(headerData, bufPtr, maxFrameSize);
            continuationPayload->headerBlockFragment = headerData;

            frame = http2Malloc(sizeof(Http2FrameT));
            frame->header.length = (uint32_t) maxFrameSize;
            frame->header.type = CONTINUATION_FRAME_TYPE;
            frame->header.flags = 0u;
            frame->header.streamId = streamId;
            frame->payload.http2ContinuationPayload = continuationPayload;
            http2FrameListAppend(frameListPtr, frame);

            remainingBuffer -= maxFrameSize;
            bufPtr += maxFrameSize;
        }

        continuationPayload = http2Malloc(sizeof(Http2ContinuationPayloadT));
        headerData = http2Malloc(remainingBuffer);
        memcpy(headerData, bufPtr, remainingBuffer);
        continuationPayload->headerBlockFragment = headerData;

        frame = http2Malloc(sizeof(Http2FrameT));
        frame->header.length = (uint32_t) maxFrameSize;
        frame->header.type = CONTINUATION_FRAME_TYPE;
        frame->header.flags = isLastFrame ? END_HEADERS_FRAME_FLAG | END_STREAM_FRAME_FLAG : END_HEADERS_FRAME_FLAG;
        frame->header.streamId = streamId;
        frame->payload.http2ContinuationPayload = continuationPayload;
        http2FrameListAppend(frameListPtr, frame);

        http2BufferDeallocate(headerBuffer);
    }

    *frameList = frameListPtr;

    return HTTP2_SUCCESS;
}

int http2HeaderListPrepend(Http2HeaderListT *HTTP2_CONST_RESTRICT_SPECIFIER headerList,
                           const char *HTTP2_CONST_RESTRICT_SPECIFIER name,
                           const size_t nameLength,
                           const char *HTTP2_CONST_RESTRICT_SPECIFIER value,
                           const size_t valueLength) {
    Http2HeaderFieldT *field;

    Http2HeaderListElementT *element = http2Calloc(sizeof(Http2HeaderListElementT));
    element->http2HeaderField = http2Malloc(sizeof(Http2HeaderFieldT));

    if (element->http2HeaderField == NULL) {
        http2Free(element);
        return NGHTTP2_ERR_NOMEM;
    }

    field = element->http2HeaderField;

    field->name = http2Malloc(nameLength);

    if (field->name == NULL) {
        http2Free(element);
        http2Free(element->http2HeaderField);
        return NGHTTP2_ERR_NOMEM;
    }

    memcpy(field->name, name, nameLength);
    field->nameLength = nameLength;

    field->value = http2Malloc(valueLength);

    if (field->value == NULL) {
        http2Free(element);
        http2Free(element->http2HeaderField);
        http2Free(field->name);
        return NGHTTP2_ERR_NOMEM;
    }

    memcpy(field->value, value, valueLength);
    field->valueLength = valueLength;

    switch (headerList->length) {
        case 0u:
            headerList->head = element;
            headerList->tail = element;
            headerList->length = 1u;
            return EXIT_SUCCESS;
        default:
            element->next = headerList->head;
            headerList->head = element;
            ++headerList->length;
            return EXIT_SUCCESS;
    }
}
