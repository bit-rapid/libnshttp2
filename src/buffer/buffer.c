/*
MIT License
-----------

Copyright (c) 2020 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <nshttp2/buffer/buffer.h>
#include <string.h>

Http2ReturnCodeT http2BufferAllocate(Http2BufferT **buffer,
                                     size_t initialLength) {
    /* First we allocate the buffer container */
    *buffer = http2Malloc(sizeof(Http2BufferT));

    /**
     * If the buffer container has not been allocated, we return that there was
     * not enough memory on the system.
     */
    if (*buffer == NULL) {
        return HTTP2_NO_MEMORY;
    }

    /* If the length is 0, this actually means that we do not need a buffer yet. */
    if (initialLength == 0u) {
        (*buffer)->buffer = NULL;
    } else {
        (*buffer)->buffer = http2Malloc(initialLength);

        /**
         * If the actual buffer has not been successfully allocated,
         * we free the container and return that there was not enough memory.
         */
        if ((*buffer)->buffer == NULL) {
            http2Free(*buffer);
            return HTTP2_NO_MEMORY;
        }
    }

    (*buffer)->length = initialLength;

    return HTTP2_SUCCESS;
}

void http2BufferDeallocate(Http2BufferT *HTTP2_CONST_RESTRICT_SPECIFIER buffer) {
    if (buffer->buffer != NULL) {
        http2Free(buffer->buffer);
    }

    http2Free(buffer);
}

Http2ReturnCodeT http2BufferExtend(Http2BufferT *HTTP2_CONST_RESTRICT_SPECIFIER http2Buffer,
                                   size_t length) {
    if (length == 0u) {
        return HTTP2_SUCCESS;
    }

    if (http2Buffer->buffer == NULL) {
        http2Buffer->buffer = http2Malloc(length);

        if (http2Buffer->buffer == NULL) {
            return HTTP2_NO_MEMORY;
        }

        http2Buffer->length = length;
    } else {
        /* First we precalculate/store constant values that we need */
        uint8_t *const originalBuffer = http2Buffer->buffer;

        const size_t newLength = http2Buffer->length + length;

        http2Buffer->buffer = http2Realloc(http2Buffer->buffer, newLength);

        /** 
         * If the realloc was not successful,
         * we return that there was not enough memory
         * and keep the original buffer.
         */
        if (http2Buffer->buffer == NULL) {
            http2Buffer->buffer = originalBuffer;
            return HTTP2_NO_MEMORY;
        }

        http2Buffer->length = newLength;
    }

    return HTTP2_SUCCESS;
}

Http2ReturnCodeT http2BufferAppend(Http2BufferT *HTTP2_CONST_RESTRICT_SPECIFIER http2Buffer,
                                   uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer,
                                   const size_t length) {
    const size_t originalLength = http2Buffer->length;

    switch (http2BufferExtend(http2Buffer, length)) {
        case HTTP2_SUCCESS:
            memcpy(http2Buffer->buffer + originalLength, buffer, length);
            return HTTP2_SUCCESS;
        case HTTP2_NO_MEMORY:
            return HTTP2_NO_MEMORY;
        default:
            return HTTP2_INTERNAL_ERROR;
    }
}

void http2BufferMoveByOffset(Http2BufferT *HTTP2_CONST_RESTRICT_SPECIFIER http2Buffer,
                             const size_t offset) {
    if (offset >= http2Buffer->length) {
        http2Free(http2Buffer->buffer);
        http2Buffer->length = 0u;
    } else {
        const size_t newLength = http2Buffer->length - offset;
        memmove(http2Buffer->buffer, http2Buffer->buffer + offset, newLength);
        http2Buffer->buffer = http2Realloc(http2Buffer->buffer, newLength);
        http2Buffer->length = newLength;
    }
}

void http2BufferReset(Http2BufferT *HTTP2_CONST_RESTRICT_SPECIFIER http2Buffer) {
    uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer = http2Buffer->buffer;

    if (buffer != NULL) {
        http2Free(buffer);
    }

    memset(http2Buffer, 0, sizeof(Http2BufferT));
}

Http2ReturnCodeT http2BufferPrepend(Http2BufferT *http2Buffer, uint8_t *buffer, const size_t length) {
    const size_t newLength = http2Buffer->length + length;
    uint8_t *newBuffer = http2Malloc(newLength);

    if (newBuffer == NULL) {
        return HTTP2_NO_MEMORY;
    }

    memcpy(newBuffer + length, http2Buffer->buffer, http2Buffer->length);

    http2Free(http2Buffer->buffer);

    memcpy(newBuffer, buffer, length);

    http2Buffer->buffer = newBuffer;

    http2Buffer->length = newLength;

    return HTTP2_SUCCESS;
}
