/*
MIT License
-----------

Copyright (c) 2020 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef NSHTTP2_FRAME_PAYLOAD_H
#define NSHTTP2_FRAME_PAYLOAD_H

void test_http2_priority_frame_payload_to_bytes(void);

void test_http2_priority_frame_payload_from_bytes(void);

void test_http2_data_frame_payload_to_bytes(void);

void test_http2_data_frame_payload_from_bytes(void);

void test_http2_headers_frame_payload_to_bytes(void);

void test_http2_headers_frame_payload_from_bytes(void);

void test_http2_rst_stream_frame_payload_to_bytes(void);

void test_http2_rst_stream_frame_payload_from_bytes(void);

void test_http2_settings_frame_payload_to_bytes(void);

void test_http2_settings_frame_payload_from_bytes(void);

void test_http2_push_promise_frame_payload_to_bytes(void);

void test_http2_push_promise_frame_payload_from_bytes(void);

void test_http2_ping_frame_payload_to_bytes(void);

void test_http2_ping_frame_payload_from_bytes(void);

void test_http2_goaway_frame_payload_to_bytes(void);

void test_http2_goaway_frame_payload_from_bytes(void);

void test_http2_goaway_frame_payload_from_bytes_with_debug_data(void);

void test_http2_window_update_payload_to_bytes(void);

void test_http2_window_update_payload_from_bytes(void);

void test_http2_continuation_payload_to_bytes(void);

void test_http2_continuation_payload_from_bytes(void);

void test_http2_priority_frame_payload_allocate(void);

void test_http2_priority_frame_payload_deallocate(void);

void test_http2_headers_frame_payload_allocate(void);

void test_http2_headers_frame_payload_deallocate(void);

void test_http2_rst_stream_frame_payload_allocate(void);

void test_http2_rst_stream_frame_payload_deallocate(void);

void test_http2_settings_frame_payload_allocate(void);

void test_http2_settings_frame_payload_deallocate(void);

void test_http2_push_promise_frame_payload_allocate(void);

void test_http2_push_promise_frame_payload_deallocate(void);

void test_http2_ping_frame_payload_allocate(void);

void test_http2_ping_frame_payload_deallocate(void);

void test_http2_goaway_frame_payload_allocate(void);

void test_http2_goaway_frame_payload_deallocate(void);

void test_http2_window_update_payload_allocate(void);

void test_http2_window_update_payload_deallocate(void);

void test_http2_continuation_payload_allocate(void);

void test_http2_continuation_payload_deallocate(void);

void test_http2_buffer_remove_padding(void);

#endif /* NSHTTP2_FRAME_PAYLOAD_H */
