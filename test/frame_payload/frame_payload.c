/*
MIT License
-----------

Copyright (c) 2020 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include "frame_payload.h"
#include <nshttp2/frame_payload.h>
#include <nshttp2/headers.h>
#include <string.h>
#include <nshttp2/unity/unity.h>
#include <nshttp2/utility/nghttp2.h>

void test_http2_priority_frame_payload_from_bytes(void) {
    Http2PriorityFramePayloadT payload;
    uint8_t buffer[HTTP2_PRIORITY_FRAME_PAYLOAD_SIZE];
    Http2PriorityFramePayloadT new_payload;
    Http2PriorityFramePayloadT non_exclusive_payload;

    memset(&payload, 0, sizeof(Http2PriorityFramePayloadT));
    memset(&new_payload, 0, sizeof(Http2PriorityFramePayloadT));
    memset(&non_exclusive_payload, 0, sizeof(Http2PriorityFramePayloadT));

    payload.dependency = 8u;
    payload.weight = 8u;
    payload.exclusive = true;

    http2PriorityFramePayloadToBytes(&payload, buffer);

    http2PriorityFramePayloadFromBytes(&new_payload, buffer);

    TEST_ASSERT_EQUAL(0, memcmp(&payload, &new_payload, sizeof(Http2PriorityFramePayloadT)));

    non_exclusive_payload.dependency = 8u;
    non_exclusive_payload.exclusive = true;
    non_exclusive_payload.weight = 8u;

    http2PriorityFramePayloadToBytes(&non_exclusive_payload, buffer);

    http2PriorityFramePayloadFromBytes(&new_payload, buffer);

    TEST_ASSERT_EQUAL(0, memcmp(&non_exclusive_payload, &new_payload, sizeof(Http2PriorityFramePayloadT)));
}

void test_http2_priority_frame_payload_to_bytes(void) {
    Http2PriorityFramePayloadT new_payload;
    Http2PriorityFramePayloadT payload;
    uint8_t buffer[HTTP2_PRIORITY_FRAME_PAYLOAD_SIZE];
    Http2PriorityFramePayloadT non_exclusive_payload;

    memset(&new_payload, 0, sizeof(Http2PriorityFramePayloadT));
    memset(&payload, 0, sizeof(Http2PriorityFramePayloadT));
    memset(&non_exclusive_payload, 0, sizeof(Http2PriorityFramePayloadT));

    payload.dependency = 8u;
    payload.exclusive = true;
    payload.weight = 8u;

    http2PriorityFramePayloadToBytes(&payload, buffer);

    http2PriorityFramePayloadFromBytes(&new_payload, buffer);

    TEST_ASSERT_EQUAL(0, memcmp(&payload, &new_payload, sizeof(Http2PriorityFramePayloadT)));

    non_exclusive_payload.dependency = 8u;
    non_exclusive_payload.exclusive = true;
    non_exclusive_payload.weight = 8u;

    http2PriorityFramePayloadToBytes(&non_exclusive_payload, buffer);

    http2PriorityFramePayloadFromBytes(&new_payload, buffer);

    TEST_ASSERT_EQUAL(0, memcmp(&non_exclusive_payload, &new_payload, sizeof(Http2PriorityFramePayloadT)));
}

void test_http2_data_frame_payload_to_bytes(void) {
    uint8_t data_frame_payload[5] = {0u, 2u, 4u, 6u, 8u};
    Http2FramePaddingT frame_padding;
    uint8_t buffer[14];
    uint8_t new_frame_payload[5];
    uint32_t frame_without_padding;

    uint8_t padding[8] = {
            1u,
            2u,
            3u,
            4u,
            5u,
            6u,
            7u,
            8u
    };

    frame_padding.padding = padding;
    frame_padding.length = 8u;

    http2DataFramePayloadToBytes(data_frame_payload,
                                 buffer,
                                 14u,
                                 &frame_padding);


    http2DataFramePayloadFromBytes(new_frame_payload,
                                   &frame_without_padding,
                                   buffer,
                                   14u,
                                   true);

    TEST_ASSERT_EQUAL(0, memcmp(data_frame_payload, new_frame_payload, 5));
}

void test_http2_data_frame_payload_from_bytes(void) {
    uint8_t data_frame_payload[5] = {0u, 2u, 4u, 6u, 8u};
    uint8_t new_frame_payload[5];
    uint8_t buffer[5];
    uint32_t frame_without_padding;

    http2DataFramePayloadToBytes(data_frame_payload,
                                 buffer,
                                 5u,
                                 NULL);

    http2DataFramePayloadFromBytes(new_frame_payload,
                                   &frame_without_padding,
                                   buffer,
                                   5u,
                                   false);

    TEST_ASSERT_EQUAL(0, memcmp(data_frame_payload, new_frame_payload, 5));
}

void test_http2_headers_frame_payload_to_bytes(void) {
    Http2HeaderFieldT host;
    Http2HeaderFieldT user_agent;
    Http2HeaderListElementT user_agent_element;
    Http2HeaderListElementT host_element;
    Http2HeaderListT header_list;
    Http2BufferT *buffer;
    nghttp2_hd_deflater *nghttp2HdDeflater = NULL;
    Http2HeadersFramePayloadT payload;
    uint8_t padding[8] = {
            1u,
            2u,
            3u,
            4u,
            5u,
            6u,
            7u,
            8u
    };
    Http2FramePaddingT frame_padding;
    uint32_t frame_length;
    uint8_t *frame_payload_buffer;
    Http2HeadersFramePayloadT new_payload;
    uint32_t frame_length_without_padding;

    host.name = (uint8_t *) "host";
    host.nameLength = strlen("host");
    host.value = (uint8_t *) ("www.wu.ac.at");
    host.valueLength = strlen("www.wu.ac.at");

    user_agent.name = (uint8_t *) "user-agent";
    user_agent.nameLength = strlen("user-agent");
    user_agent.value = (uint8_t *) "curl/7.68.0";
    user_agent.valueLength = strlen("curl/7.68.0");

    user_agent_element.next = NULL;
    user_agent_element.http2HeaderField = &user_agent;

    host_element.next = &user_agent_element;
    host_element.http2HeaderField = &host;

    header_list.length = 2u;
    header_list.head = &host_element;
    header_list.tail = &user_agent_element;

    ngHttp2GetDeflater(&nghttp2HdDeflater, 4096u);

    http2HeadersEncode(&header_list,
                       &buffer,
                       nghttp2HdDeflater);

    payload.headerPayload = buffer->buffer;
    payload.priority.dependency = 20u;
    payload.priority.weight = 8u;
    payload.priority.exclusive = true;

    frame_padding.padding = padding;
    frame_padding.length = 8u;

    frame_length = (uint32_t) (sizeof(uint8_t) + buffer->length + 8u + HTTP2_PRIORITY_FRAME_PAYLOAD_SIZE);
    frame_payload_buffer = http2Malloc(frame_length);

    http2HeadersFramePayloadToBytes(&payload,
                                    frame_payload_buffer,
                                    frame_length,
                                    &frame_padding,
                                    true);

    new_payload.headerPayload = http2Malloc(buffer->length);

    http2HeadersFramePayloadFromBytes(&new_payload,
                                      &frame_length_without_padding,
                                      frame_payload_buffer,
                                      frame_length,
                                      true,
                                      true);

    TEST_ASSERT_EQUAL(0u, memcmp(&new_payload.priority, &payload.priority, HTTP2_PRIORITY_FRAME_PAYLOAD_SIZE));
    TEST_ASSERT_EQUAL(0u, memcmp(new_payload.headerPayload, payload.headerPayload, buffer->length));

    http2Free(new_payload.headerPayload);
    http2BufferDeallocate(buffer);
    nghttp2_hd_deflate_del(nghttp2HdDeflater);
    http2Free(frame_payload_buffer);
}

void test_http2_headers_frame_payload_from_bytes(void) {
    Http2HeaderFieldT host;
    Http2HeaderFieldT user_agent;
    Http2HeaderListElementT user_agent_element;
    Http2HeaderListElementT host_element;
    Http2HeaderListT header_list;
    Http2BufferT *buffer;
    nghttp2_hd_deflater *nghttp2HdDeflater = NULL;
    Http2HeadersFramePayloadT payload;
    uint32_t frame_length;
    uint8_t *frame_payload_buffer;
    Http2HeadersFramePayloadT new_payload;
    uint32_t frame_length_without_padding;

    host.name = (uint8_t *) "host";
    host.nameLength = strlen("host");
    host.value = (uint8_t *) ("www.wu.ac.at");
    host.valueLength = strlen("www.wu.ac.at");

    user_agent.name = (uint8_t *) "user-agent";
    user_agent.nameLength = strlen("user-agent");
    user_agent.value = (uint8_t *) "curl/7.68.0";
    user_agent.valueLength = strlen("curl/7.68.0");

    user_agent_element.next = NULL;
    user_agent_element.http2HeaderField = &user_agent;

    host_element.next = &user_agent_element;
    host_element.http2HeaderField = &host;

    header_list.length = 2u;
    header_list.head = &host_element;
    header_list.tail = &user_agent_element;

    ngHttp2GetDeflater(&nghttp2HdDeflater, 4096u);

    http2HeadersEncode(&header_list,
                       &buffer,
                       nghttp2HdDeflater);

    payload.headerPayload = buffer->buffer;
    payload.priority.dependency = 20u;
    payload.priority.weight = 8u;
    payload.priority.exclusive = true;

    frame_length = buffer->length + HTTP2_PRIORITY_FRAME_PAYLOAD_SIZE;
    frame_payload_buffer = http2Malloc(frame_length);

    http2HeadersFramePayloadToBytes(&payload,
                                    frame_payload_buffer,
                                    frame_length,
                                    NULL,
                                    true);

    new_payload.headerPayload = http2Malloc(buffer->length);

    http2HeadersFramePayloadFromBytes(&new_payload,
                                      &frame_length_without_padding,
                                      frame_payload_buffer,
                                      frame_length,
                                      false,
                                      true);

    TEST_ASSERT_EQUAL(payload.priority.exclusive,
                      new_payload.priority.exclusive);

    TEST_ASSERT_EQUAL(payload.priority.dependency,
                      new_payload.priority.dependency);

    TEST_ASSERT_EQUAL(payload.priority.weight,
                      new_payload.priority.weight);

    TEST_ASSERT_EQUAL(0, memcmp(new_payload.headerPayload, payload.headerPayload, buffer->length));

    http2BufferDeallocate(buffer);
    http2Free(new_payload.headerPayload);
    nghttp2_hd_deflate_del(nghttp2HdDeflater);
    http2Free(frame_payload_buffer);
}

void test_http2_rst_stream_frame_payload_to_bytes(void) {
    Http2RstStreamFramePayloadT payload;
    uint8_t buffer[HTTP2_RST_STREAM_FRAME_PAYLOAD_SIZE];
    Http2RstStreamFramePayloadT new_payload;

    payload.errorCode = PROTOCOL_ERROR_ERROR_CODE;

    http2RstStreamFramePayloadToBytes(&payload, buffer);

    http2RstStreamFramePayloadFromBytes(&new_payload, buffer);

    TEST_ASSERT_EQUAL(payload.errorCode, new_payload.errorCode);
}

void test_http2_rst_stream_frame_payload_from_bytes(void) {
    test_http2_rst_stream_frame_payload_to_bytes();
}

void test_http2_settings_frame_payload_to_bytes(void) {
    Http2SettingsFramePayloadT payload;
    uint8_t buffer[HTTP2_SETTINGS_FORMAT_SIZE * 5];
    Http2SettingsFramePayloadT new_payload;

    payload.initialWindowSize = 80000u;
    payload.maxHeaderListSize = 90000u;
    payload.maxFrameSize = 32000u;
    payload.maxConcurrentStreams = 100u;
    payload.enablePush = true;
    payload.headerTableSize = 8192u;

    http2SettingsFramePayloadToBytes(&payload, buffer);

    http2SettingsFramePayloadFromBytes(&new_payload,
                                       buffer,
                                       HTTP2_SETTINGS_FORMAT_SIZE * 5,
                                       false);

    TEST_ASSERT_EQUAL(payload.initialWindowSize, new_payload.initialWindowSize);
    TEST_ASSERT_EQUAL(payload.maxHeaderListSize, new_payload.maxHeaderListSize);
    TEST_ASSERT_EQUAL(payload.maxFrameSize, new_payload.maxFrameSize);
    TEST_ASSERT_EQUAL(payload.maxConcurrentStreams, new_payload.maxConcurrentStreams);
    TEST_ASSERT_EQUAL(payload.enablePush, new_payload.enablePush);
    TEST_ASSERT_EQUAL(payload.headerTableSize, new_payload.headerTableSize);
}

void test_http2_settings_frame_payload_from_bytes(void) {
    Http2SettingsFramePayloadT payload;
    uint8_t buffer[HTTP2_SETTINGS_FORMAT_SIZE * 5];
    Http2SettingsFramePayloadT new_payload;

    payload.initialWindowSize = 80000u;
    payload.maxHeaderListSize = 90000u;
    payload.maxFrameSize = 32000u;
    payload.maxConcurrentStreams = 100u;
    payload.enablePush = true;
    payload.headerTableSize = 8192u;

    http2SettingsFramePayloadToBytes(&payload, buffer);

    http2SettingsFramePayloadFromBytes(&new_payload,
                                       buffer,
                                       HTTP2_SETTINGS_FORMAT_SIZE * 5,
                                       false);

    TEST_ASSERT_EQUAL(payload.initialWindowSize, new_payload.initialWindowSize);
    TEST_ASSERT_EQUAL(payload.maxHeaderListSize, new_payload.maxHeaderListSize);
    TEST_ASSERT_EQUAL(payload.maxFrameSize, new_payload.maxFrameSize);
    TEST_ASSERT_EQUAL(payload.maxConcurrentStreams, new_payload.maxConcurrentStreams);
    TEST_ASSERT_EQUAL(payload.enablePush, new_payload.enablePush);
    TEST_ASSERT_EQUAL(payload.headerTableSize, new_payload.headerTableSize);
}

void test_http2_push_promise_frame_payload_to_bytes(void) {
    Http2HeaderFieldT host;
    Http2HeaderFieldT user_agent;
    Http2HeaderListElementT user_agent_element;
    Http2HeaderListElementT host_element;
    Http2HeaderListT header_list;
    Http2BufferT *buffer;
    nghttp2_hd_deflater *nghttp2HdDeflater = NULL;
    Http2PushPromiseFramePayloadT payload;
    uint32_t frame_length;
    uint8_t *frame_payload_buffer;
    uint8_t padding[8] = {
            1u,
            2u,
            3u,
            4u,
            5u,
            6u,
            7u,
            8u
    };
    Http2FramePaddingT frame_padding;
    Http2PushPromiseFramePayloadT *new_payload;

    host.name = (uint8_t *) "host";
    host.nameLength = strlen("host");
    host.value = (uint8_t *) "www.wu.ac.at";
    host.valueLength = strlen("www.wu.ac.at");

    user_agent.name = (uint8_t *) "user-agent";
    user_agent.nameLength = strlen("user-agent");
    user_agent.value = (uint8_t *) "curl/7.68.0";
    user_agent.valueLength = strlen("curl/7.68.0");

    user_agent_element.next = NULL;
    user_agent_element.http2HeaderField = &user_agent;

    host_element.next = &user_agent_element;
    host_element.http2HeaderField = &host;

    header_list.length = 2u;
    header_list.head = &host_element;
    header_list.tail = &user_agent_element;

    ngHttp2GetDeflater(&nghttp2HdDeflater, 4096u);

    http2HeadersEncode(&header_list,
                       &buffer,
                       nghttp2HdDeflater);

    payload.headerBlockFragment = buffer->buffer;
    payload.promisedStreamId = 23u;

    frame_length = buffer->length + sizeof(uint32_t);

    frame_payload_buffer = http2Malloc(frame_length + 8u + sizeof(uint8_t));

    frame_padding.padding = padding;
    frame_padding.length = 8u;

    http2PushPromiseFramePayloadToBytes(&payload,
                                        frame_payload_buffer,
                                        frame_length + 8u + sizeof(uint8_t),
                                        &frame_padding);

    http2PushPromiseFramePayloadAllocate(&new_payload, frame_length);

    http2PushPromiseFramePayloadFromBytes(new_payload,
                                          frame_payload_buffer,
                                          frame_length + 8u + sizeof(uint8_t),
                                          true);

    TEST_ASSERT_EQUAL(payload.promisedStreamId,
                      new_payload->promisedStreamId);

    TEST_ASSERT_EQUAL(0,
                      memcmp(payload.headerBlockFragment,
                             new_payload->headerBlockFragment,
                             buffer->length));

    http2PushPromiseFramePayloadDeallocate(new_payload);
    nghttp2_hd_deflate_del(nghttp2HdDeflater);
    http2BufferDeallocate(buffer);
    http2Free(frame_payload_buffer);
}

void test_http2_push_promise_frame_payload_from_bytes(void) {
    Http2HeaderFieldT host;
    Http2HeaderFieldT user_agent;
    Http2HeaderListElementT user_agent_element;
    Http2HeaderListElementT host_element;
    Http2HeaderListT header_list;
    Http2BufferT *buffer;
    nghttp2_hd_deflater *nghttp2HdDeflater = NULL;
    Http2PushPromiseFramePayloadT payload;
    uint32_t frame_length;
    uint8_t *frame_payload_buffer;
    Http2PushPromiseFramePayloadT *new_payload;

    host.name = (uint8_t *) "host";
    host.nameLength = strlen("host");
    host.value = (uint8_t *) "www.wu.ac.at";
    host.valueLength = strlen("www.wu.ac.at");

    user_agent.name = (uint8_t *) "user-agent";
    user_agent.nameLength = strlen("user-agent");
    user_agent.value = (uint8_t *) "curl/7.68.0";
    user_agent.valueLength = strlen("curl/7.68.0");

    user_agent_element.next = NULL;
    user_agent_element.http2HeaderField = &user_agent;

    host_element.next = &user_agent_element;
    host_element.http2HeaderField = &host;

    header_list.length = 2u;
    header_list.head = &host_element;
    header_list.tail = &user_agent_element;

    ngHttp2GetDeflater(&nghttp2HdDeflater, 4096u);

    http2HeadersEncode(&header_list,
                       &buffer,
                       nghttp2HdDeflater);

    payload.headerBlockFragment = buffer->buffer;
    payload.promisedStreamId = 23u;

    frame_length = buffer->length + sizeof(uint32_t);
    frame_payload_buffer = http2Malloc(frame_length);

    http2PushPromiseFramePayloadToBytes(&payload,
                                        frame_payload_buffer,
                                        frame_length,
                                        NULL);

    http2PushPromiseFramePayloadAllocate(&new_payload, frame_length);

    http2PushPromiseFramePayloadFromBytes(new_payload,
                                          frame_payload_buffer,
                                          frame_length,
                                          false);

    TEST_ASSERT_EQUAL(payload.promisedStreamId, new_payload->promisedStreamId);
    TEST_ASSERT_EQUAL(0,
                      memcmp(payload.headerBlockFragment,
                             new_payload->headerBlockFragment,
                             buffer->length));

    http2PushPromiseFramePayloadDeallocate(new_payload);
    http2BufferDeallocate(buffer);
    nghttp2_hd_deflate_del(nghttp2HdDeflater);
    http2Free(frame_payload_buffer);
}

void test_http2_ping_frame_payload_to_bytes(void) {
    Http2PingFramePayloadT payload;
    uint8_t i;
    uint8_t buffer[HTTP2_PING_FRAME_PAYLOAD_SIZE];
    Http2PingFramePayloadT new_payload;

    for (i = 0u; i < 8u; ++i) {
        payload.opaqueData[i] = i;
    }

    http2PingFramePayloadToBytes(&payload,
                                 buffer);

    http2PingFramePayloadFromBytes(&new_payload,
                                   buffer);

    TEST_ASSERT_EQUAL(0, memcmp(payload.opaqueData, new_payload.opaqueData, HTTP2_PING_FRAME_PAYLOAD_SIZE));

}

void test_http2_ping_frame_payload_from_bytes(void) {
    Http2PingFramePayloadT payload;
    uint8_t i;
    uint8_t buffer[HTTP2_PING_FRAME_PAYLOAD_SIZE];
    Http2PingFramePayloadT new_payload;

    for (i = 0u; i < 8u; ++i) {
        payload.opaqueData[i] = i;
    }

    http2PingFramePayloadToBytes(&payload,
                                 buffer);

    http2PingFramePayloadFromBytes(&new_payload,
                                   buffer);

    TEST_ASSERT_EQUAL(0, memcmp(payload.opaqueData, new_payload.opaqueData, HTTP2_PING_FRAME_PAYLOAD_SIZE));
}

void test_http2_goaway_frame_payload_to_bytes(void) {
    Http2GoawayFramePayloadT payload;
    uint8_t buffer[HTTP2_GOAWAY_MINIMUM_PAYLOAD_LEN];
    Http2GoawayFramePayloadT new_payload;

    payload.errorCode = PROTOCOL_ERROR_ERROR_CODE;
    payload.additionalDebugData = NULL;
    payload.lastStreamId = 45u;

    http2GoawayFramePayloadToBytes(&payload,
                                   buffer,
                                   HTTP2_GOAWAY_MINIMUM_PAYLOAD_LEN);

    http2GoawayFramePayloadFromBytes(&new_payload,
                                     buffer,
                                     HTTP2_GOAWAY_MINIMUM_PAYLOAD_LEN);

    TEST_ASSERT_EQUAL(payload.errorCode, new_payload.errorCode);
    TEST_ASSERT_EQUAL(payload.lastStreamId, new_payload.lastStreamId);
}

void test_http2_goaway_frame_payload_from_bytes(void) {
    Http2GoawayFramePayloadT payload;
    uint8_t buffer[HTTP2_GOAWAY_MINIMUM_PAYLOAD_LEN];
    Http2GoawayFramePayloadT new_payload;

    payload.errorCode = PROTOCOL_ERROR_ERROR_CODE;
    payload.additionalDebugData = NULL;
    payload.lastStreamId = 45u;

    http2GoawayFramePayloadToBytes(&payload,
                                   buffer,
                                   HTTP2_GOAWAY_MINIMUM_PAYLOAD_LEN);

    http2GoawayFramePayloadFromBytes(&new_payload,
                                     buffer,
                                     HTTP2_GOAWAY_MINIMUM_PAYLOAD_LEN);

    TEST_ASSERT_EQUAL(payload.errorCode, new_payload.errorCode);
    TEST_ASSERT_EQUAL(payload.lastStreamId, new_payload.lastStreamId);
}

void test_http2_window_update_payload_to_bytes(void) {
    Http2WindowUpdatePayloadT payload;
    uint8_t buffer[HTTP2_WINDOW_UPDATE_PAYLOAD_LEN];
    Http2WindowUpdatePayloadT new_payload;

    payload.windowSizeIncrement = UINT16_MAX;

    http2WindowUpdatePayloadToBytes(&payload, buffer);

    http2WindowUpdatePayloadFromBytes(&new_payload, buffer);

    TEST_ASSERT_EQUAL(payload.windowSizeIncrement, new_payload.windowSizeIncrement);
}

void test_http2_window_update_payload_from_bytes(void) {
    Http2WindowUpdatePayloadT payload;
    uint8_t buffer[HTTP2_WINDOW_UPDATE_PAYLOAD_LEN];
    Http2WindowUpdatePayloadT new_payload;

    payload.windowSizeIncrement = UINT16_MAX;

    http2WindowUpdatePayloadToBytes(&payload, buffer);

    http2WindowUpdatePayloadFromBytes(&new_payload, buffer);

    TEST_ASSERT_EQUAL(payload.windowSizeIncrement, new_payload.windowSizeIncrement);
}

void test_http2_continuation_payload_to_bytes(void) {
    Http2HeaderFieldT host;
    Http2HeaderFieldT user_agent;
    Http2HeaderListElementT user_agent_element;
    Http2HeaderListElementT host_element;
    Http2HeaderListT header_list;
    Http2BufferT *buffer;
    nghttp2_hd_deflater *nghttp2HdDeflater = NULL;
    Http2ContinuationPayloadT payload;
    uint8_t *frame_payload_buffer;
    Http2ContinuationPayloadT new_payload;

    host.name = (uint8_t *) "host";
    host.nameLength = strlen("host");
    host.value = (uint8_t *) "www.wu.ac.at";
    host.valueLength = strlen("www.wu.ac.at");

    user_agent.name = (uint8_t *) "user-agent";
    user_agent.nameLength = strlen("user-agent");
    user_agent.value = (uint8_t *) "curl/7.68.0";
    user_agent.valueLength = strlen("curl/7.68.0");

    user_agent_element.next = NULL;
    user_agent_element.http2HeaderField = &user_agent;

    host_element.next = &user_agent_element;
    host_element.http2HeaderField = &host;

    header_list.length = 2u;
    header_list.head = &host_element;
    header_list.tail = &user_agent_element;

    ngHttp2GetDeflater(&nghttp2HdDeflater, 4096u);

    http2HeadersEncode(&header_list,
                       &buffer,
                       nghttp2HdDeflater);

    payload.headerBlockFragment = buffer->buffer;
    frame_payload_buffer = http2Malloc(buffer->length);

    http2ContinuationPayloadToBytes(&payload,
                                    frame_payload_buffer,
                                    buffer->length);

    new_payload.headerBlockFragment = http2Malloc(buffer->length);

    http2ContinuationPayloadFromBytes(&new_payload,
                                      frame_payload_buffer,
                                      buffer->length);

    TEST_ASSERT_EQUAL(0, memcmp(payload.headerBlockFragment,
                                new_payload.headerBlockFragment,
                                buffer->length));

    http2Free(new_payload.headerBlockFragment);
    http2BufferDeallocate(buffer);
    nghttp2_hd_deflate_del(nghttp2HdDeflater);
    http2Free(frame_payload_buffer);
}

void test_http2_continuation_payload_from_bytes(void) {
    Http2HeaderFieldT host;
    Http2HeaderFieldT user_agent;
    Http2HeaderListElementT user_agent_element;
    Http2HeaderListElementT host_element;
    Http2HeaderListT header_list;
    Http2BufferT *buffer;
    nghttp2_hd_deflater *nghttp2HdDeflater = NULL;
    Http2ContinuationPayloadT payload;
    uint8_t *frame_payload_buffer;
    Http2ContinuationPayloadT new_payload;

    host.name = (uint8_t *) "host";
    host.nameLength = strlen("host");
    host.value = (uint8_t *) "www.wu.ac.at";
    host.valueLength = strlen("www.wu.ac.at");

    user_agent.name = (uint8_t *) "user-agent";
    user_agent.nameLength = strlen("user-agent");
    user_agent.value = (uint8_t *) "curl/7.68.0";
    user_agent.valueLength = strlen("curl/7.68.0");

    user_agent_element.next = NULL;
    user_agent_element.http2HeaderField = &user_agent;

    host_element.next = &user_agent_element;
    host_element.http2HeaderField = &host;

    header_list.length = 2u;
    header_list.head = &host_element;
    header_list.tail = &user_agent_element;

    ngHttp2GetDeflater(&nghttp2HdDeflater, 4096u);

    http2HeadersEncode(&header_list,
                       &buffer,
                       nghttp2HdDeflater);

    payload.headerBlockFragment = buffer->buffer;

    frame_payload_buffer = http2Malloc(buffer->length);

    http2ContinuationPayloadToBytes(&payload,
                                    frame_payload_buffer,
                                    buffer->length);

    new_payload.headerBlockFragment = http2Malloc(buffer->length);

    http2ContinuationPayloadFromBytes(&new_payload,
                                      frame_payload_buffer,
                                      buffer->length);

    TEST_ASSERT_EQUAL(0, memcmp(payload.headerBlockFragment, new_payload.headerBlockFragment, buffer->length));

    http2Free(new_payload.headerBlockFragment);
    http2BufferDeallocate(buffer);
    nghttp2_hd_deflate_del(nghttp2HdDeflater);
    http2Free(frame_payload_buffer);
}

void test_http2_priority_frame_payload_allocate(void) {
    Http2PriorityFramePayloadT *payload;
    uint8_t buffer[HTTP2_PRIORITY_FRAME_PAYLOAD_SIZE];

    http2PriorityFramePayloadAllocate(&payload);

    memset(payload, 0, sizeof(Http2PriorityFramePayloadT));

    TEST_ASSERT_NOT_NULL(payload);

    http2PriorityFramePayloadToBytes(payload, buffer);

    http2PriorityFramePayloadDeallocate(payload);
}

void test_http2_priority_frame_payload_deallocate(void) {
    Http2PriorityFramePayloadT *payload;
    http2PriorityFramePayloadAllocate(&payload);

    TEST_ASSERT_NOT_NULL(payload);

    http2PriorityFramePayloadDeallocate(payload);
}

void test_http2_headers_frame_payload_allocate(void) {
    Http2HeaderFieldT host;
    Http2HeaderFieldT user_agent;
    Http2HeaderListElementT user_agent_element;
    Http2HeaderListElementT host_element;
    Http2HeaderListT header_list;
    Http2BufferT *buffer;
    nghttp2_hd_deflater *nghttp2HdDeflater = NULL;
    Http2HeadersFramePayloadT *payload;
    uint8_t padding[8] = {
            1u,
            2u,
            3u,
            4u,
            5u,
            6u,
            7u,
            8u
    };
    Http2FramePaddingT frame_padding;
    uint32_t frame_length;
    uint8_t *frame_payload_buffer;

    host.name = (uint8_t *) "host";
    host.nameLength = strlen("host");
    host.value = (uint8_t *) "www.wu.ac.at";
    host.valueLength = strlen("www.wu.ac.at");

    user_agent.name = (uint8_t *) "user-agent";
    user_agent.nameLength = strlen("user-agent");
    user_agent.value = (uint8_t *) "curl/7.68.0";
    user_agent.valueLength = strlen("curl/7.68.0");

    user_agent_element.next = NULL;
    user_agent_element.http2HeaderField = &user_agent;

    host_element.next = &user_agent_element;
    host_element.http2HeaderField = &host;

    header_list.length = 2u;
    header_list.head = &host_element;
    header_list.tail = &user_agent_element;

    ngHttp2GetDeflater(&nghttp2HdDeflater, 4096u);

    http2HeadersEncode(&header_list,
                       &buffer,
                       nghttp2HdDeflater);

    http2HeadersFramePayloadAllocate(&payload,
                                     true,
                                     (uint32_t) buffer->length + HTTP2_PRIORITY_FRAME_PAYLOAD_SIZE);

    memcpy(payload->headerPayload, buffer->buffer, buffer->length);
    payload->priority.dependency = 20u;
    payload->priority.weight = 8u;
    payload->priority.exclusive = true;

    frame_padding.padding = padding;
    frame_padding.length = 8u;

    frame_length = (uint32_t) (sizeof(uint8_t) + buffer->length + 8u + HTTP2_PRIORITY_FRAME_PAYLOAD_SIZE);

    frame_payload_buffer = http2Malloc(frame_length);

    http2HeadersFramePayloadToBytes(payload,
                                    frame_payload_buffer,
                                    frame_length,
                                    &frame_padding,
                                    true);

    http2BufferDeallocate(buffer);
    http2HeadersFramePayloadDeallocate(payload);
    nghttp2_hd_deflate_del(nghttp2HdDeflater);
    http2Free(frame_payload_buffer);
}

void test_http2_headers_frame_payload_deallocate(void) {
    Http2HeadersFramePayloadT *payload;
    http2HeadersFramePayloadAllocate(&payload, false, 2);

    TEST_ASSERT_NOT_NULL(payload);
    http2HeadersFramePayloadDeallocate(payload);
}

void test_http2_rst_stream_frame_payload_allocate(void) {
    Http2RstStreamFramePayloadT *payload;
    uint8_t buffer[HTTP2_RST_STREAM_FRAME_PAYLOAD_SIZE];

    http2RstStreamFramePayloadAllocate(&payload);

    TEST_ASSERT_NOT_NULL(payload);

    payload->errorCode = PROTOCOL_ERROR_ERROR_CODE;

    http2RstStreamFramePayloadToBytes(payload, buffer);

    http2RstStreamFramePayloadDeallocate(payload);
}

void test_http2_rst_stream_frame_payload_deallocate(void) {
    Http2RstStreamFramePayloadT *payload;
    http2RstStreamFramePayloadAllocate(&payload);

    TEST_ASSERT_NOT_NULL(payload);

    http2RstStreamFramePayloadDeallocate(payload);
}

void test_http2_settings_frame_payload_allocate(void) {
    Http2SettingsFramePayloadT *payload;
    uint8_t buffer[HTTP2_SETTINGS_FORMAT_SIZE * 5] = {0};

    http2SettingsFramePayloadAllocate(&payload);

    TEST_ASSERT_NOT_NULL(payload);

    payload->headerTableSize = 8192u;
    payload->initialWindowSize = 80000u;
    payload->maxHeaderListSize = 90000u;
    payload->maxConcurrentStreams = 32000u;
    payload->enablePush = true;
    payload->headerTableSize = 8192u;

    http2SettingsFramePayloadToBytes(payload, buffer);

    http2SettingsFramePayloadDeallocate(payload);
}

void test_http2_settings_frame_payload_deallocate(void) {
    Http2SettingsFramePayloadT *payload;
    http2SettingsFramePayloadAllocate(&payload);

    TEST_ASSERT_NOT_NULL(payload);

    http2SettingsFramePayloadDeallocate(payload);
}

void test_http2_push_promise_frame_payload_allocate(void) {
    Http2HeaderFieldT host;
    Http2HeaderFieldT user_agent;
    Http2HeaderListElementT user_agent_element;
    Http2HeaderListElementT host_element;
    Http2HeaderListT header_list;
    Http2BufferT *buffer;
    nghttp2_hd_deflater *nghttp2HdDeflater = NULL;
    uint32_t frame_length;
    uint8_t *frame_payload_buffer;
    Http2PushPromiseFramePayloadT *payload;

    host.name = (uint8_t *) "host";
    host.nameLength = strlen("host");
    host.value = (uint8_t *) "www.wu.ac.at";
    host.valueLength = strlen("www.wu.ac.at");

    user_agent.name = (uint8_t *) "user-agent";
    user_agent.nameLength = strlen("user-agent");
    user_agent.value = (uint8_t *) "curl/7.68.0";
    user_agent.valueLength = strlen("curl/7.68.0");

    user_agent_element.next = NULL;
    user_agent_element.http2HeaderField = &user_agent;

    host_element.next = &user_agent_element;
    host_element.http2HeaderField = &host;

    header_list.length = 2u;
    header_list.head = &host_element;
    header_list.tail = &user_agent_element;

    ngHttp2GetDeflater(&nghttp2HdDeflater, 4096u);

    http2HeadersEncode(&header_list,
                       &buffer,
                       nghttp2HdDeflater);

    frame_length = (uint32_t) (buffer->length + sizeof(uint32_t));
    frame_payload_buffer = http2Malloc(frame_length);

    http2PushPromiseFramePayloadAllocate(&payload, (uint32_t) (sizeof(uint32_t) + buffer->length));

    TEST_ASSERT_NOT_NULL(payload);

    memcpy(payload->headerBlockFragment, buffer->buffer, buffer->length);
    payload->promisedStreamId = 23u;

    http2PushPromiseFramePayloadToBytes(payload,
                                        frame_payload_buffer,
                                        frame_length,
                                        NULL);

    http2PushPromiseFramePayloadDeallocate(payload);
    http2BufferDeallocate(buffer);
    nghttp2_hd_deflate_del(nghttp2HdDeflater);
    http2Free(frame_payload_buffer);
}

void test_http2_push_promise_frame_payload_deallocate(void) {
    Http2PushPromiseFramePayloadT *payload;
    http2PushPromiseFramePayloadAllocate(&payload, sizeof(uint32_t) + 5);

    TEST_ASSERT_NOT_NULL(payload);

    http2PushPromiseFramePayloadDeallocate(payload);
}

void test_http2_ping_frame_payload_allocate(void) {
    Http2PingFramePayloadT *payload;
    uint8_t opaque_data[8] = {1u, 2u, 3u, 4u, 5u, 6u, 7u, 8u};
    uint8_t buffer[HTTP2_PING_FRAME_PAYLOAD_SIZE];

    http2PingFramePayloadAllocate(&payload);

    TEST_ASSERT_NOT_NULL(payload);

    memcpy(&payload->opaqueData, opaque_data, 8 * sizeof(uint8_t));

    http2PingFramePayloadToBytes(payload,
                                 buffer);

    http2PingFramePayloadDeallocate(payload);
}

void test_http2_ping_frame_payload_deallocate(void) {
    Http2PingFramePayloadT *payload;
    http2PingFramePayloadAllocate(&payload);

    TEST_ASSERT_NOT_NULL(payload);

    http2PingFramePayloadDeallocate(payload);
}

void test_http2_goaway_frame_payload_allocate(void) {
    Http2GoawayFramePayloadT *payload;
    uint8_t buffer[HTTP2_GOAWAY_MINIMUM_PAYLOAD_LEN];

    http2GoawayFramePayloadAllocate(&payload, HTTP2_GOAWAY_MINIMUM_PAYLOAD_LEN);

    TEST_ASSERT_NOT_NULL(payload);

    payload->errorCode = PROTOCOL_ERROR_ERROR_CODE;
    payload->additionalDebugData = 0u;
    payload->lastStreamId = 45u;

    http2GoawayFramePayloadToBytes(payload,
                                   buffer,
                                   HTTP2_GOAWAY_MINIMUM_PAYLOAD_LEN);

    http2GoawayFramePayloadDeallocate(payload);
}

void test_http2_goaway_frame_payload_deallocate(void) {
    Http2GoawayFramePayloadT *payload;
    http2GoawayFramePayloadAllocate(&payload, HTTP2_GOAWAY_MINIMUM_PAYLOAD_LEN);

    TEST_ASSERT_NOT_NULL(payload);

    http2GoawayFramePayloadDeallocate(payload);
}

void test_http2_window_update_payload_allocate(void) {
    Http2WindowUpdatePayloadT *payload;
    uint8_t buffer[HTTP2_WINDOW_UPDATE_PAYLOAD_LEN];

    http2WindowUpdatePayloadAllocate(&payload);

    TEST_ASSERT_NOT_NULL(payload);
    payload->windowSizeIncrement = UINT16_MAX;

    http2WindowUpdatePayloadToBytes(payload, buffer);

    http2WindowUpdatePayloadDeallocate(payload);
}

void test_http2_window_update_payload_deallocate(void) {
    Http2WindowUpdatePayloadT *payload;
    http2WindowUpdatePayloadAllocate(&payload);

    TEST_ASSERT_NOT_NULL(payload);

    http2WindowUpdatePayloadDeallocate(payload);
}

void test_http2_continuation_payload_allocate(void) {
    Http2HeaderFieldT host;
    Http2HeaderFieldT user_agent;
    Http2HeaderListElementT user_agent_element;
    Http2HeaderListElementT host_element;
    Http2HeaderListT header_list;
    Http2BufferT *buffer;
    nghttp2_hd_deflater *nghttp2HdDeflater = NULL;
    Http2ContinuationPayloadT *payload;
    uint8_t *frame_payload_buffer;

    host.name = (uint8_t *) "host";
    host.nameLength = strlen("host");
    host.value = (uint8_t *) "www.wu.ac.at";
    host.valueLength = strlen("www.wu.ac.at");

    user_agent.name = (uint8_t *) "user-agent";
    user_agent.nameLength = strlen("user-agent");
    user_agent.value = (uint8_t *) "curl/7.68.0";
    user_agent.valueLength = strlen("curl/7.68.0");

    user_agent_element.next = NULL;
    user_agent_element.http2HeaderField = &user_agent;

    host_element.next = &user_agent_element;
    host_element.http2HeaderField = &host;

    header_list.length = 2u;
    header_list.head = &host_element;
    header_list.tail = &user_agent_element;

    ngHttp2GetDeflater(&nghttp2HdDeflater, 4096u);

    http2HeadersEncode(&header_list,
                       &buffer,
                       nghttp2HdDeflater);

    http2ContinuationPayloadAllocate(&payload, (uint32_t) buffer->length);

    TEST_ASSERT_NOT_NULL(payload);

    frame_payload_buffer = http2Malloc(buffer->length);

    http2ContinuationPayloadToBytes(payload,
                                    frame_payload_buffer,
                                    (uint32_t) buffer->length);

    http2BufferDeallocate(buffer);
    http2ContinuationPayloadDeallocate(payload);
    nghttp2_hd_deflate_del(nghttp2HdDeflater);
    http2Free(frame_payload_buffer);
}

void test_http2_continuation_payload_deallocate(void) {
    Http2ContinuationPayloadT *payload;
    uint8_t frame_payload_buffer[10];

    http2ContinuationPayloadAllocate(&payload, 10);

    TEST_ASSERT_NOT_NULL(payload);

    http2ContinuationPayloadToBytes(payload,
                                    frame_payload_buffer,
                                    10);

    http2ContinuationPayloadDeallocate(payload);
}

void test_http2_buffer_remove_padding(void) {
    uint8_t data_frame_payload[5] = {0u, 2u, 4u, 6u, 8u};
    uint8_t padding[8] = {
            1u,
            2u,
            3u,
            4u,
            5u,
            6u,
            7u,
            8u
    };
    Http2FramePaddingT frame_padding;
    uint8_t buffer[14];
    Http2BufferWithoutPaddingT buffer_without_padding;

    frame_padding.padding = padding;
    frame_padding.length = 8u;

    http2DataFramePayloadToBytes(data_frame_payload,
                                 buffer,
                                 14u,
                                 &frame_padding);

    http2BufferRemovePadding(buffer,
                             14u,
                             &buffer_without_padding);

    TEST_ASSERT_EQUAL(5, buffer_without_padding.frameLengthWithoutPadding);
    TEST_ASSERT_EQUAL(0, memcmp(buffer_without_padding.buffer, data_frame_payload, 5));
}

void test_http2_goaway_frame_payload_from_bytes_with_debug_data(void) {
    uint8_t *debugData;
    Http2GoawayFramePayloadT payload;
    uint8_t buffer[HTTP2_GOAWAY_MINIMUM_PAYLOAD_LEN];
    Http2GoawayFramePayloadT new_payload;

    debugData = http2Malloc(210);

    payload.errorCode = PROTOCOL_ERROR_ERROR_CODE;
    payload.additionalDebugData = debugData;
    payload.lastStreamId = 45u;

    http2GoawayFramePayloadToBytes(&payload,
                                   buffer,
                                   HTTP2_GOAWAY_MINIMUM_PAYLOAD_LEN);

    http2GoawayFramePayloadFromBytes(&new_payload,
                                     buffer,
                                     HTTP2_GOAWAY_MINIMUM_PAYLOAD_LEN);

    TEST_ASSERT_EQUAL(payload.errorCode, new_payload.errorCode);
    TEST_ASSERT_EQUAL(payload.lastStreamId, new_payload.lastStreamId);

    http2Free(debugData);
}
