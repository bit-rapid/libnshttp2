/*
MIT License
-----------

Copyright (c) 2020 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <nshttp2/unity/unity.h>
#include <nshttp2/utility/memory.h>
#include "memory.h"
#include <string.h>

void test_http2_malloc(void) {
    void *test = http2Malloc(123u);

    TEST_ASSERT_NOT_NULL(test);

    http2Free(test);
}

void test_http2_calloc(void) {
    void *test = http2Calloc(100u);
    void *comparison = http2Malloc(100u);

    memset(comparison, 0, 100u);

    TEST_ASSERT_EQUAL(0,
                      memcmp(test, comparison, 100u));

    http2Free(comparison);
    http2Free(test);
}

void test_http2_free(void) {
    void *test = http2Calloc(100u);
    void *comparison = http2Malloc(100u);

    memset(comparison, 0, 100u);

    TEST_ASSERT_EQUAL(0,
                      memcmp(test, comparison, 100u));

    http2Free(comparison);
    http2Free(test);
}

void test_http2_realloc(void) {
    void *test = http2Malloc(5u);
    void *new_test = http2Realloc(test, strlen("123456789") + 1);

    if (new_test == NULL) {
        http2Free(test);
        return;
    }

    strcpy(new_test, "123456789");

    TEST_ASSERT_EQUAL(0, strcmp(new_test, "123456789"));

    http2Free(new_test);
}
