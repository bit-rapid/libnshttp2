/*
MIT License
-----------

Copyright (c) 2020 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include "string_utils.h"
#include <nshttp2/utility/string_utils.h>
#include <nshttp2/utility/memory.h>
#include <nshttp2/unity/unity.h>

void test_string_start_with_substring(void) {
    TEST_ASSERT_TRUE(stringStartWithSubstring("TEST", "T", 1u));
    TEST_ASSERT_TRUE(stringStartWithSubstring("TEST", "TE", 2u));
    TEST_ASSERT_TRUE(stringStartWithSubstring("TEST", "TES", 3u));
    TEST_ASSERT_TRUE(stringStartWithSubstring("TEST", "TEST", 4u));
    TEST_ASSERT_FALSE(stringStartWithSubstring("TEST", "PEST", 4u));
    TEST_ASSERT_FALSE(stringStartWithSubstring("TEST", "TESP", 4u));
}

void test_stringDuplicate(void) {
    char *test;
    stringDuplicate("whatever", &test);
    TEST_ASSERT_EQUAL_STRING("whatever", test);
    http2Free(test);
}
