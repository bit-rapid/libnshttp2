/*
MIT License
-----------

Copyright (c) 2020 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include "frame.h"
#include <nshttp2/frame.h>
#include <nshttp2/headers.h>
#include <nshttp2/unity/unity.h>
#include <string.h>
#include <nshttp2/utility/nghttp2.h>

void test_http2_is_non_zero_header_buffer(void) {
    uint8_t zero_buffer[HTTP2_FRAME_HEADER_SIZE] = {0};
    uint8_t non_zero_buffer[HTTP2_FRAME_HEADER_SIZE] = {1};

    TEST_ASSERT_FALSE(http2IsNonZeroHeaderBuffer(zero_buffer));
    TEST_ASSERT_TRUE(http2IsNonZeroHeaderBuffer(non_zero_buffer));
}

void test_http2_frame_header_to_bytes(void) {
    Http2FrameHeaderT frame_header;
    uint8_t buffer[HTTP2_FRAME_HEADER_SIZE];
    Http2FrameHeaderT new_frame_header;

    frame_header.length = 9u;
    frame_header.type = PRIORITY_FRAME_TYPE;
    frame_header.flags = PADDED_FRAME_FLAG;
    frame_header.streamId = 3u;

    http2FrameHeaderToBytes(&frame_header, buffer);

    http2FrameHeaderFromBytes(&new_frame_header, buffer);

    TEST_ASSERT_EQUAL(frame_header.length, new_frame_header.length);
    TEST_ASSERT_EQUAL(frame_header.type, new_frame_header.type);
    TEST_ASSERT_EQUAL(frame_header.streamId, new_frame_header.streamId);
    TEST_ASSERT_EQUAL(frame_header.flags, new_frame_header.flags);
}

void test_http2_frame_header_from_bytes(void) {
    Http2FrameHeaderT frame_header;
    uint8_t buffer[HTTP2_FRAME_HEADER_SIZE];
    Http2FrameHeaderT new_frame_header;

    frame_header.length = 9u;
    frame_header.type = PRIORITY_FRAME_TYPE;
    frame_header.streamId = 3u;
    frame_header.flags = PADDED_FRAME_FLAG;

    http2FrameHeaderToBytes(&frame_header, buffer);

    http2FrameHeaderFromBytes(&new_frame_header, buffer);

    TEST_ASSERT_EQUAL(frame_header.length, new_frame_header.length);
    TEST_ASSERT_EQUAL(frame_header.type, new_frame_header.type);
    TEST_ASSERT_EQUAL(frame_header.streamId, new_frame_header.streamId);
    TEST_ASSERT_EQUAL(frame_header.flags, new_frame_header.flags);
}

void test_http2_frame_from_bytes_header_set_data_frame(void) {
    Http2FrameHeaderT header;
    uint8_t data_frame_payload[5] = {'t', 'e', 's', 't', 's'};
    uint8_t padding[8] = {
            1u,
            2u,
            3u,
            4u,
            5u,
            6u,
            7u,
            8u
    };
    uint8_t buffer[14u + HTTP2_FRAME_HEADER_SIZE];
    Http2FramePaddingT frame_padding;
    Http2FrameT frame;
    uint8_t new_data_frame_payload[5];
    Http2FrameT new_frame;

    header.type = DATA_FRAME_TYPE;
    header.flags = PADDED_FRAME_FLAG;
    header.length = 14u;

    frame_padding.padding = padding;
    frame_padding.length = 8u;

    frame.header = header;
    frame.payload.http2DataFramePayload = data_frame_payload;

    if (http2FrameToBytes(&frame,
                          buffer,
                          &frame_padding) == UNSUPPORTED_FRAME_TYPE) {
        TEST_FAIL();
    }

    new_frame.header = header;
    new_frame.payload.http2DataFramePayload = new_data_frame_payload;

    if (http2FrameFromBytesHeaderSet(&new_frame,
                                     false,
                                     buffer) != HTTP2_SUCCESS) {
        TEST_FAIL();
    }

    TEST_ASSERT_EQUAL(0,
                      memcmp(new_frame.payload.http2DataFramePayload,
                             frame.payload.http2DataFramePayload,
                             new_frame.header.length));
}

void test_http2_frame_from_bytes_header_set_headers_frame(void) {
    Http2HeaderFieldT user_agent;
    Http2HeaderListElementT user_agent_element;
    Http2HeaderFieldT host;
    Http2HeaderListElementT host_element;
    Http2HeaderListT header_list;
    nghttp2_hd_deflater *nghttp2HdDeflater = NULL;
    nghttp2_hd_inflater *nghttp2HdInflater = NULL;
    Http2BufferT *buffer;
    Http2HeadersFramePayloadT payload;
    uint8_t padding[8] = {
            1u,
            2u,
            3u,
            4u,
            5u,
            6u,
            7u,
            8u
    };
    Http2FramePaddingT frame_padding;
    uint32_t frame_length;
    Http2FrameT frame;
    uint8_t *frameBuffer;
    uint8_t *newHeaderPayload;
    Http2HeadersFramePayloadT new_payload;
    Http2FrameT new_frame;

    user_agent.name = (uint8_t *) "user-agent";
    user_agent.nameLength = strlen("user-agent");
    user_agent.value = (uint8_t *) "curl/7.68.0";
    user_agent.valueLength = strlen("curl/7.68.0");

    user_agent_element.next = NULL;
    user_agent_element.http2HeaderField = &user_agent;

    host.name = (uint8_t *) "host";
    host.nameLength = strlen("host");
    host.value = (uint8_t *) "www.wu.ac.at";
    host.valueLength = strlen("www.wu.ac.at");

    host_element.next = &user_agent_element;
    host_element.http2HeaderField = &host;

    header_list.length = 2u;
    header_list.head = &host_element;
    header_list.tail = &user_agent_element;

    ngHttp2GetDeflater(&nghttp2HdDeflater, 4096u);

    ngHttp2GetInflater(&nghttp2HdInflater);

    http2HeadersEncode(&header_list,
                       &buffer,
                       nghttp2HdDeflater);

    payload.headerPayload = buffer->buffer;
    payload.priority.dependency = 20u;
    payload.priority.weight = 8u;
    payload.priority.exclusive = true;

    frame_padding.padding = padding;
    frame_padding.length = 8u;

    frame_length = sizeof(uint8_t) + buffer->length + frame_padding.length + HTTP2_PRIORITY_FRAME_PAYLOAD_SIZE;

    frame.header.length = frame_length;
    frame.header.flags = END_HEADERS_FRAME_FLAG | PADDED_FRAME_FLAG | PRIORITY_FRAME_FLAG;
    frame.header.streamId = 4u;
    frame.header.type = HEADERS_FRAME_TYPE;
    frame.payload.http2HeadersFramePayload = &payload;

    frameBuffer = http2Malloc(frame_length + HTTP2_FRAME_HEADER_SIZE);
    http2FrameToBytes(&frame,
                      frameBuffer,
                      &frame_padding);

    newHeaderPayload = http2Malloc(buffer->length);

    new_payload.headerPayload = newHeaderPayload;

    new_frame.header.length = frame_length;
    new_frame.header.flags = END_HEADERS_FRAME_FLAG | PADDED_FRAME_FLAG | PRIORITY_FRAME_FLAG;
    new_frame.header.streamId = 4u;
    new_frame.header.type = HEADERS_FRAME_TYPE;

    new_frame.payload.http2HeadersFramePayload = &new_payload;

    http2FrameFromBytesHeaderSet(&new_frame,
                                 false,
                                 frameBuffer);

    TEST_ASSERT_EQUAL(new_frame.payload.http2HeadersFramePayload->priority.exclusive,
                      frame.payload.http2HeadersFramePayload->priority.exclusive);

    TEST_ASSERT_EQUAL(new_frame.payload.http2HeadersFramePayload->priority.dependency,
                      frame.payload.http2HeadersFramePayload->priority.dependency);

    TEST_ASSERT_EQUAL(new_frame.payload.http2HeadersFramePayload->priority.weight,
                      frame.payload.http2HeadersFramePayload->priority.weight);

    TEST_ASSERT_EQUAL(0u, memcmp(new_frame.payload.http2HeadersFramePayload->headerPayload,
                                 frame.payload.http2HeadersFramePayload->headerPayload,
                                 buffer->length));

    http2BufferDeallocate(buffer);
    nghttp2_hd_inflate_del(nghttp2HdInflater);
    nghttp2_hd_deflate_del(nghttp2HdDeflater);
    http2Free(frameBuffer);
    http2Free(newHeaderPayload);
}

void test_http2_frame_is_non_control_frame(void) {
    Http2GoawayFramePayloadT frame_payload;
    Http2FrameT frame;
    Http2PriorityFramePayloadT priority;

    frame_payload.lastStreamId = 32u;
    frame_payload.additionalDebugData = NULL;
    frame_payload.errorCode = PROTOCOL_ERROR_ERROR_CODE;

    frame.header.length = HTTP2_GOAWAY_MINIMUM_PAYLOAD_LEN;
    frame.header.type = GOAWAY_FRAME_TYPE;
    frame.header.streamId = 0u;
    frame.payload.http2GoawayFramePayload = &frame_payload;

    TEST_ASSERT_FALSE(http2FrameIsNonControlFrame(&frame));

    priority.weight = 4u;
    priority.dependency = 32u;
    priority.exclusive = true;

    frame.header.length = HTTP2_PRIORITY_FRAME_PAYLOAD_SIZE;
    frame.header.type = PRIORITY_FRAME_TYPE;
    frame.header.streamId = 32u;
    frame.payload.http2PriorityFramePayload = &priority;

    TEST_ASSERT_TRUE(http2FrameIsNonControlFrame(&frame));
}

void test_http2_is_valid_connection_preface(void) {
    uint8_t buffer[HTTP2_CONNECTION_PREFACE_LEN] = HTTP2_CONNECTION_PREFACE;

    TEST_ASSERT_TRUE(http2IsValidConnectionPreface(buffer));
}

void test_http2_frame_is_control_frame(void) {
    Http2FrameT frame;
    Http2PriorityFramePayloadT priority;

    frame.header.length = HTTP2_GOAWAY_MINIMUM_PAYLOAD_LEN;
    frame.header.type = GOAWAY_FRAME_TYPE;
    frame.header.streamId = 0u;

    TEST_ASSERT_TRUE(http2FrameIsStreamFrame(&frame));

    priority.weight = 4u;
    priority.dependency = 32u;
    priority.exclusive = true;

    frame.header.length = HTTP2_PRIORITY_FRAME_PAYLOAD_SIZE;
    frame.header.type = PRIORITY_FRAME_TYPE;
    frame.header.streamId = 32u;
    frame.payload.http2PriorityFramePayload = &priority;

    TEST_ASSERT_FALSE(http2FrameIsStreamFrame(&frame));
}

void test_http2_frame_has_priority(void) {
    Http2FrameT frame;
    frame.header.streamId = 32u;
    frame.header.flags = PRIORITY_FRAME_FLAG | END_HEADERS_FRAME_FLAG;
    frame.header.length = 5u;

    TEST_ASSERT_TRUE(http2FrameHasPriority(&frame));

    frame.header.flags = END_HEADERS_FRAME_FLAG;

    TEST_ASSERT_FALSE(http2FrameHasPriority(&frame));
}

void test_http2_frame_is_last_header(void) {
    Http2FrameT frame;
    frame.header.streamId = 32u;
    frame.header.flags = PRIORITY_FRAME_FLAG | END_HEADERS_FRAME_FLAG;
    frame.header.length = 5u;
    frame.payload.http2DataFramePayload = NULL;

    TEST_ASSERT_TRUE(http2FrameIsLastHeader(&frame));

    frame.header.flags = END_HEADERS_FRAME_FLAG;

    TEST_ASSERT_TRUE(http2FrameIsLastHeader(&frame));
}

void test_http2_frame_type_name(void) {
    TEST_ASSERT_EQUAL(0, strcmp(http2FrameTypeName(DATA_FRAME_TYPE), DATA_FRAME_NAME));
    TEST_ASSERT_EQUAL(0, strcmp(http2FrameTypeName(HEADERS_FRAME_TYPE), HEADERS_FRAME_NAME));
    TEST_ASSERT_EQUAL(0, strcmp(http2FrameTypeName(PRIORITY_FRAME_TYPE), PRIORITY_FRAME_NAME));
    TEST_ASSERT_EQUAL(0, strcmp(http2FrameTypeName(RST_STREAM_FRAME_TYPE), RST_STREAM_FRAME_NAME));
    TEST_ASSERT_EQUAL(0, strcmp(http2FrameTypeName(SETTINGS_FRAME_TYPE), SETTINGS_FRAME_NAME));
    TEST_ASSERT_EQUAL(0, strcmp(http2FrameTypeName(PUSH_PROMISE_FRAME_TYPE), PUSH_PROMISE_FRAME_NAME));
    TEST_ASSERT_EQUAL(0, strcmp(http2FrameTypeName(PING_FRAME_TYPE), PING_FRAME_NAME));
    TEST_ASSERT_EQUAL(0, strcmp(http2FrameTypeName(GOAWAY_FRAME_TYPE), GOAWAY_FRAME_NAME));
    TEST_ASSERT_EQUAL(0, strcmp(http2FrameTypeName(WINDOW_UPDATE_FRAME_TYPE), WINDOW_UPDATE_FRAME_NAME));
    TEST_ASSERT_EQUAL(0, strcmp(http2FrameTypeName(CONTINUATION_FRAME_TYPE), CONTINUATION_FRAME_NAME));
}

void test_http2_frame_from_bytes_header_set_priority_frame(void) {
    Http2PriorityFramePayloadT payload;
    Http2FrameT frame;
    uint8_t padding[8] = {
            1u,
            2u,
            3u,
            4u,
            5u,
            6u,
            7u,
            8u
    };
    Http2FramePaddingT frame_padding;
    uint8_t buffer[HTTP2_PRIORITY_FRAME_PAYLOAD_SIZE + HTTP2_FRAME_HEADER_SIZE];
    Http2PriorityFramePayloadT new_payload;
    Http2FrameT new_frame;

    payload.weight = 4u;
    payload.dependency = 5u;
    payload.exclusive = true;

    frame.header.length = HTTP2_PRIORITY_FRAME_PAYLOAD_SIZE;
    frame.header.type = PRIORITY_FRAME_TYPE;
    frame.header.streamId = 3u;
    frame.header.flags = DEFAULT_FRAME_FLAG;
    frame.payload.http2PriorityFramePayload = &payload;

    frame_padding.padding = padding;
    frame_padding.length = 8u;

    http2FrameToBytes(&frame,
                      buffer,
                      &frame_padding);

    new_frame.header.length = HTTP2_PRIORITY_FRAME_PAYLOAD_SIZE;
    new_frame.header.flags = DEFAULT_FRAME_FLAG;
    new_frame.header.streamId = 3u;
    new_frame.header.type = PRIORITY_FRAME_TYPE;
    new_frame.payload.http2PriorityFramePayload = &new_payload;

    http2FrameFromBytesHeaderSet(&new_frame,
                                 false,
                                 buffer);

    TEST_ASSERT_EQUAL(frame.payload.http2PriorityFramePayload->exclusive,
                      new_frame.payload.http2PriorityFramePayload->exclusive);

    TEST_ASSERT_EQUAL(frame.payload.http2PriorityFramePayload->weight,
                      new_frame.payload.http2PriorityFramePayload->weight);

    TEST_ASSERT_EQUAL(frame.payload.http2PriorityFramePayload->dependency,
                      new_frame.payload.http2PriorityFramePayload->dependency);
}

void test_http2_frame_from_bytes_header_set_rst_stream_frame(void) {
    Http2RstStreamFramePayloadT payload;
    Http2FrameHeaderT header;
    Http2FrameT frame;
    Http2RstStreamFramePayloadT new_payload;
    Http2FrameT new_frame;
    uint8_t buffer[HTTP2_RST_STREAM_FRAME_PAYLOAD_SIZE + HTTP2_FRAME_HEADER_SIZE];

    payload.errorCode = PROTOCOL_ERROR_ERROR_CODE;

    header.streamId = 4u;
    header.length = HTTP2_RST_STREAM_FRAME_PAYLOAD_SIZE;
    header.type = RST_STREAM_FRAME_TYPE;

    frame.header = header;
    frame.payload.http2RstStreamFramePayload = &payload;

    new_frame.header = header;
    new_frame.payload.http2RstStreamFramePayload = &new_payload;

    http2FrameToBytes(&frame,
                      buffer,
                      NULL);

    http2FrameFromBytesHeaderSet(&new_frame,
                                 false,
                                 buffer);

    TEST_ASSERT_EQUAL(frame.payload.http2RstStreamFramePayload->errorCode,
                      new_frame.payload.http2RstStreamFramePayload->errorCode);

}

void test_http2_frame_from_bytes_header_set_settings_frame(void) {
    Http2SettingsFramePayloadT payload;
    Http2FrameHeaderT header;
    Http2FrameT frame;
    uint8_t buffer[HTTP2_SETTINGS_FORMAT_SIZE * 5 + HTTP2_FRAME_HEADER_SIZE];
    Http2SettingsFramePayloadT new_payload;
    Http2FrameT new_frame;

    payload.initialWindowSize = 80000u;
    payload.maxHeaderListSize = 90000u;
    payload.maxFrameSize = 32000u;
    payload.maxConcurrentStreams = 100u;
    payload.enablePush = true;
    payload.headerTableSize = 8192u;

    header.type = SETTINGS_FRAME_TYPE;
    header.length = HTTP2_SETTINGS_FORMAT_SIZE * 5;
    header.flags = DEFAULT_FRAME_FLAG;
    header.streamId = 0u;

    frame.header = header;
    frame.payload.http2SettingsFramePayload = &payload;

    http2FrameToBytes(&frame,
                      buffer,
                      NULL);

    new_frame.header = header;
    new_frame.payload.http2SettingsFramePayload = &new_payload;

    http2FrameFromBytesHeaderSet(&new_frame,
                                 false,
                                 buffer);

    TEST_ASSERT_EQUAL(frame.payload.http2SettingsFramePayload->initialWindowSize,
                      new_frame.payload.http2SettingsFramePayload->initialWindowSize);
    TEST_ASSERT_EQUAL(frame.payload.http2SettingsFramePayload->maxHeaderListSize,
                      new_frame.payload.http2SettingsFramePayload->maxHeaderListSize);
    TEST_ASSERT_EQUAL(frame.payload.http2SettingsFramePayload->maxFrameSize,
                      new_frame.payload.http2SettingsFramePayload->maxFrameSize);
    TEST_ASSERT_EQUAL(frame.payload.http2SettingsFramePayload->maxConcurrentStreams,
                      new_frame.payload.http2SettingsFramePayload->maxConcurrentStreams);
    TEST_ASSERT_EQUAL(frame.payload.http2SettingsFramePayload->enablePush,
                      new_frame.payload.http2SettingsFramePayload->enablePush);
    TEST_ASSERT_EQUAL(frame.payload.http2SettingsFramePayload->headerTableSize,
                      new_frame.payload.http2SettingsFramePayload->headerTableSize);
}

void test_http2_frame_from_bytes_header_set_push_promise_frame(void) {
    Http2HeaderFieldT host;
    Http2HeaderFieldT user_agent;
    Http2HeaderListElementT user_agent_element;
    Http2HeaderListElementT host_element;
    Http2HeaderListT header_list;
    nghttp2_hd_deflater *nghttp2HdDeflater = NULL;
    nghttp2_hd_inflater *nghttp2HdInflater = NULL;
    Http2BufferT *buffer;
    Http2PushPromiseFramePayloadT payload;
    Http2FrameHeaderT header;
    Http2FrameT frame;
    uint8_t *frame_buffer;
    uint8_t *header_block;
    Http2PushPromiseFramePayloadT new_payload;
    Http2FrameT new_frame;

    host.name = (uint8_t *) "host";
    host.nameLength = strlen("host");
    host.value = (uint8_t *) "www.wu.ac.at";
    host.valueLength = strlen("www.wu.ac.at");

    user_agent.name = (uint8_t *) "user-agent";
    user_agent.nameLength = strlen("user-agent");
    user_agent.value = (uint8_t *) "curl/7.68.0";
    user_agent.valueLength = strlen("curl/7.68.0");

    user_agent_element.next = NULL;
    user_agent_element.http2HeaderField = &user_agent;

    host_element.next = &user_agent_element;
    host_element.http2HeaderField = &host;

    header_list.length = 2u;
    header_list.head = &host_element;
    header_list.tail = &user_agent_element;

    ngHttp2GetDeflater(&nghttp2HdDeflater, 4096u);

    ngHttp2GetInflater(&nghttp2HdInflater);

    http2HeadersEncode(&header_list,
                       &buffer,
                       nghttp2HdDeflater);

    payload.headerBlockFragment = buffer->buffer;
    payload.promisedStreamId = 23u;

    header.type = PUSH_PROMISE_FRAME_TYPE;
    header.streamId = 3u;
    header.length = (uint32_t) (buffer->length + sizeof(uint32_t));
    header.flags = DEFAULT_FRAME_FLAG;

    frame.header = header;
    frame.payload.http2PushPromiseFramePayload = &payload;

    frame_buffer = http2Malloc(buffer->length + sizeof(uint32_t) + HTTP2_FRAME_HEADER_SIZE);

    http2FrameToBytes(&frame,
                      frame_buffer,
                      NULL);

    header_block = http2Malloc(buffer->length);

    new_payload.headerBlockFragment = header_block;

    new_frame.header = header;
    new_frame.payload.http2PushPromiseFramePayload = &new_payload;

    http2FrameFromBytesHeaderSet(&new_frame,
                                 false,
                                 frame_buffer);

    TEST_ASSERT_EQUAL(frame.payload.http2PushPromiseFramePayload->promisedStreamId,
                      new_frame.payload.http2PushPromiseFramePayload->promisedStreamId);

    TEST_ASSERT_EQUAL(0,
                      memcmp(frame.payload.http2PushPromiseFramePayload->headerBlockFragment,
                             new_frame.payload.http2PushPromiseFramePayload->headerBlockFragment,
                             buffer->length));

    http2BufferDeallocate(buffer);
    nghttp2_hd_deflate_del(nghttp2HdDeflater);
    nghttp2_hd_inflate_del(nghttp2HdInflater);
    http2Free(frame_buffer);
    http2Free(header_block);
}

void test_http2_frame_from_bytes_header_set_ping_frame(void) {
    Http2PingFramePayloadT payload;
    uint8_t i;
    Http2FrameHeaderT header;
    Http2FrameT frame;
    Http2PingFramePayloadT new_payload;
    Http2FrameT new_frame;
    uint8_t buffer[HTTP2_PING_FRAME_PAYLOAD_SIZE + HTTP2_FRAME_HEADER_SIZE];

    for (i = 0u; i < 8u; ++i) {
        payload.opaqueData[i] = i;
    }

    header.length = HTTP2_PING_FRAME_PAYLOAD_SIZE;
    header.streamId = 3u;
    header.type = PING_FRAME_TYPE;
    header.flags = DEFAULT_FRAME_FLAG;

    frame.header = header;
    frame.payload.http2PingFramePayload = &payload;

    new_frame.header = header;
    new_frame.payload.http2PingFramePayload = &new_payload;


    http2FrameToBytes(&frame,
                      buffer,
                      NULL);

    http2FrameFromBytesHeaderSet(&new_frame,
                                 false,
                                 buffer);

    TEST_ASSERT_EQUAL(0, memcmp(frame.payload.http2PingFramePayload->opaqueData,
                                new_frame.payload.http2PingFramePayload->opaqueData,
                                HTTP2_PING_FRAME_PAYLOAD_SIZE));
}

void test_http2_frame_from_bytes_header_set_goaway_frame(void) {
    Http2GoawayFramePayloadT payload;
    Http2FrameHeaderT header;
    Http2FrameT frame;
    uint8_t buffer[HTTP2_GOAWAY_MINIMUM_PAYLOAD_LEN + HTTP2_FRAME_HEADER_SIZE];
    Http2GoawayFramePayloadT new_payload;
    Http2FrameT new_frame;

    payload.errorCode = PROTOCOL_ERROR_ERROR_CODE;
    payload.additionalDebugData = NULL;
    payload.lastStreamId = 45u;

    header.type = GOAWAY_FRAME_TYPE;
    header.flags = 0u;
    header.streamId = 0u;
    header.streamId = 0u;
    header.length = HTTP2_GOAWAY_MINIMUM_PAYLOAD_LEN;

    frame.header = header;
    frame.payload.http2GoawayFramePayload = &payload;

    http2FrameToBytes(&frame,
                      buffer,
                      NULL);

    new_frame.header = header;
    new_frame.payload.http2GoawayFramePayload = &new_payload;

    http2FrameFromBytesHeaderSet(&new_frame,
                                 false,
                                 buffer);

    TEST_ASSERT_EQUAL(frame.payload.http2GoawayFramePayload->errorCode,
                      new_frame.payload.http2GoawayFramePayload->errorCode);
    TEST_ASSERT_EQUAL(frame.payload.http2GoawayFramePayload->lastStreamId,
                      new_frame.payload.http2GoawayFramePayload->lastStreamId);
}

void test_http2_frame_from_bytes_header_set_window_update_frame(void) {
    Http2WindowUpdatePayloadT payload;
    Http2FrameHeaderT header;
    Http2FrameT frame;
    uint8_t buffer[HTTP2_WINDOW_UPDATE_PAYLOAD_LEN + HTTP2_FRAME_HEADER_SIZE];
    Http2WindowUpdatePayloadT new_payload;
    Http2FrameT new_frame;

    payload.windowSizeIncrement = UINT16_MAX;

    header.type = WINDOW_UPDATE_FRAME_TYPE;
    header.streamId = 0u;
    header.flags = DEFAULT_FRAME_FLAG;
    header.length = HTTP2_WINDOW_UPDATE_PAYLOAD_LEN;

    frame.header = header;
    frame.payload.http2WindowUpdatePayload = &payload;

    http2FrameToBytes(&frame,
                      buffer,
                      NULL);

    new_frame.header = header;
    new_frame.payload.http2WindowUpdatePayload = &new_payload;

    http2FrameFromBytesHeaderSet(&new_frame,
                                 false,
                                 buffer);

    TEST_ASSERT_EQUAL(frame.payload.http2WindowUpdatePayload->windowSizeIncrement,
                      new_frame.payload.http2WindowUpdatePayload->windowSizeIncrement);
}

void test_http2_frame_from_bytes_header_set_continuation_frame(void) {
    Http2HeaderFieldT host;
    Http2HeaderFieldT user_agent;
    Http2HeaderListElementT user_agent_element;
    Http2HeaderListElementT host_element;
    Http2HeaderListT header_list;
    nghttp2_hd_deflater *nghttp2HdDeflater = NULL;
    nghttp2_hd_inflater *nghttp2HdInflater = NULL;
    Http2BufferT *buffer;
    Http2ContinuationPayloadT payload;
    uint8_t *header_block;
    Http2ContinuationPayloadT new_payload;
    Http2FrameHeaderT header;
    uint8_t *frame_buffer;
    Http2FrameT frame;
    Http2FrameT new_frame;

    host.name = (uint8_t *) "host";
    host.nameLength = strlen("host");
    host.value = (uint8_t *) "www.wu.ac.at";
    host.valueLength = strlen("www.wu.ac.at");

    user_agent.name = (uint8_t *) "user-agent";
    user_agent.nameLength = strlen("user-agent");
    user_agent.value = (uint8_t *) "curl/7.68.0";
    user_agent.valueLength = strlen("curl/7.68.0");

    user_agent_element.next = NULL;
    user_agent_element.http2HeaderField = &user_agent;

    host_element.next = &user_agent_element;
    host_element.http2HeaderField = &host;

    header_list.length = 2u;
    header_list.head = &host_element;
    header_list.tail = &user_agent_element;

    ngHttp2GetDeflater(&nghttp2HdDeflater, 4096u);

    ngHttp2GetInflater(&nghttp2HdInflater);

    http2HeadersEncode(&header_list,
                       &buffer,
                       nghttp2HdDeflater);

    payload.headerBlockFragment = buffer->buffer;

    header_block = http2Malloc(buffer->length);

    new_payload.headerBlockFragment = header_block;

    header.length = buffer->length;
    header.flags = END_HEADERS_FRAME_FLAG;
    header.streamId = 4u;
    header.type = CONTINUATION_FRAME_TYPE;

    frame_buffer = http2Malloc(buffer->length + HTTP2_FRAME_HEADER_SIZE);

    frame.header = header;
    frame.payload.http2ContinuationPayload = &payload;

    new_frame.header = header;
    new_frame.payload.http2ContinuationPayload = &new_payload;

    http2FrameToBytes(&frame,
                      frame_buffer,
                      NULL);

    http2FrameFromBytesHeaderSet(&new_frame,
                                 false,
                                 frame_buffer);

    TEST_ASSERT_EQUAL(0, memcmp(
            frame.payload.http2ContinuationPayload->headerBlockFragment,
            new_frame.payload.http2ContinuationPayload->headerBlockFragment,
            buffer->length));

    http2BufferDeallocate(buffer);
    nghttp2_hd_inflate_del(nghttp2HdInflater);
    nghttp2_hd_deflate_del(nghttp2HdDeflater);
    http2Free(header_block);
    http2Free(frame_buffer);
}

void test_http2_frame_from_bytes_data_frame(void) {
    Http2FrameHeaderT header;
    uint8_t data_frame_payload[5] = {'t', 'e', 's', 't', 's'};
    uint8_t padding[8] = {
            1u,
            2u,
            3u,
            4u,
            5u,
            6u,
            7u,
            8u
    };
    uint8_t buffer[14u + HTTP2_FRAME_HEADER_SIZE];
    Http2FramePaddingT frame_padding;
    Http2FrameT frame;
    uint8_t new_data_frame_payload[5];
    Http2FrameT new_frame;

    header.type = DATA_FRAME_TYPE;
    header.flags = PADDED_FRAME_FLAG;
    header.length = 14u;
    header.streamId = 512u;

    frame_padding.padding = padding;
    frame_padding.length = 8u;

    frame.header = header;
    frame.payload.http2DataFramePayload = data_frame_payload;

    if (http2FrameToBytes(&frame,
                          buffer,
                          &frame_padding) == UNSUPPORTED_FRAME_TYPE) {
        TEST_FAIL();
    }

    new_frame.payload.http2DataFramePayload = new_data_frame_payload;

    if (http2FrameFromBytes(&new_frame,
                            false,
                            buffer) != HTTP2_SUCCESS) {
        TEST_FAIL();
    }

    TEST_ASSERT_EQUAL(frame.header.type,
                      new_frame.header.type);

    TEST_ASSERT_EQUAL(frame.header.streamId,
                      new_frame.header.streamId);

    TEST_ASSERT_EQUAL(frame.header.flags,
                      new_frame.header.flags);

    TEST_ASSERT_EQUAL(0,
                      memcmp(new_frame.payload.http2DataFramePayload,
                             frame.payload.http2DataFramePayload,
                             new_frame.header.length));
}

void test_http2_frame_from_bytes_headers_frame(void) {
    Http2HeaderFieldT user_agent;
    Http2HeaderListElementT user_agent_element;
    Http2HeaderFieldT host;
    Http2HeaderListElementT host_element;
    Http2HeaderListT header_list;
    nghttp2_hd_deflater *nghttp2HdDeflater = NULL;
    nghttp2_hd_inflater *nghttp2HdInflater = NULL;
    Http2BufferT *buffer;
    Http2HeadersFramePayloadT payload;
    uint8_t padding[8] = {
            1u,
            2u,
            3u,
            4u,
            5u,
            6u,
            7u,
            8u
    };
    Http2FramePaddingT frame_padding;
    uint32_t frame_length;
    Http2FrameT frame;
    uint8_t *new_header_payload;
    uint8_t *frame_buffer;
    Http2HeadersFramePayloadT new_payload;
    Http2FrameT new_frame;

    user_agent.name = (uint8_t *) "user-agent";
    user_agent.nameLength = strlen("user-agent");
    user_agent.value = (uint8_t *) "curl/7.68.0";
    user_agent.valueLength = strlen("curl/7.68.0");

    user_agent_element.next = NULL;
    user_agent_element.http2HeaderField = &user_agent;

    host.name = (uint8_t *) "host";
    host.nameLength = strlen("host");
    host.value = (uint8_t *) "www.wu.ac.at";
    host.valueLength = strlen("www.wu.ac.at");

    host_element.http2HeaderField = &host;
    host_element.next = &user_agent_element;

    header_list.length = 2u;
    header_list.head = &host_element;
    header_list.tail = &user_agent_element;

    ngHttp2GetDeflater(&nghttp2HdDeflater, 4096u);

    ngHttp2GetInflater(&nghttp2HdInflater);

    http2HeadersEncode(&header_list,
                       &buffer,
                       nghttp2HdDeflater);

    payload.headerPayload = buffer->buffer;
    payload.priority.dependency = 20u;
    payload.priority.weight = 8u;
    payload.priority.exclusive = true;

    frame_padding.padding = padding;
    frame_padding.length = 8u;

    frame_length = sizeof(uint8_t) + buffer->length + frame_padding.length + HTTP2_PRIORITY_FRAME_PAYLOAD_SIZE;

    frame.header.length = frame_length;
    frame.header.streamId = 4u;
    frame.header.type = HEADERS_FRAME_TYPE;
    frame.header.flags = END_HEADERS_FRAME_FLAG | PADDED_FRAME_FLAG | PRIORITY_FRAME_FLAG;
    frame.payload.http2HeadersFramePayload = &payload;

    frame_buffer = http2Malloc(HTTP2_FRAME_HEADER_SIZE + frame.header.length);

    http2FrameToBytes(&frame,
                      frame_buffer,
                      &frame_padding);

    new_header_payload = http2Malloc(buffer->length);

    new_payload.headerPayload = new_header_payload;

    new_frame.payload.http2HeadersFramePayload = &new_payload;

    http2FrameFromBytes(&new_frame,
                        false,
                        frame_buffer);

    TEST_ASSERT_EQUAL(frame.header.type,
                      new_frame.header.type);

    TEST_ASSERT_EQUAL(frame.header.streamId,
                      new_frame.header.streamId);

    TEST_ASSERT_EQUAL(frame.header.flags,
                      new_frame.header.flags);

    TEST_ASSERT_EQUAL(new_frame.payload.http2HeadersFramePayload->priority.exclusive,
                      frame.payload.http2HeadersFramePayload->priority.exclusive);

    TEST_ASSERT_EQUAL(new_frame.payload.http2HeadersFramePayload->priority.dependency,
                      frame.payload.http2HeadersFramePayload->priority.dependency);

    TEST_ASSERT_EQUAL(new_frame.payload.http2HeadersFramePayload->priority.weight,
                      frame.payload.http2HeadersFramePayload->priority.weight);

    TEST_ASSERT_EQUAL(0u, memcmp(new_frame.payload.http2HeadersFramePayload->headerPayload,
                                 frame.payload.http2HeadersFramePayload->headerPayload,
                                 buffer->length));

    http2BufferDeallocate(buffer);
    nghttp2_hd_deflate_del(nghttp2HdDeflater);
    nghttp2_hd_inflate_del(nghttp2HdInflater);
    http2Free(frame_buffer);
    http2Free(new_header_payload);
}

void test_http2_frame_from_bytes_priority_frame(void) {
    Http2PriorityFramePayloadT payload;
    Http2FrameT frame;
    uint8_t padding[8] = {
            1u,
            2u,
            3u,
            4u,
            5u,
            6u,
            7u,
            8u
    };
    Http2FramePaddingT frame_padding;
    uint8_t buffer[HTTP2_PRIORITY_FRAME_PAYLOAD_SIZE + HTTP2_FRAME_HEADER_SIZE];
    Http2PriorityFramePayloadT new_payload;
    Http2FrameT new_frame;

    payload.weight = 4u;
    payload.dependency = 5u;
    payload.exclusive = true;

    frame.header.length = HTTP2_PRIORITY_FRAME_PAYLOAD_SIZE;
    frame.header.type = PRIORITY_FRAME_TYPE;
    frame.header.streamId = 3u;
    frame.header.flags = DEFAULT_FRAME_FLAG;
    frame.payload.http2PriorityFramePayload = &payload;

    frame_padding.padding = padding;
    frame_padding.length = 8u;

    http2FrameToBytes(&frame,
                      buffer,
                      &frame_padding);

    new_frame.payload.http2PriorityFramePayload = &new_payload;

    http2FrameFromBytes(&new_frame,
                        false,
                        buffer);

    TEST_ASSERT_EQUAL(frame.header.type,
                      new_frame.header.type);

    TEST_ASSERT_EQUAL(frame.header.streamId,
                      new_frame.header.streamId);

    TEST_ASSERT_EQUAL(frame.header.flags,
                      new_frame.header.flags);

    TEST_ASSERT_EQUAL(frame.payload.http2PriorityFramePayload->exclusive,
                      new_frame.payload.http2PriorityFramePayload->exclusive);

    TEST_ASSERT_EQUAL(frame.payload.http2PriorityFramePayload->weight,
                      new_frame.payload.http2PriorityFramePayload->weight);

    TEST_ASSERT_EQUAL(frame.payload.http2PriorityFramePayload->dependency,
                      new_frame.payload.http2PriorityFramePayload->dependency);
}

void test_http2_frame_from_bytes_rst_stream_frame(void) {
    Http2RstStreamFramePayloadT payload;
    Http2FrameHeaderT header;
    Http2FrameT frame;
    Http2RstStreamFramePayloadT new_payload;
    Http2FrameT new_frame;
    uint8_t buffer[HTTP2_RST_STREAM_FRAME_PAYLOAD_SIZE + HTTP2_FRAME_HEADER_SIZE];

    payload.errorCode = PROTOCOL_ERROR_ERROR_CODE;

    header.streamId = 4u;
    header.length = HTTP2_RST_STREAM_FRAME_PAYLOAD_SIZE;
    header.type = RST_STREAM_FRAME_TYPE;
    header.flags = DEFAULT_FRAME_FLAG;

    frame.header = header;
    frame.payload.http2RstStreamFramePayload = &payload;

    new_frame.payload.http2RstStreamFramePayload = &new_payload;

    http2FrameToBytes(&frame,
                      buffer,
                      NULL);

    http2FrameFromBytes(&new_frame,
                        false,
                        buffer);

    TEST_ASSERT_EQUAL(frame.header.type,
                      new_frame.header.type);

    TEST_ASSERT_EQUAL(frame.header.streamId,
                      new_frame.header.streamId);

    TEST_ASSERT_EQUAL(frame.header.flags,
                      new_frame.header.flags);

    TEST_ASSERT_EQUAL(frame.payload.http2RstStreamFramePayload->errorCode,
                      new_frame.payload.http2RstStreamFramePayload->errorCode);
}

void test_http2_frame_from_bytes_settings_frame(void) {
    Http2SettingsFramePayloadT payload;
    Http2FrameHeaderT header;
    Http2FrameT frame;
    uint8_t buffer[HTTP2_SETTINGS_FORMAT_SIZE * 5 + HTTP2_FRAME_HEADER_SIZE];
    Http2SettingsFramePayloadT new_payload;
    Http2FrameT new_frame;

    payload.initialWindowSize = 80000u;
    payload.maxHeaderListSize = 90000u;
    payload.maxFrameSize = 32000u;
    payload.maxConcurrentStreams = 100u;
    payload.enablePush = true;
    payload.headerTableSize = 8192u;

    header.type = SETTINGS_FRAME_TYPE;
    header.length = HTTP2_SETTINGS_FORMAT_SIZE * 5;
    header.flags = DEFAULT_FRAME_FLAG;
    header.streamId = 0u;

    frame.header = header;
    frame.payload.http2SettingsFramePayload = &payload;

    http2FrameToBytes(&frame,
                      buffer,
                      NULL);

    new_frame.payload.http2SettingsFramePayload = &new_payload;

    http2FrameFromBytes(&new_frame,
                        false,
                        buffer);

    TEST_ASSERT_EQUAL(frame.header.type,
                      new_frame.header.type);

    TEST_ASSERT_EQUAL(frame.header.streamId,
                      new_frame.header.streamId);

    TEST_ASSERT_EQUAL(frame.header.flags,
                      new_frame.header.flags);

    TEST_ASSERT_EQUAL(frame.payload.http2SettingsFramePayload->initialWindowSize,
                      new_frame.payload.http2SettingsFramePayload->initialWindowSize);
    TEST_ASSERT_EQUAL(frame.payload.http2SettingsFramePayload->maxHeaderListSize,
                      new_frame.payload.http2SettingsFramePayload->maxHeaderListSize);
    TEST_ASSERT_EQUAL(frame.payload.http2SettingsFramePayload->maxFrameSize,
                      new_frame.payload.http2SettingsFramePayload->maxFrameSize);
    TEST_ASSERT_EQUAL(frame.payload.http2SettingsFramePayload->maxConcurrentStreams,
                      new_frame.payload.http2SettingsFramePayload->maxConcurrentStreams);
    TEST_ASSERT_EQUAL(frame.payload.http2SettingsFramePayload->enablePush,
                      new_frame.payload.http2SettingsFramePayload->enablePush);
    TEST_ASSERT_EQUAL(frame.payload.http2SettingsFramePayload->headerTableSize,
                      new_frame.payload.http2SettingsFramePayload->headerTableSize);
}

void test_http2_frame_from_bytes_push_promise_frame(void) {
    Http2HeaderFieldT host;
    Http2HeaderFieldT user_agent;
    Http2HeaderListElementT user_agent_element;
    Http2HeaderListElementT host_element;
    Http2HeaderListT header_list;
    nghttp2_hd_deflater *nghttp2HdDeflater = NULL;
    nghttp2_hd_inflater *nghttp2HdInflater = NULL;
    Http2BufferT *buffer;
    Http2PushPromiseFramePayloadT payload;
    Http2FrameHeaderT header;
    Http2FrameT frame;
    uint8_t *frame_buffer;
    uint8_t *header_block;
    Http2PushPromiseFramePayloadT new_payload;
    Http2FrameT new_frame;

    host.name = (uint8_t *) "host";
    host.nameLength = strlen("host");
    host.value = (uint8_t *) "www.wu.ac.at";
    host.valueLength = strlen("www.wu.ac.at");

    user_agent.name = (uint8_t *) "user-agent";
    user_agent.nameLength = strlen("user-agent");
    user_agent.value = (uint8_t *) "curl/7.68.0";
    user_agent.valueLength = strlen("curl/7.68.0");

    user_agent_element.next = NULL;
    user_agent_element.http2HeaderField = &user_agent;

    host_element.next = &user_agent_element;
    host_element.http2HeaderField = &host;

    header_list.length = 2u;
    header_list.head = &host_element;
    header_list.tail = &user_agent_element;

    ngHttp2GetDeflater(&nghttp2HdDeflater, 4096u);

    ngHttp2GetInflater(&nghttp2HdInflater);

    http2HeadersEncode(&header_list,
                       &buffer,
                       nghttp2HdDeflater);

    payload.headerBlockFragment = buffer->buffer;
    payload.promisedStreamId = 23u;

    header.type = PUSH_PROMISE_FRAME_TYPE;
    header.streamId = 3u;
    header.length = (uint32_t) (buffer->length + sizeof(uint32_t));
    header.flags = DEFAULT_FRAME_FLAG;

    frame.header = header;
    frame.payload.http2PushPromiseFramePayload = &payload;

    frame_buffer = http2Malloc(buffer->length + sizeof(uint32_t) + HTTP2_FRAME_HEADER_SIZE);

    http2FrameToBytes(&frame,
                      frame_buffer,
                      NULL);

    header_block = http2Malloc(buffer->length);

    new_payload.headerBlockFragment = header_block;

    new_frame.payload.http2PushPromiseFramePayload = &new_payload;

    http2FrameFromBytes(&new_frame,
                        false,
                        frame_buffer);

    TEST_ASSERT_EQUAL(frame.header.type,
                      new_frame.header.type);

    TEST_ASSERT_EQUAL(frame.header.streamId,
                      new_frame.header.streamId);

    TEST_ASSERT_EQUAL(frame.header.flags,
                      new_frame.header.flags);

    TEST_ASSERT_EQUAL(frame.payload.http2PushPromiseFramePayload->promisedStreamId,
                      new_frame.payload.http2PushPromiseFramePayload->promisedStreamId);

    TEST_ASSERT_EQUAL(0,
                      memcmp(frame.payload.http2PushPromiseFramePayload->headerBlockFragment,
                             new_frame.payload.http2PushPromiseFramePayload->headerBlockFragment,
                             buffer->length));

    http2BufferDeallocate(buffer);
    nghttp2_hd_inflate_del(nghttp2HdInflater);
    nghttp2_hd_deflate_del(nghttp2HdDeflater);
    http2Free(frame_buffer);
    http2Free(header_block);
}

void test_http2_frame_from_bytes_ping_frame(void) {
    Http2PingFramePayloadT payload;
    uint8_t i;
    Http2FrameHeaderT header;
    Http2FrameT frame;
    Http2PingFramePayloadT new_payload;
    Http2FrameT new_frame;
    uint8_t buffer[HTTP2_PING_FRAME_PAYLOAD_SIZE + HTTP2_FRAME_HEADER_SIZE];

    for (i = 0u; i < 8; ++i) {
        payload.opaqueData[i] = i;
    }

    header.length = HTTP2_PING_FRAME_PAYLOAD_SIZE;
    header.streamId = 3u;
    header.type = PING_FRAME_TYPE;
    header.flags = DEFAULT_FRAME_FLAG;

    frame.header = header;
    frame.payload.http2PingFramePayload = &payload;

    new_frame.header = header;
    new_frame.payload.http2PingFramePayload = &new_payload;

    http2FrameToBytes(&frame,
                      buffer,
                      NULL);

    http2FrameFromBytesHeaderSet(&new_frame,
                                 false,
                                 buffer);

    TEST_ASSERT_EQUAL(frame.header.type,
                      new_frame.header.type);

    TEST_ASSERT_EQUAL(frame.header.streamId,
                      new_frame.header.streamId);

    TEST_ASSERT_EQUAL(frame.header.flags,
                      new_frame.header.flags);

    TEST_ASSERT_EQUAL(0, memcmp(frame.payload.http2PingFramePayload->opaqueData,
                                new_frame.payload.http2PingFramePayload->opaqueData,
                                HTTP2_PING_FRAME_PAYLOAD_SIZE));
}

void test_http2_frame_from_bytes_goaway_frame(void) {
    Http2GoawayFramePayloadT payload;
    Http2FrameHeaderT header;
    Http2FrameT frame;
    uint8_t buffer[HTTP2_GOAWAY_MINIMUM_PAYLOAD_LEN + HTTP2_FRAME_HEADER_SIZE];
    Http2GoawayFramePayloadT new_payload;
    Http2FrameT new_frame;

    payload.errorCode = PROTOCOL_ERROR_ERROR_CODE;
    payload.additionalDebugData = NULL;
    payload.lastStreamId = 45u;

    header.type = GOAWAY_FRAME_TYPE;
    header.flags = DEFAULT_FRAME_FLAG;
    header.streamId = 0u;
    header.length = HTTP2_GOAWAY_MINIMUM_PAYLOAD_LEN;

    frame.header = header;
    frame.payload.http2GoawayFramePayload = &payload;

    http2FrameToBytes(&frame,
                      buffer,
                      NULL);

    new_frame.payload.http2GoawayFramePayload = &new_payload;

    http2FrameFromBytes(&new_frame,
                        false,
                        buffer);

    TEST_ASSERT_EQUAL(frame.header.type,
                      new_frame.header.type);

    TEST_ASSERT_EQUAL(frame.header.streamId,
                      new_frame.header.streamId);

    TEST_ASSERT_EQUAL(frame.header.flags,
                      new_frame.header.flags);

    TEST_ASSERT_EQUAL(frame.payload.http2GoawayFramePayload->errorCode,
                      new_frame.payload.http2GoawayFramePayload->errorCode);
    TEST_ASSERT_EQUAL(frame.payload.http2GoawayFramePayload->lastStreamId,
                      new_frame.payload.http2GoawayFramePayload->lastStreamId);
}

void test_http2_frame_from_bytes_window_update_frame(void) {
    Http2WindowUpdatePayloadT payload;
    Http2FrameHeaderT header;
    Http2FrameT frame;
    uint8_t buffer[HTTP2_WINDOW_UPDATE_PAYLOAD_LEN + HTTP2_FRAME_HEADER_SIZE];
    Http2WindowUpdatePayloadT new_payload;
    Http2FrameT new_frame;

    payload.windowSizeIncrement = UINT16_MAX;

    header.type = WINDOW_UPDATE_FRAME_TYPE;
    header.streamId = 0u;
    header.flags = DEFAULT_FRAME_FLAG;
    header.length = HTTP2_WINDOW_UPDATE_PAYLOAD_LEN;

    frame.header = header;
    frame.payload.http2WindowUpdatePayload = &payload;

    http2FrameToBytes(&frame,
                      buffer,
                      NULL);

    new_frame.header = header;
    new_frame.payload.http2WindowUpdatePayload = &new_payload;

    http2FrameFromBytes(&new_frame,
                        false,
                        buffer);

    TEST_ASSERT_EQUAL(frame.header.type,
                      new_frame.header.type);

    TEST_ASSERT_EQUAL(frame.header.streamId,
                      new_frame.header.streamId);

    TEST_ASSERT_EQUAL(frame.header.flags,
                      new_frame.header.flags);

    TEST_ASSERT_EQUAL(frame.payload.http2WindowUpdatePayload->windowSizeIncrement,
                      new_frame.payload.http2WindowUpdatePayload->windowSizeIncrement);
}

void test_http2_frame_from_bytes_continuation_frame(void) {
    Http2HeaderFieldT host;
    Http2HeaderFieldT user_agent;
    Http2HeaderListElementT user_agent_element;
    Http2HeaderListElementT host_element;
    Http2HeaderListT header_list;
    nghttp2_hd_deflater *nghttp2HdDeflater = NULL;
    nghttp2_hd_inflater *nghttp2HdInflater = NULL;
    Http2BufferT *buffer;
    Http2ContinuationPayloadT payload;
    uint8_t *header_block;
    Http2ContinuationPayloadT new_payload;
    Http2FrameHeaderT header;
    uint8_t *frame_buffer;
    Http2FrameT frame;
    Http2FrameT new_frame;

    host.name = (uint8_t *) "host";
    host.nameLength = strlen("host");
    host.value = (uint8_t *) "www.wu.ac.at";
    host.valueLength = strlen("www.wu.ac.at");

    user_agent.name = (uint8_t *) "user-agent";
    user_agent.nameLength = strlen("user-agent");
    user_agent.value = (uint8_t *) "curl/7.68.0";
    user_agent.valueLength = strlen("curl/7.68.0");

    user_agent_element.next = NULL;
    user_agent_element.http2HeaderField = &user_agent;

    host_element.next = &user_agent_element;
    host_element.http2HeaderField = &host;

    header_list.length = 2u;
    header_list.head = &host_element;
    header_list.tail = &user_agent_element;

    ngHttp2GetDeflater(&nghttp2HdDeflater, 4096u);

    ngHttp2GetInflater(&nghttp2HdInflater);

    http2HeadersEncode(&header_list,
                       &buffer,
                       nghttp2HdDeflater);

    payload.headerBlockFragment = buffer->buffer;

    header_block = http2Malloc(buffer->length);

    new_payload.headerBlockFragment = header_block;

    header.length = buffer->length;
    header.flags = END_HEADERS_FRAME_FLAG;
    header.streamId = 4u;
    header.type = CONTINUATION_FRAME_TYPE;

    frame_buffer = http2Malloc(buffer->length + HTTP2_FRAME_HEADER_SIZE);

    frame.header = header;
    frame.payload.http2ContinuationPayload = &payload;

    new_frame.payload.http2ContinuationPayload = &new_payload;

    http2FrameToBytes(&frame,
                      frame_buffer,
                      NULL);

    http2FrameFromBytes(&new_frame,
                        false,
                        frame_buffer);

    TEST_ASSERT_EQUAL(frame.header.type,
                      new_frame.header.type);

    TEST_ASSERT_EQUAL(frame.header.streamId,
                      new_frame.header.streamId);

    TEST_ASSERT_EQUAL(frame.header.flags,
                      new_frame.header.flags);

    TEST_ASSERT_EQUAL(0, memcmp(
            frame.payload.http2ContinuationPayload->headerBlockFragment,
            new_frame.payload.http2ContinuationPayload->headerBlockFragment,
            buffer->length));

    http2BufferDeallocate(buffer);
    nghttp2_hd_deflate_del(nghttp2HdDeflater);
    nghttp2_hd_inflate_del(nghttp2HdInflater);
    http2Free(header_block);
    http2Free(frame_buffer);
}

void test_http2_frame_to_bytes_frame_with_allocation(void) {
    Http2FrameHeaderT header;
    Http2FramePaddingT frame_padding;
    Http2FrameT frame;
    uint8_t *buffer;
    size_t buffer_size;
    uint8_t new_data_frame_payload[5];
    uint8_t data_frame_payload[5] = {'t', 'e', 's', 't', 's'};
    uint8_t padding[8] = {
            1u,
            2u,
            3u,
            4u,
            5u,
            6u,
            7u,
            8u
    };
    Http2FrameT new_frame;

    header.type = DATA_FRAME_TYPE;
    header.flags = PADDED_FRAME_FLAG;
    header.length = 14u;
    header.streamId = 5u;

    frame_padding.padding = padding;
    frame_padding.length = 8u;

    frame.header = header;
    frame.payload.http2DataFramePayload = data_frame_payload;

    if (http2FrameToBytesWithAllocation(&frame,
                                        &buffer,
                                        &buffer_size,
                                        &frame_padding) == UNSUPPORTED_FRAME_TYPE) {
        TEST_FAIL();
    }

    new_frame.payload.http2DataFramePayload = new_data_frame_payload;

    if (http2FrameFromBytes(&new_frame,
                            false,
                            buffer) != HTTP2_SUCCESS) {
        TEST_FAIL();
    }

    TEST_ASSERT_EQUAL(frame.header.type,
                      new_frame.header.type);

    TEST_ASSERT_EQUAL(frame.header.streamId,
                      new_frame.header.streamId);

    TEST_ASSERT_EQUAL(frame.header.flags,
                      new_frame.header.flags);

    TEST_ASSERT_EQUAL(0,
                      memcmp(new_frame.payload.http2DataFramePayload,
                             frame.payload.http2DataFramePayload,
                             new_frame.header.length));

    http2Free(buffer);
}

void test_http2_frame_data_frame_payload_allocate(void) {
    Http2FrameHeaderT header;
    uint8_t data_frame_payload[5] = {'t', 'e', 's', 't', 's'};
    uint8_t padding[8] = {
            1u,
            2u,
            3u,
            4u,
            5u,
            6u,
            7u,
            8u
    };
    Http2FramePaddingT frame_padding;
    uint8_t buffer[14u + HTTP2_FRAME_HEADER_SIZE];
    Http2FrameT frame;
    Http2FrameT *new_frame;

    header.type = DATA_FRAME_TYPE;
    header.flags = PADDED_FRAME_FLAG;
    header.length = 14u;

    frame_padding.padding = padding;
    frame_padding.length = 8u;

    frame.header = header;
    frame.payload.http2DataFramePayload = data_frame_payload;

    if (http2FrameToBytes(&frame,
                          buffer,
                          &frame_padding) == UNSUPPORTED_FRAME_TYPE) {
        TEST_FAIL();
    }

    http2FrameAllocate(&new_frame,
                       DATA_FRAME_TYPE,
                       frame.header.flags,
                       frame.header.length);

    if (http2FrameFromBytes(new_frame,
                            false,
                            buffer) != HTTP2_SUCCESS) {
        TEST_FAIL();
    }

    TEST_ASSERT_EQUAL(0,
                      memcmp(new_frame->payload.http2DataFramePayload,
                             frame.payload.http2DataFramePayload,
                             new_frame->header.length));

    http2FrameDeallocate(new_frame);
}

void test_http2_frame_headers_frame_payload_allocate(void) {
    Http2HeaderFieldT user_agent;
    Http2HeaderListElementT user_agent_element;
    Http2HeaderFieldT host;
    Http2HeaderListElementT host_element;
    Http2HeaderListT header_list;
    nghttp2_hd_deflater *nghttp2HdDeflater = NULL;
    nghttp2_hd_inflater *nghttp2HdInflater = NULL;
    Http2BufferT *buffer;
    Http2HeadersFramePayloadT payload;
    uint8_t padding[8] = {
            1u,
            2u,
            3u,
            4u,
            5u,
            6u,
            7u,
            8u
    };
    Http2FramePaddingT frame_padding;
    uint32_t frame_length;
    Http2FrameT frame;
    uint8_t *frame_buffer;
    Http2FrameT *new_frame;

    user_agent.name = (uint8_t *) "user-agent";
    user_agent.nameLength = strlen("user-agent");
    user_agent.value = (uint8_t *) "curl/7.68.0";
    user_agent.valueLength = strlen("curl/7.68.0");

    user_agent_element.next = NULL;
    user_agent_element.http2HeaderField = &user_agent;

    host.name = (uint8_t *) "host";
    host.nameLength = strlen("host");
    host.value = (uint8_t *) "www.wu.ac.at";
    host.valueLength = strlen("www.wu.ac.at");

    host_element.next = &user_agent_element;
    host_element.http2HeaderField = &host;

    header_list.length = 2u;
    header_list.head = &host_element;
    header_list.tail = &user_agent_element;

    ngHttp2GetDeflater(&nghttp2HdDeflater, 4096u);

    ngHttp2GetInflater(&nghttp2HdInflater);

    http2HeadersEncode(&header_list,
                       &buffer,
                       nghttp2HdDeflater);

    payload.headerPayload = buffer->buffer;
    payload.priority.dependency = 20u;
    payload.priority.weight = 8u;
    payload.priority.exclusive = true;

    frame_padding.padding = padding;
    frame_padding.length = 8u;

    frame_length = (uint32_t) (sizeof(uint8_t) + buffer->length + frame_padding.length +
                               HTTP2_PRIORITY_FRAME_PAYLOAD_SIZE);

    frame.header.length = frame_length;
    frame.header.flags = END_HEADERS_FRAME_FLAG | PADDED_FRAME_FLAG | PRIORITY_FRAME_FLAG;
    frame.header.streamId = 4u;
    frame.header.type = HEADERS_FRAME_TYPE;
    frame.payload.http2HeadersFramePayload = &payload;

    frame_buffer = http2Malloc(frame_length + HTTP2_FRAME_HEADER_SIZE);

    http2FrameToBytes(&frame,
                      frame_buffer,
                      &frame_padding);

    http2FrameAllocate(&new_frame,
                       HEADERS_FRAME_TYPE,
                       frame.header.flags,
                       frame.header.length);

    http2FrameFromBytes(new_frame,
                        false,
                        frame_buffer);

    TEST_ASSERT_EQUAL(new_frame->payload.http2HeadersFramePayload->priority.exclusive,
                      frame.payload.http2HeadersFramePayload->priority.exclusive);

    TEST_ASSERT_EQUAL(new_frame->payload.http2HeadersFramePayload->priority.dependency,
                      frame.payload.http2HeadersFramePayload->priority.dependency);

    TEST_ASSERT_EQUAL(new_frame->payload.http2HeadersFramePayload->priority.weight,
                      frame.payload.http2HeadersFramePayload->priority.weight);

    TEST_ASSERT_EQUAL(0u, memcmp(new_frame->payload.http2HeadersFramePayload->headerPayload,
                                 frame.payload.http2HeadersFramePayload->headerPayload,
                                 buffer->length));

    http2FrameDeallocate(new_frame);
    http2BufferDeallocate(buffer);
    nghttp2_hd_inflate_del(nghttp2HdInflater);
    nghttp2_hd_deflate_del(nghttp2HdDeflater);
    http2Free(frame_buffer);
}

void test_http2_frame_priority_frame_payload_allocate(void) {
    Http2PriorityFramePayloadT payload;
    Http2FrameT frame;
    uint8_t padding[8] = {
            1u,
            2u,
            3u,
            4u,
            5u,
            6u,
            7u,
            8u
    };
    Http2FramePaddingT frame_padding;
    uint8_t buffer[HTTP2_PRIORITY_FRAME_PAYLOAD_SIZE + HTTP2_FRAME_HEADER_SIZE];
    Http2FrameT *new_frame;

    payload.weight = 4u;
    payload.dependency = 5u;
    payload.exclusive = true;

    frame.header.length = HTTP2_PRIORITY_FRAME_PAYLOAD_SIZE;
    frame.header.type = PRIORITY_FRAME_TYPE;
    frame.header.streamId = 3u;
    frame.header.flags = DEFAULT_FRAME_FLAG;
    frame.payload.http2PriorityFramePayload = &payload;

    frame_padding.padding = padding;
    frame_padding.length = 8u;

    http2FrameToBytes(&frame,
                      buffer,
                      &frame_padding);

    http2FrameAllocate(&new_frame,
                       PRIORITY_FRAME_TYPE,
                       frame.header.flags,
                       frame.header.length);

    http2FrameFromBytes(new_frame,
                        false,
                        buffer);

    TEST_ASSERT_EQUAL(frame.payload.http2PriorityFramePayload->exclusive,
                      new_frame->payload.http2PriorityFramePayload->exclusive);

    TEST_ASSERT_EQUAL(frame.payload.http2PriorityFramePayload->weight,
                      new_frame->payload.http2PriorityFramePayload->weight);

    TEST_ASSERT_EQUAL(frame.payload.http2PriorityFramePayload->dependency,
                      new_frame->payload.http2PriorityFramePayload->dependency);

    http2FrameDeallocate(new_frame);
}

void test_http2_frame_rst_stream_frame_payload_allocate(void) {
    Http2RstStreamFramePayloadT payload;
    Http2FrameHeaderT header;
    Http2FrameT frame;
    Http2FrameT *new_frame;
    uint8_t buffer[HTTP2_RST_STREAM_FRAME_PAYLOAD_SIZE + HTTP2_FRAME_HEADER_SIZE];

    payload.errorCode = PROTOCOL_ERROR_ERROR_CODE;

    header.streamId = 4u;
    header.length = HTTP2_RST_STREAM_FRAME_PAYLOAD_SIZE;
    header.type = RST_STREAM_FRAME_TYPE;

    frame.header = header;
    frame.payload.http2RstStreamFramePayload = &payload;

    http2FrameAllocate(&new_frame,
                       RST_STREAM_FRAME_TYPE,
                       frame.header.flags,
                       frame.header.length);

    http2FrameToBytes(&frame,
                      buffer,
                      NULL);

    http2FrameFromBytes(new_frame,
                        false,
                        buffer);

    TEST_ASSERT_EQUAL(frame.payload.http2RstStreamFramePayload->errorCode,
                      new_frame->payload.http2RstStreamFramePayload->errorCode);

    http2FrameDeallocate(new_frame);
}

void test_http2_frame_settings_frame_payload_allocate(void) {
    Http2SettingsFramePayloadT payload;
    Http2FrameHeaderT header;
    Http2FrameT frame;
    uint8_t buffer[HTTP2_SETTINGS_FORMAT_SIZE * 5 + HTTP2_FRAME_HEADER_SIZE];
    Http2FrameT *new_frame;

    payload.initialWindowSize = 80000u;
    payload.maxHeaderListSize = 90000u;
    payload.maxFrameSize = 32000u;
    payload.maxConcurrentStreams = 100u;
    payload.enablePush = true;
    payload.headerTableSize = 8192;

    header.type = SETTINGS_FRAME_TYPE;
    header.length = HTTP2_SETTINGS_FORMAT_SIZE * 5;
    header.flags = DEFAULT_FRAME_FLAG;
    header.streamId = 0u;

    frame.header = header;
    frame.payload.http2SettingsFramePayload = &payload;

    http2FrameToBytes(&frame,
                      buffer,
                      NULL);

    http2FrameAllocate(&new_frame,
                       SETTINGS_FRAME_TYPE,
                       frame.header.flags,
                       frame.header.length);

    http2FrameFromBytesHeaderSet(new_frame,
                                 false,
                                 buffer);

    TEST_ASSERT_EQUAL(frame.payload.http2SettingsFramePayload->initialWindowSize,
                      new_frame->payload.http2SettingsFramePayload->initialWindowSize);
    TEST_ASSERT_EQUAL(frame.payload.http2SettingsFramePayload->maxHeaderListSize,
                      new_frame->payload.http2SettingsFramePayload->maxHeaderListSize);
    TEST_ASSERT_EQUAL(frame.payload.http2SettingsFramePayload->maxFrameSize,
                      new_frame->payload.http2SettingsFramePayload->maxFrameSize);
    TEST_ASSERT_EQUAL(frame.payload.http2SettingsFramePayload->maxConcurrentStreams,
                      new_frame->payload.http2SettingsFramePayload->maxConcurrentStreams);
    TEST_ASSERT_EQUAL(frame.payload.http2SettingsFramePayload->enablePush,
                      new_frame->payload.http2SettingsFramePayload->enablePush);
    TEST_ASSERT_EQUAL(frame.payload.http2SettingsFramePayload->headerTableSize,
                      new_frame->payload.http2SettingsFramePayload->headerTableSize);

    http2FrameDeallocate(new_frame);
}

void test_http2_frame_push_promise_frame_payload_allocate(void) {
    Http2HeaderFieldT host;
    Http2HeaderFieldT user_agent;
    Http2HeaderListElementT user_agent_element;
    Http2HeaderListElementT host_element;
    Http2HeaderListT header_list;
    nghttp2_hd_deflater *nghttp2HdDeflater = NULL;
    nghttp2_hd_inflater *nghttp2HdInflater = NULL;
    Http2BufferT *buffer;
    Http2PushPromiseFramePayloadT payload;
    Http2FrameHeaderT header;
    Http2FrameT frame;
    uint8_t *frame_buffer;
    Http2FrameT *new_frame;

    host.name = (uint8_t *) "host";
    host.nameLength = strlen("host");
    host.value = (uint8_t *) "www.wu.ac.at";
    host.valueLength = strlen("www.wu.ac.at");

    user_agent.name = (uint8_t *) "user-agent";
    user_agent.nameLength = strlen("user-agent");
    user_agent.value = (uint8_t *) "curl/7.68.0";
    user_agent.valueLength = strlen("curl/7.68.0");

    user_agent_element.next = NULL;
    user_agent_element.http2HeaderField = &user_agent;

    host_element.http2HeaderField = &host;
    host_element.next = &user_agent_element;

    header_list.length = 2u;
    header_list.head = &host_element;
    header_list.tail = &user_agent_element;

    ngHttp2GetDeflater(&nghttp2HdDeflater, 4096u);

    ngHttp2GetInflater(&nghttp2HdInflater);

    http2HeadersEncode(&header_list,
                       &buffer,
                       nghttp2HdDeflater);

    payload.headerBlockFragment = buffer->buffer;
    payload.promisedStreamId = 23u;

    header.type = PUSH_PROMISE_FRAME_TYPE;
    header.streamId = 3u;
    header.length = (uint32_t) (buffer->length + sizeof(uint32_t));
    header.flags = DEFAULT_FRAME_FLAG;

    frame.header = header;
    frame.payload.http2PushPromiseFramePayload = &payload;

    frame_buffer = http2Malloc(buffer->length + sizeof(uint32_t) + HTTP2_FRAME_HEADER_SIZE);

    http2FrameToBytes(&frame,
                      frame_buffer,
                      NULL);

    http2FrameAllocate(&new_frame,
                       PUSH_PROMISE_FRAME_TYPE,
                       frame.header.flags,
                       frame.header.length);

    http2FrameFromBytes(new_frame,
                        false,
                        frame_buffer);

    TEST_ASSERT_EQUAL(frame.payload.http2PushPromiseFramePayload->promisedStreamId,
                      new_frame->payload.http2PushPromiseFramePayload->promisedStreamId);

    TEST_ASSERT_EQUAL(0,
                      memcmp(frame.payload.http2PushPromiseFramePayload->headerBlockFragment,
                             new_frame->payload.http2PushPromiseFramePayload->headerBlockFragment,
                             buffer->length));

    http2FrameDeallocate(new_frame);

    http2BufferDeallocate(buffer);
    nghttp2_hd_deflate_del(nghttp2HdDeflater);
    nghttp2_hd_inflate_del(nghttp2HdInflater);
    http2Free(frame_buffer);
}

void test_http2_frame_ping_frame_payload_allocate(void) {
    Http2PingFramePayloadT payload;
    uint8_t i;
    Http2FrameHeaderT header;
    Http2FrameT frame;
    Http2FrameT *new_frame;
    uint8_t buffer[HTTP2_PING_FRAME_PAYLOAD_SIZE + HTTP2_FRAME_HEADER_SIZE];

    for (i = 0u; i < 8u; ++i) {
        payload.opaqueData[i] = i;
    }

    header.length = HTTP2_PING_FRAME_PAYLOAD_SIZE;
    header.streamId = 3u;
    header.type = PING_FRAME_TYPE;
    header.flags = DEFAULT_FRAME_FLAG;

    frame.header = header;
    frame.payload.http2PingFramePayload = &payload;

    http2FrameAllocate(&new_frame,
                       PING_FRAME_TYPE,
                       frame.header.flags,
                       frame.header.length);

    http2FrameToBytes(&frame,
                      buffer,
                      NULL);

    http2FrameFromBytes(new_frame,
                        false,
                        buffer);

    TEST_ASSERT_EQUAL(0, memcmp(frame.payload.http2PingFramePayload->opaqueData,
                                new_frame->payload.http2PingFramePayload->opaqueData,
                                HTTP2_PING_FRAME_PAYLOAD_SIZE));

    http2FrameDeallocate(new_frame);
}

void test_http2_frame_goaway_frame_payload_allocate(void) {
    Http2GoawayFramePayloadT payload;
    Http2FrameHeaderT header;
    Http2FrameT frame;
    uint8_t buffer[HTTP2_GOAWAY_MINIMUM_PAYLOAD_LEN + HTTP2_FRAME_HEADER_SIZE];
    Http2FrameT *new_frame;

    payload.errorCode = PROTOCOL_ERROR_ERROR_CODE;
    payload.additionalDebugData = NULL;
    payload.lastStreamId = 45u;

    header.type = GOAWAY_FRAME_TYPE;
    header.flags = DEFAULT_FRAME_FLAG;
    header.streamId = 0u;
    header.length = HTTP2_GOAWAY_MINIMUM_PAYLOAD_LEN;

    frame.header = header;
    frame.payload.http2GoawayFramePayload = &payload;

    http2FrameToBytes(&frame,
                      buffer,
                      NULL);

    http2FrameAllocate(&new_frame,
                       GOAWAY_FRAME_TYPE,
                       frame.header.flags,
                       frame.header.length);

    http2FrameFromBytes(new_frame,
                        false,
                        buffer);

    TEST_ASSERT_EQUAL(frame.payload.http2GoawayFramePayload->errorCode,
                      new_frame->payload.http2GoawayFramePayload->errorCode);
    TEST_ASSERT_EQUAL(frame.payload.http2GoawayFramePayload->lastStreamId,
                      new_frame->payload.http2GoawayFramePayload->lastStreamId);

    http2FrameDeallocate(new_frame);
}

void test_http2_frame_window_update_frame_payload_allocate(void) {
    Http2FrameHeaderT header;
    Http2WindowUpdatePayloadT payload;
    Http2FrameT frame;
    Http2FrameT *new_frame;
    uint8_t buffer[HTTP2_WINDOW_UPDATE_PAYLOAD_LEN + HTTP2_FRAME_HEADER_SIZE];
    payload.windowSizeIncrement = UINT16_MAX;

    header.type = WINDOW_UPDATE_FRAME_TYPE;
    header.streamId = 0u;
    header.flags = DEFAULT_FRAME_FLAG;
    header.length = HTTP2_WINDOW_UPDATE_PAYLOAD_LEN;

    frame.header = header;
    frame.payload.http2WindowUpdatePayload = &payload;

    http2FrameToBytes(&frame,
                      buffer,
                      NULL);

    http2FrameAllocate(&new_frame, WINDOW_UPDATE_FRAME_TYPE, frame.header.flags, frame.header.length);

    http2FrameFromBytes(new_frame,
                        false,
                        buffer);

    TEST_ASSERT_EQUAL(frame.payload.http2WindowUpdatePayload->windowSizeIncrement,
                      new_frame->payload.http2WindowUpdatePayload->windowSizeIncrement);

    http2FrameDeallocate(new_frame);
}

void test_http2_frame_continuation_frame_payload_allocate(void) {
    Http2HeaderFieldT host;
    Http2HeaderFieldT user_agent;
    Http2HeaderListElementT user_agent_element;
    Http2HeaderListElementT host_element;
    Http2HeaderListT header_list;
    nghttp2_hd_deflater *nghttp2HdDeflater = NULL;
    nghttp2_hd_inflater *nghttp2HdInflater = NULL;
    Http2BufferT *buffer;
    Http2ContinuationPayloadT payload;
    Http2FrameHeaderT header;
    uint8_t *frame_buffer;
    Http2FrameT frame;
    Http2FrameT *new_frame;

    host.name = (uint8_t *) "host";
    host.nameLength = strlen("host");
    host.value = (uint8_t *) "www.wu.ac.at";
    host.valueLength = strlen("www.wu.ac.at");

    user_agent.name = (uint8_t *) "user-agent";
    user_agent.nameLength = strlen("user-agent");
    user_agent.value = (uint8_t *) "curl/7.68.0";
    user_agent.valueLength = strlen("curl/7.68.0");

    user_agent_element.http2HeaderField = &user_agent;
    user_agent_element.next = NULL;

    host_element.http2HeaderField = &host;
    host_element.next = &user_agent_element;

    header_list.length = 2u;
    header_list.head = &host_element;
    header_list.tail = &user_agent_element;

    ngHttp2GetDeflater(&nghttp2HdDeflater, 4096u);

    ngHttp2GetInflater(&nghttp2HdInflater);

    http2HeadersEncode(&header_list,
                       &buffer,
                       nghttp2HdDeflater);

    payload.headerBlockFragment = buffer->buffer;

    header.length = (uint32_t) buffer->length;
    header.flags = END_HEADERS_FRAME_FLAG;
    header.streamId = 4u;
    header.type = CONTINUATION_FRAME_TYPE;

    frame_buffer = http2Malloc(buffer->length + HTTP2_FRAME_HEADER_SIZE);

    frame.header = header;
    frame.payload.http2ContinuationPayload = &payload;

    http2FrameAllocate(&new_frame, CONTINUATION_FRAME_TYPE, frame.header.flags, frame.header.length);

    http2FrameToBytes(&frame,
                      frame_buffer,
                      NULL);

    http2FrameFromBytes(new_frame,
                        false,
                        frame_buffer);

    TEST_ASSERT_EQUAL(0, memcmp(
            frame.payload.http2ContinuationPayload->headerBlockFragment,
            new_frame->payload.http2ContinuationPayload->headerBlockFragment,
            buffer->length));

    http2FrameDeallocate(new_frame);
    http2BufferDeallocate(buffer);
    nghttp2_hd_inflate_del(nghttp2HdInflater);
    nghttp2_hd_deflate_del(nghttp2HdDeflater);
    http2Free(frame_buffer);
}

void test_http2_frame_goaway_frame_payload_allocate_debug_data(void) {
    uint8_t *debugData;
    Http2GoawayFramePayloadT payload;
    Http2FrameHeaderT header;
    Http2FrameT frame;
    uint8_t buffer[HTTP2_GOAWAY_MINIMUM_PAYLOAD_LEN + HTTP2_FRAME_HEADER_SIZE + 203];
    Http2FrameT *new_frame;

    debugData = http2Malloc(203);

    memset(debugData, 1, 203);

    payload.errorCode = PROTOCOL_ERROR_ERROR_CODE;
    payload.additionalDebugData = debugData;
    payload.lastStreamId = 45u;

    header.type = GOAWAY_FRAME_TYPE;
    header.flags = DEFAULT_FRAME_FLAG;
    header.streamId = 0u;
    header.length = HTTP2_GOAWAY_MINIMUM_PAYLOAD_LEN + 203;

    frame.header = header;
    frame.payload.http2GoawayFramePayload = &payload;

    http2FrameToBytes(&frame,
                      buffer,
                      NULL);

    http2FrameAllocate(&new_frame,
                       GOAWAY_FRAME_TYPE,
                       frame.header.flags,
                       frame.header.length);

    http2FrameFromBytes(new_frame,
                        false,
                        buffer);

    TEST_ASSERT_EQUAL(frame.payload.http2GoawayFramePayload->errorCode,
                      new_frame->payload.http2GoawayFramePayload->errorCode);
    TEST_ASSERT_EQUAL(frame.payload.http2GoawayFramePayload->lastStreamId,
                      new_frame->payload.http2GoawayFramePayload->lastStreamId);
    TEST_ASSERT_EQUAL(0, memcmp(frame.payload.http2GoawayFramePayload->additionalDebugData,
                                new_frame->payload.http2GoawayFramePayload->additionalDebugData, 203));

    http2FrameDeallocate(new_frame);
    http2Free(debugData);
}
