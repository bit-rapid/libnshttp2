/*
MIT License
-----------

Copyright (c) 2020 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef NS_HTTP2_LIBS_STREAM_H
#define NS_HTTP2_LIBS_STREAM_H

#include <nshttp2/frame.h>
#include <nshttp2/frame_payload.h>
#include <nshttp2/headers.h>
#include <nshttp2/utility/memory.h>
#include <nshttp2/buffer/buffer.h>
#include <nshttp2/stream_hashmap.h>
#include <nshttp2/return_code.h>
#include <nshttp2/frame_list.h>
#include <stdio.h>
#include <nshttp2/buffer/temporary_buffer.h>
#include <nshttp2/utility/file_list.h>
#include <nshttp2/stddef.h>

#define HTTP2_STREAM_CONTINUATION_EXPECTED 4u

struct Http2Stream;

/**
 * HTTP/2 stream states as specified in https://tools.ietf.org/html/rfc7540#section-5.1
 */
typedef enum Http2StreamState {
    IDLE,
    RESERVED_LOCAL,
    RESERVED_REMOTE,
    OPEN,
    HALF_CLOSED_LOCAL,
    HALF_CLOSED_REMOTE,
    CLOSED
} Http2StreamStateT;

/**
 * HTTP/2 stream transitions specified in https://tools.ietf.org/html/rfc7540#section-5.1
 */
typedef enum Http2StreamTransition {
    SEND_PP,
    RECV_PP,
    SEND_H,
    RECV_H,
    RECV_ES,
    SEND_ES,
    SEND_R,
    RECV_R,
    NO_TRANSITION
} Http2StreamTransitionT;

/**
 * HTTP/2 stream structure, used to keep a stream id and state
 */
typedef struct Http2Stream {
    pthread_mutex_t mutex;

    /**
     * The stream identifier is what really gets transmitted over the network.
     */
    uint_fast32_t streamId;
    /**
     * The allowed range is 1-256, however it uses 8 bits,
     * to obtain the value we always need to add 1 to the current weight.
     */
    uint_fast8_t weight;
    uint8_t flags;

    /**
     * Holds the current state of the stream.
     * https://tools.ietf.org/html/rfc7540#section-5.1
     */
    Http2StreamStateT state;

    /**
     * Remaining windows
     */
    int_fast32_t remainingSendWindow;
    int_fast32_t remainingReceiveWindow;

    /**
     * Stream relations to other streams
     */
    struct Http2Stream *parent;
    StreamHashmapT *children;
    StreamHashmapT *siblings;

    /**
     * Stream buffer for incoming/outgoing data
     */
    Http2BufferT *incomingHeadersBuffer;
    Http2TemporaryBufferT incomingDataBuffer;

    Http2BufferT *outgoingDataMemoryBuffer;

    void *customData;
} Http2StreamT;

/**
 * Allocates the stream on the heap and places it into Http2Stream.
 * @param http2Stream HTTP/2 stream DTO.
 * @param streamId Stream id to set for the stream DTO.
 * @param weight Weight to set for the stream.
 * @param remainingSendWindow Send window of the stream.
 * @param remainingReceiveWindow Receive window of the stream.
 * @returns Return codes:
 * - ENOMEM - was not able to allocate memory on the heap. No stream has been allocated.
 * - EXIT_SUCCESS - everything went fine, the stream has been allocated.
 */
Http2ReturnCodeT http2StreamAllocate(Http2StreamT **http2Stream,
                                     uint_fast32_t streamId,
                                     uint8_t weight,
                                     uint32_t remainingSendWindow,
                                     uint32_t remainingReceiveWindow,
                                     size_t maxIncomingDataInMemory);

/**
 * Deallocates the provided Http2Stream.
 * @param http2Stream Stream pointer to the block of memory for the stream.
 */
Http2ReturnCodeT http2StreamDeallocate(Http2StreamT *http2Stream);

/**
 * Adds a child dependency to the stream.
 * @param http2Stream HTTP/2 stream DTO.
 * @param child Child stream to add as a child to the provided stream.
 * @return Return codes:
 * - ENOMEM - was not able to allocate memory on the heap. No stream has been allocated.
 * - EXIT_SUCCESS - everything went fine, the stream has been allocated.
 */
Http2ReturnCodeT http2StreamAddChild(Http2StreamT *http2Stream, Http2StreamT *child);

/**
 * Removes a child from the stream.
 * @param http2Stream HTTP/2 stream DTO.
 * @param child Child stream to remove as a child from the provided stream.
 */
void http2StreamRemoveChild(Http2StreamT *http2Stream, Http2StreamT *child);

/**
 * Adds a stream as a sibling to the given stream.
 * @param http2Stream HTTP/2 stream DTO.
 * @param sibling Sibling to add.
 */
Http2ReturnCodeT http2StreamAddSibling(Http2StreamT *http2Stream,
                                       Http2StreamT *sibling);

/**
 * Removes a sibling from the stream list, as well as from all the lists of the other siblings.
 * @param http2Stream HTTP/2 stream DTO.
 * @param sibling Sibling stream to remove.
 */
void http2StreamRemoveSibling(Http2StreamT *http2Stream, Http2StreamT *sibling);

/**
 * Sets the state of the stream atomically.
 * @param http2Stream HTTP/2 stream DTO.
 * @param state State to set.
 */
void http2StreamSetState(Http2StreamT *http2Stream,
                         Http2StreamStateT state);

/**
 * Check if a frame would deliver an error code.
 * @param http2Stream HTTP/2 stream DTO.
 * @param frame Frame to check to be processed on stream.
 * @param sent Determines if the frame is sent or received on the stream.
 * @return Returns the following error codes:
 * - PROTOCOL_ERROR_ERROR_CODE - Protocol error happens on stream.
 * - STREAM_CLOSED_ERROR_CODE - Stream is closed.
 * - NO_ERROR_ERROR_CODE - Everything is fine.
 */
uint8_t http2StreamFrameGetError(Http2StreamT *http2Stream,
                                 Http2FrameT *frame,
                                 bool sent);

/**
 * Adds headers block to the incoming stream headers buffer.
 * @param http2Stream HTTP/2 stream DTO.
 * @param frame Incoming headers frame to process on stream.
 * @return Returns the following return codes:
 * - HTTP2_NO_MEMORY - No memory available for header buffer allocation.
 * - HTTP2_SUCCESS - Everything is fine.
 */
Http2ReturnCodeT http2StreamAddHeaderBlock(Http2StreamT *http2Stream,
                                           Http2FrameT *frame);

/**
 * Decodes headers found in the incoming stream headers buffer.
 * It clears the headers buffer once they are decoded and put into the headerList.
 * @param http2Stream HTTP/2 stream DTO.
 * @param headerList Header list into which to put the decoded header buffer.
 * @return Returns the following return codes:
 * - HTTP2_NO_MEMORY - No memory available for headerList allocation.
 * - HTTP2_SUCCESS - Everything is fine.
 */
Http2ReturnCodeT http2StreamDecodeHeaders(Http2StreamT *http2Stream,
                                          Http2HeaderListT **headerList,
                                          nghttp2_hd_inflater *nghttp2HdInflater);

Http2ReturnCodeT http2StreamAppendIncomingData(Http2StreamT *http2Stream,
                                               uint8_t *buffer,
                                               size_t bufferLength,
                                               const char *temporaryFileTemplate);

Http2ReturnCodeT http2StreamAppendOutgoingMemoryData(Http2StreamT *http2Stream,
                                                     uint8_t *buffer,
                                                     size_t bufferLength);

Http2ReturnCodeT http2StreamGetIncomingData(Http2StreamT *http2Stream,
                                            uint8_t **buffer,
                                            size_t *bufferLength);

void http2StreamDeallocateIncomingData(Http2StreamT *http2Stream);

void http2StreamDeallocateOutgoingData(Http2StreamT *http2Stream);

bool http2StreamHasRemainingOutgoingData(Http2StreamT *http2Stream);

void http2StreamSetCustomData(Http2StreamT *http2Stream, void *customData);

void http2StreamGetCustomData(Http2StreamT *http2Stream, void **customData);

#endif /* NS_HTTP2_LIBS_STREAM_H */

