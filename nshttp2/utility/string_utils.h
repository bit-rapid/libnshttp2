/*
MIT License
-----------

Copyright (c) 2020 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef NS_HTTP2_LIBS_STRING_UTILS_H
#define NS_HTTP2_LIBS_STRING_UTILS_H

#include <stdlib.h>
#include <nshttp2/return_code.h>
#include <nshttp2/stddef.h>

/**
 * Function which checks if str1 starts with str2, the length
 * to be checked is specified. 
 * Limitations: 
 * - str1 and str2 have to be non-null
 * - str1 and str2 have to be at least the length of len
 * @param str1 String to check.
 * @param str2 String to find in str1.
 * @param len Length to check in str1 and str2.
 * @return Boolean indicating if the strings match for given length.
 */
bool stringStartWithSubstring(const char *str1,
                              const char *str2,
                              size_t len);

Http2ReturnCodeT stringDuplicate(const char *string, char **newString);

#endif /* SIMPLE_HTTP2_SERVER_STRING_UTILS_H */
