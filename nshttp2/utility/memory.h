/*
MIT License
-----------

Copyright (c) 2020 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef NS_HTTP2_LIBS_MEMORY_H
#define NS_HTTP2_LIBS_MEMORY_H

#include <stdlib.h>

typedef struct Http2MemoryAllocators {
    /**
   * Custom allocator function to replace malloc().
   */
    void *(*malloc)(size_t size);

    /**
     * Custom allocator function to replace free().
     */
    void (*free)(void *ptr);

    /**
     * Custom allocator function to replace calloc().
     */
    void *(*calloc)(size_t nmemb, size_t size);

    /**
     * Custom allocator function to replace realloc().
     */
    void *(*realloc)(void *ptr, size_t size);
} Http2MemoryAllocatorsT;

/**
 * Allocates memory on the heap.
 * @param size Size in bytes.
 * @return Pointer to the newly allocated space, returns null if it fails.
 */
void *http2Malloc(size_t size);

/**
 * Allocates memory on the heap and zeroes it out.
 * @param size Size in bytes.
 * @return Pointer to the newly allocated space, returns null if it fails.
 */
void *http2Calloc(size_t size);

/**
 * Frees memory on the heap.
 * @param ptr Pointer to the start of the memory.
 */
void http2Free(void *ptr);

/**
 * Reallocates memory on the heap.
 * @param ptr Pointer to existing heap memory.
 * @param size Size to reallocate the memory to.
 * @return Pointer to the new heap memory. Can be NULL.
 */
void *http2Realloc(void *ptr, size_t size);

/**
 * Replaces the standard library memory functions with the specified memory functions.
 * @param memoryAllocators Memory allocators/deallocators to use.
 */
void http2SetupMemoryAllocators(Http2MemoryAllocatorsT *memoryAllocators);

#endif /* NS_HTTP2_LIBS_MEMORY_H */
