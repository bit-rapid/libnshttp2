/*
MIT License
-----------

Copyright (c) 2020 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef NSHTTP2_FILE_LIST_H
#define NSHTTP2_FILE_LIST_H

#include <stdlib.h>
#include <nshttp2/return_code.h>

typedef struct Http2OutgoingFile {
    char *fileName;
    size_t offset;
    size_t size;
} Http2OutgoingFileT;

/**
 * Wrapper structure for Http2OutgoingFile elements,
 * used as a list element for Http2FileList.
 */
typedef struct Http2FileListElement {
    Http2OutgoingFileT *outgoingFile;
    struct Http2FileListElement *next;
} Http2FileListElementT;

/**
 * Singly-linked list containing Http2FileList elements
 * and length of the list.
 */
typedef struct Http2FileList {
    Http2FileListElementT *head;
    Http2FileListElementT *tail;
    size_t length;
    size_t bufferSize;
} Http2FileListT;

Http2ReturnCodeT http2OutgoingFileAllocate(Http2OutgoingFileT **outgoingFile,
                                           char *fileName,
                                           size_t offset,
                                           size_t size);

void http2OutgoingFileDeallocate(Http2OutgoingFileT *outgoingFile);

Http2ReturnCodeT http2FileListAllocate(Http2FileListT **fileList,
                                       char *fileName,
                                       size_t offset,
                                       size_t size);

void http2FileListDeallocate(Http2FileListT *fileList);

Http2ReturnCodeT http2FileListAppend(Http2FileListT *fileList,
                                     char *fileName,
                                     size_t offset,
                                     size_t size);

void http2FileListRemoveElementFromStart(Http2FileListT *fileList);

#endif /* NSHTTP2_FILE_LIST_H */
