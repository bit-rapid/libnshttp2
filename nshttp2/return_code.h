/*
MIT License
-----------

Copyright (c) 2020 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef NSHTTP2_RETURN_CODES_H
#define NSHTTP2_RETURN_CODES_H

typedef enum Http2ReturnCode {
    /**
     * Everything went fine.
     */
    HTTP2_SUCCESS = 0,
    /**
     * Not enough memory on the heap.
     */
    HTTP2_NO_MEMORY = -1,
    /**
     * Stream hashmap is empty.
     */
    HTTP2_STREAM_MAP_EMPTY = -2,
    /**
     * Internal failure, which cannot be fixed.
     */
    HTTP2_FAILURE = -3,
    /**
     * Error code indicating that the provided frame size is
     * not enough to store all the provided headers in the DTO.
     * Guaranteed not to be used by nghttp2 as a return code.
     */
    HTTP2_BUFFER_INSUFFICIENT = -4,
    /**
     * The provided frame is unsupported currently.
     */
    HTTP2_UNSUPPORTED_FRAME_TYPE = -5,
    /**
     * This action results in a protocol error.
     */
    HTTP2_PROTOCOL_ERROR = -6,
    /**
     * Runtime error happened, which cannot be easily fixed.
     */
    HTTP2_INTERNAL_ERROR = -7,
    HTTP2_STREAM_CLOSED_ERROR = -8,
    HTTP2_STREAM_ID_TOO_LARGE = -9
} Http2ReturnCodeT;

#endif /* NSHTTP2_RETURN_CODES_H */
