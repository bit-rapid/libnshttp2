/*
MIT License
-----------

Copyright (c) 2020 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef NS_HTTP2_LIBS_HEADERS_H
#define NS_HTTP2_LIBS_HEADERS_H

#include <nghttp2/nghttp2.h>
#include <nshttp2/frame.h>
#include <nshttp2/buffer/buffer.h>
#include <string.h>
#include <stdlib.h>
#include <nshttp2/frame_list.h>
#include <nshttp2/stddef.h>

/**
 * Structure representing a HTTP/2 header.
 */
typedef struct Http2HeaderField {
    uint8_t *name;
    uint8_t *value;
    size_t nameLength;
    size_t valueLength;
} Http2HeaderFieldT;

/**
 * HTTP/2 header list element.
 */
typedef struct Http2HeaderListElement {
    Http2HeaderFieldT *http2HeaderField;
    struct Http2HeaderListElement *next;
} Http2HeaderListElementT;

/**
 * HTTP/2 header list.
 */
typedef struct Http2HeaderList {
    Http2HeaderListElementT *head;
    Http2HeaderListElementT *tail;
    size_t length;
} Http2HeaderListT;

/**
 * Append header to a list by providing a name and value pair.
 * @param headerList Header list to which to append the header name/value pair.
 * @param name Name of the header.
 * @param nameLength Length of the name string (without null character).
 * @param value Value of the header.
 * @param valueLength Length of the value string (without null character).
 * @return Following return values are possible:
 * NGHTTP2_ERR_NOMEM - Not enough memory on the heap available.
 * EXIT_SUCCESS - Allocation was successful.
 */
int http2HeaderListAppend(Http2HeaderListT *headerList,
                          const char *name,
                          size_t nameLength,
                          const char *value,
                          size_t valueLength);

/**
 * Append header to a list by providing a name and value pair.
 * @param headerList Header list to which to append the header name/value pair.
 * @param name Name of the header.
 * @param nameLength Length of the name string (without null character).
 * @param value Value of the header.
 * @param valueLength Length of the value string (without null character).
 * @return Following return values are possible:
 * NGHTTP2_ERR_NOMEM - Not enough memory on the heap available.
 * EXIT_SUCCESS - Allocation was successful.
 */
int http2HeaderListPrepend(Http2HeaderListT *headerList,
                           const char *name,
                           size_t nameLength,
                           const char *value,
                           size_t valueLength);

/**
 * Allocates the http2HeaderField on the heap of proper size for the given
 * name length and value length.
 * @param http2HeaderField Pointer to a pointer to set for the newly allocated Http2HeaderFieldT.
 * @param nameLength Length of header name.
 * @param valueLength Length of value name.
 * @return Following return values are possible:
 * NGHTTP2_ERR_NOMEM - Not enough memory on the heap available.
 * EXIT_SUCCESS - Allocation was successful.
 */
int http2HeaderFieldAllocate(Http2HeaderFieldT **http2HeaderField,
                             size_t nameLength,
                             size_t valueLength);

/**
 * Deallocates the http2HeaderField.
 * @param http2HeaderField HTTP/2 header DTO, assumed not to be null.
 */
void http2HeaderFieldDeallocate(Http2HeaderFieldT *http2HeaderField);

/**
 * Fills the nv values based on http2HeaderField.
 * Does not allocate Http2HeaderField, only its fields.
 * @param http2HeaderField HTTP/2 header field DTO.
 * @param nv NGHTTP2 NV DTO to fill with values.
 * @return Following return values are possible:
 * NGHTTP2_ERR_NOMEM - Not enough memory on the heap available.
 * EXIT_SUCCESS - Allocation and filling of values was successful.
 */
int http2HeaderFieldToNv(Http2HeaderFieldT *http2HeaderField,
                         nghttp2_nv *nv);

/**
 * Converts the Http2HeaderList into nv, which is allocated on the heap.
 * @param http2_header_list HTTP/2 header list DTO.
 * @param nv NGHTTP2 NV DTO.
 * @return Following return values are possible:
 * NGHTTP2_ERR_NOMEM - Not enough memory on the heap available.
 * EXIT_SUCCESS - Allocation was successful.
 */
int http2HeadersToNvArray(Http2HeaderListT *http2_header_list,
                          nghttp2_nv **nv);

/**
 * Allocates the http2HeaderListElement on the heap.
 * @param nameLength Length of header name.
 * @param valueLength Length of value name.
 * @return  Following return values are possible:
 * NGHTTP2_ERR_NOMEM - Not enough memory on the heap available.
 * EXIT_SUCCESS - Allocation was successful.
 */
int http2HeaderAllocate(Http2HeaderListElementT **http2HeaderListElement,
                        size_t nameLength,
                        size_t valueLength);

/**
 * Deallocates the http2HeaderList.
 * @param http2HeaderList HTTP/2 header list DTO.
 */
void http2HeaderListDeallocate(Http2HeaderListT *http2HeaderList);

/**
 * Fills the values of http2HeaderField using the values found in nv.
 * @param nv NGHTTP2 NV DTO.
 * @param http2HeaderField DTO to fill with values from nv.
 */
void http2HeaderFromNghttp2Nv(nghttp2_nv *nv,
                              Http2HeaderFieldT *http2HeaderField);

/**
 * Allocates and fills the http2HeaderList based on the http2_headers_frame.
 * The inflater is created automatically.
 * @param http2HeaderList HTTP/2 headers list DTO.
 * @param nghttp2_inflater Nghttp2 inflater, needs to be allocated usign the nghttp2 library.
 * @return Following return values are possible:
 * NGHTTP2_ERR_NOMEM - Not enough memory on the heap available.
 * NGHTTP2_ERR_HEADER_COMP - Inflation process has failed.
 * NGHTTP2_ERR_BUFFER_ERROR - The header field name or value is too large.
 * EXIT_SUCCESS - Decoding was successful.
 */
ssize_t http2HeadersDecode(uint8_t *bufferPtr,
                           uint32_t bufferLength,
                           Http2HeaderListT **http2HeaderList,
                           nghttp2_hd_inflater *nghttp2_inflater);

/**
 * Allocates and fills the buffer based on the header_table_size, max_frame_size
 * from the settings frame.
 * @param http2HeaderList HTTP/2 header list DTO.
 * @param buffer Buffer pointer where to put the allocated deflated data.
 * @return Following return values are possible:
 * HTTP2_NO_MEMORY - Not enough memory on the heap available.
 * HTTP2_BUFFER_INSUFFICIENT - Too many HTTP/2 headers provided, exceeds the maximum header size.
 * HTTP2_FAILURE - Deflation process has failed.
 * HTTP2_SUCCESS - Everything went fine.
 */
Http2ReturnCodeT http2HeadersEncode(Http2HeaderListT *http2HeaderList,
                                    Http2BufferT **buffer,
                                    nghttp2_hd_deflater *nghttp2Deflater);

/**
 * Deallocates nv.
 * @param nv NGHTTP2 NV DTO.
 */
void http2NvFieldsDeallocate(nghttp2_nv *nv);

/**
 * Deallocates an array of NGHTTP2 NV DTOs.
 * @param nv NGHTTP2 NV DTO.
 * @param length Length of array.
 */
void http2NvArrayDeallocate(nghttp2_nv *nv,
                            size_t length);

/**
 * Append the source list elements to the target,
 * deallocates the source list container, but not the elements.
 * @param target Header list to which to append the source list elements.
 * @param source Header list which to append to the target and deallocate the container of.
 */
void http2HeaderListJoin(Http2HeaderListT *target, Http2HeaderListT *source);

Http2ReturnCodeT http2HeaderFramesFromList(Http2FrameListT **frameList,
                                           Http2HeaderListT *headerList,
                                           size_t maxFrameSize,
                                           bool isLastFrame,
                                           uint32_t streamId,
                                           nghttp2_hd_deflater *nghttp2Deflater);

#endif /* NS_HTTP2_LIBS_HEADERS_H */
